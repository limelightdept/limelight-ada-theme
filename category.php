<?php
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
get_header();
?>
    <main id="main">
        <div class="outer-grid">
            <div class="grid">
                <?php
                    while (have_posts()) : the_post();
                ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <div class="entry-content">
                            
                        </div>
                    </article>
                <?php
                    endwhile;
                ?>
            </div>
        </div>
    </main>
<?php
get_footer();
