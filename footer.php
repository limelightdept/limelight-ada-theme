<?php
    global $page_display_options;

    if(!is_array($page_display_options)) $page_display_options = array();

    $hide_navigation = in_array("hide_footer_navigation", $page_display_options);
    $show_policies = in_array("always_show_policies", $page_display_options);
    $policy_pages = [
        'privacy' => limelight_get_option("policy_page_privacy"),
        'terms' => limelight_get_option("policy_page_terms"),
        'disclaimer' => limelight_get_option("policy_page_disclaimer"),
    ];
    // set the image url
    $image_id = limelight_get_option("logo_footer_id");

    $logo = limelight_get_option("logo_footer_id");

    // retrieve the thumbnail size of our image
    $logo = wp_get_attachment_image($image_id, 'full');

    $logo_html = limelight_get_option("logo_footer_html");
    if(!empty($logo_html)) $logo = $logo_html;
        
    $hide_limelight_logo = limelight_get_option("logo_footer_hide");    

    $footer_layout = limelight_get_option("footer_layout");
    if(empty($footer_layout))
        $footer_layout = "default";
    

    $footer_template = limelight_get_option('elementor_footer_template');
    $show_footer = 'ELEMENTOR';

    if(\Elementor\Plugin::$instance->preview->is_preview_mode() && $id == $footer_template){
        $show_footer = 'PREVIEW';
    }
    if(!$footer_template || $footer_template == ''){
        $show_footer = 'THEME';
    }

?>
<?php

if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'footers' ) ) {

  if($show_footer === 'ELEMENTOR' && $footer_template):
      ?>
        <footer>
      <?php
        echo \Elementor\Plugin::$instance->frontend->get_builder_content( $footer_template );
      ?>
      </footer>
      <?php
  elseif($show_footer === 'THEME'):
    
?>
<footer id="site-footer" class="elementor">
    <div class="elementor-section elementor-section-boxed">
        <div class="elementor-container site-footer-<?php echo $footer_layout; ?>">
        <?php
        
        if(file_exists(get_stylesheet_directory()."/templates/footer/footer-".$footer_layout.".php"))
            include(get_stylesheet_directory()."/templates/footer/footer-".$footer_layout.".php");
        else
            include(get_template_directory()."/templates/footer/footer-".$footer_layout.".php");
        ?>
        </div>
    </div>
</footer>
<?php
  endif;
}
?>
</div>
</div>
<?php wp_footer(); ?>

</body>
</html>
