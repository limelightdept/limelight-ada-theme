<?php
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}

get_header();
?>
<style>
	#payment-form-1766{display:none;}
</style>
<script>
	jQuery(function($){
		function changeSelect(){
			var el = $('[name="cert-dropdown"]');
			var selected = $(el).val();
			var all_ids = [ 1760, 1766 ];
			$.each(all_ids, function(){
				if(this != selected){
					$("#payment-form-" + this).hide();
				}else{
					$("#payment-form-" + this).show();
				}
			});
		}
		$('[name="cert-dropdown"]').on("change", changeSelect);
	});
</script>
    <main id="main">
        <div class="outer-grid">
			<div class="grid">
			<?php
			while (have_posts()) : the_post();
			?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
				</article>
			<?php
			endwhile;
			?>
			</div>
        </div>
    </main>
<?php
get_footer();
