(function($) {
    $(document).ready(function() {


        //Listen for the hooker init function so I can call my own init function with the hooker info
        document.addEventListener("ll_hooker_init", function(e, data) {
            init(e.detail);
        });

        function init(ll_hooker) {

            ll_hooker.addFilter("ll_form_data", "ll_forms", function(data, form) {
                domain_data = JSON.parse(php_var.domains);

                domain_arr = [];
                for (id in domain_data) {
                    var parts = domain_data[id]['url'].split("://");
                    var ob = {
                        "protocol": parts[0],
                        "domain": parts[1],
                        "last_scanned": domain_data[id]['last_scanned']
                    };
                    domain_arr.push(ob);
                }

                data.model = {
                    domains: domain_arr
                }

                return data;
            });
        };
    });
})(jQuery);