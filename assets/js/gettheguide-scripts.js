
jQuery(document).ready(function($){
	var scan_results = [];
	var domain = "";
	var protocol = "";
	var sitemap = "";
	var resetTimes = 0;
	
	$('[name="_wpcf7"][value="1621"]').closest('form').find('[type="submit"]').hide();
	
	var submitHtml = '<div class="submit flex right">'+
					'<div class="vc_btn3-container ada-button white medium">'+
					'<button class="wpcf7-form-control wpcf7-button vc_general vc_btn3 vc_btn3-size-md vc_btn3-color-ada-button">START NOW</button>'+
					'</div>'+
					'</div>';
	$('[name="_wpcf7"][value="1621"]').closest('form').append(submitHtml);
	
	submitHtml = '<div class="submit flex right">'+
				'<div class="vc_btn3-container ada-button white medium">'+
				'<button class="wpcf7-form-control wpcf7-button vc_general vc_btn3 vc_btn3-size-md vc_btn3-color-ada-button">SUBMIT</button>'+
				'</div>'+
				'</div>';
	$('[name="_wpcf7"][value="1609"]').closest('form').find('[type="submit"]').hide();
	$('[name="_wpcf7"][value="1609"]').closest('form').append(submitHtml);
	var labels = $('[name="_wpcf7"][value="1609"]').closest('form').find('label');	
	$.each(labels, function(){
		$(this).css('font-size', '15px').css('margin-top', '10px');
	});
	
	document.addEventListener( 'wpcf7mailsent', function( event ) {
		if(event.detail.contactFormId == "1621"){
			domain = event.detail.apiResponse.domain;
			if(domain.length > 1){ domain = domain.replace(/\/+$/, '') }
			protocol = event.detail.apiResponse.protocol[0];
			sitemap = event.detail.apiResponse.sitemap;
			//$(".content-body > .vc_row").remove();
			$('[name="_wpcf7"][value="1621"]').closest('[role="form"]').remove();
			$(".scanning-container").show();
			get_scan_results();
		}
		if(event.detail.contactFormId == "1609"){			
			if(!$.isEmptyObject(scan_results)){
				var ll_user_data = {};
				$.each(event.detail.inputs, function(index, input_data){
					ll_user_data[input_data.name] = input_data.value;
				});
				
				$.ajax({
					url: ajaxurl,
					type: "POST",
					data: {
						action: "ll_save_to_active_campaign",
						user_data: ll_user_data,
						list_id: '12',
						ac_tag: 'OADA Free Compliance Audit',
					},
					success: function(response){},
					error: function(){},
					complete: function(){}
				});
				
				$.ajax({
					url: ajaxurl,
					type: "POST",
					data: {
						results: scan_results.data,
						ll_user_data: ll_user_data,
						domain : protocol + "://" + domain,
						action: "free_scan"
					},
					success: function(response){
						//window.location.href = "/scan-complete/";
					}
				});
			}
		}
	}, false );
	
	function get_scan_results(){		
		sitemap = { "non_query_links": ["/"] };
		$.ajax({
			url: "https://scan.onlineada.com/webservice/v1/scan/" + 0 + "/0",
			type: "POST",
			dataType: "json",
			data: {
				sitemap: sitemap,
				protocol: protocol,
				domain: domain,
			},
			success: function(response){ console.log(response); },
			error: function(response){ $(".scanning-container").html('There was a problem scanning the domain ' + domain + '. Try refreshing the page and entering the domain again.');  },
			complete: function(response){
				if(!$.isEmptyObject(response.responseJSON.data)){
					scan_results = response.responseJSON;
					$(".scanning-container").hide();
					$(".form-container").show();
				}else{
					if(resetTimes == 10){
						$(".scanning-container").remove();
						$(".form-container").html("<h2>The page you entered had no relevant issues.</h2>");
						$(".form-container").show();
					}else{
						resetTimes++;
						get_scan_results();
					}
				}
			}
		});
	}
});
