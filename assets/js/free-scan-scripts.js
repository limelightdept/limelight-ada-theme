(function($) {
    console.log("access");
    const scan = create_scan();

    function create_scan() {
        let do_cbr = $('[name="cbr"]').prop("checked") ? true : false;
        let scan = {
            stop: false,
            resetTimes: 0,
            filter_step: 1,
            cbr_id: "",
            results: "",
            snapshot: "",
            current_step: 0,
            do_cbr: do_cbr,
            user_data: {},
            snapshot_url: php_var.remote_uri + "/webservice/v1/snapshot",
            message_log: "",
            steps: [],
            api_url: php_var.site_url + "/wp-json/limelight/v1/request_data?_wpnonce=" + php_var.api_nonce,
        }

        scan.next_step = function() {
            scan.current_step += 1;
            scan.do_scan_step(scan.current_step);
            return;
        }

        scan.exit = function() {
            scan.stop = true;
            return;
        }

        scan.build_scan_steps = function() {
            scan.steps.push({
                desc: "Start Scan",
                message: "<div>Processing your scan...</div>"
            });
            scan.steps.push({
                desc: "Start Scan homepage",
                message: "<div>Scanning your website...</div>"
            });
            scan.steps.push({
                desc: "End Scan homepage",
                message: "<div>Completed scanning your website</div>"
            });
            if (scan.do_cbr) {
                scan.steps.push({
                    desc: "Check for colorblind report",
                    message: "<div>Initiating color blind report...</div>"
                });
                scan.steps.push({
                    desc: "Do filters",
                    message: "<div>Generating filter <span class='step-left'>1</span><span class='step-mid'>/</span><span class='step-right'>9</span></div>"
                });

                scan.steps.push({
                    desc: "Complete CBR",
                    message: "<div>Color blind report complete</div>"
                });
            } else {
                scan.steps.push({
                    desc: "Check for colorblind report",
                    message: "<div>Skipping color blind report</div>"
                });
            }
            scan.steps.push({
                desc: "Taking screenshot",
                message: "<div>Taking a screenshot of your homepage</div>"
            });
            scan.steps.push({
                desc: "Complete scan",
                message: "<div>Scan complete</div>"
            });
            return;
        }

        scan.do_scan_step = function(step) {
            step = scan.steps[step];
            if (!scan.stop) {
                switch (step.desc) {
                    case "Select Options":
                        break;
                    case "Start Scan":
                        scan.message_log.append(step.message);
                        scan.next_step();
                        break;
                    case "Start Scan homepage":
                        scan.message_log.append(step.message);
                        scan.call_scan_api();
                        break;
                    case "End Scan homepage":
                        scan.message_log.append(step.message);
                        scan.next_step();
                        break;
                    case "Check for colorblind report":
                        scan.message_log.append(step.message);
                        scan.next_step();
                        break;
                    case "Do filters":
                        scan.message_log.append(step.message);
                        scan.get_first_filter();
                        break;
                    case "Complete CBR":
                        scan.message_log.append(step.message);
                        scan.next_step();
                        break;
                    case "Taking screenshot":
                        scan.message_log.append(step.message);
                        scan.get_domain_screenshot();
                        break;
                    case "Complete scan":
                        scan.message_log.hide();
                        $('#copy-container').addClass("neg-margin");
                        let processing_html = "<div class='processing'>";
                        processing_html += "<div class='header'>Processing...</div>";
                        processing_html += "<div class='loader loading'></div>";
                        processing_html += "<div class='comment'>(Do not navigate away from this page. You will be redirected momentarily.)</div>";
                        processing_html += "</div>";
                        scan.message_log.parent().html(processing_html);
                        scan.mapUserdata(scan.vm);
                        scan.submit_user();
                        break;
                }
            }
            return;
        }

        scan.call_scan_api = function() {
            $.ajax({
                url: scan.api_url,
                data: {
                    domain: scan.domain,
                    protocol: scan.protocol
                },
                method: "POST",
                success: function(re) {
                    let response = JSON.parse(re);
                    scan.results = response.data;
                    if (!$.isEmptyObject(response.data)) {
                        return scan.next_step();
                    } else {
                        if (scan.resetTimes === 10) {
                            $(".scanning-container").html("<h2>The page you entered had no relevant issues.</h2>");
                            return;
                        } else {
                            scan.resetTimes++;
                            scan.call_scan_api();
                            return;
                        }
                    }
                },
                error: function(re) {
                    scan.message_log.append("<div>There was a problem with the scan.</div>");
                    scan.message_log.append("<div>" + re.responseJSON.msg + "</div>");
                    return;
                }
            });
        }

        scan.get_first_filter = function() {
            let url = php_var.remote_uri + "/webservice/v1/cbc/" + 'free' + "/init";

            $.ajax({
                url: url,
                type: "POST",
                data: {
                    domain: scan.protocol + "://" + scan.domain
                },
                success: function(response) {
                    scan.cbr_id = response.id;

                    scan.get_filter(scan.filter_step);
                },
                error: function(response) {
                    scan.message_log.append("Something has gone wrong with generating the colorblind report. Check the console for more information");
                    console.log(response);
                    return scan.exit();
                }
            });
        }
        scan.get_filter = function(step) {
            let url = php_var.remote_uri + "/webservice/v1/cbc/" + scan.cbr_id + "/" + step + "/create_filter";

            $.ajax({
                url: url,
                type: "GET",
                data: {
                    domain: scan.protocol + "://" + scan.domain
                },
                success: function(response) {
                    if (scan.filter_step !== 9) {
                        scan.filter_step++;
                        $("#message-log .step-left").html(scan.filter_step);
                        scan.get_filter(scan.filter_step);
                        return;
                    }
                    if (scan.filter_step === 9) {
                        return scan.next_step();
                    }
                },
                error: function(response) {
                    scan.message_log.append("Something has gone wrong with generating the colorblind report. Check the console for more information");
                    console.log(response);
                    return scan.exit();
                }
            });
        }

        scan.get_domain_screenshot = function() {
            $.ajax({
                url: scan.snapshot_url,
                method: "POST",
                data: {
                    domain: scan.protocol + "://" + scan.domain,
                    height: 800
                },
                success: (re) => {
                    scan.snapshot = re;
                    return scan.next_step();
                },
            });
        }

        scan.submit_snapshot = function() {
            //Save homepage snapshot

            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: {
                    action: "save_snapshot",
                    b64: scan.snapshot,
                    domain_id: scan.domain_id,
                    user_id: scan.user_id,
                },
                success: function(re) {
                    scan.complete_scan();

                },
                error: function(re) {

                }
            });
        }

        scan.complete_scan = function() {
            //complete_audit
            $.ajax({
                url: ajaxurl,
                data: {
                    action: 'complete_audit',
                    domain_id: scan.domain_id
                },
                success: function() {
                    window.location.href = "/scan-complete/";
                }
            });
        }

        scan.submit_user = function() {
            $.ajax({
                url: ajaxurl,
                type: "POST",
                data: {
                    user_data: scan.user_data,
                    domain: scan.domain,
                    protocol: scan.protocol,
                    action: "free_scan"
                },
                success: function(response) {
                    scan.domain_id = response.domain_id;
                    scan.user_id = response.user_id;

                    scan.submit_scan_results();
                }
            });
        }

        scan.submit_scan_results = function() {
            $.ajax({
                url: ajaxurl,
                type: "POST",
                data: {
                    results: scan.results,
                    page: "/",
                    action: "save_scan_results",
                    domain_id: scan.domain_id,
                    cbr_id: scan.cbr_id,
                    domain: scan.domain,
                    user_id: scan.user_id,
                },
                success: function(response) {
                    console.log(response);
                    scan.submit_snapshot();
                }
            });

        }

        scan.mapUserdata = function(vm) {
            scan.user_data["user_name"] = vm.model.user_name;
            scan.user_data["user_email"] = vm.model.user_email;
            scan.user_data["user_password"] = vm.model.user_password;
        }

        return scan;
    }



    //Listen for the hooker init function so I can call my own init function with the hooker info
    document.addEventListener("ll_hooker_init", function(e, data) {
        init(e.detail);
    });

    function init(ll_hooker) {
        //Add action for when all forms have loaded
        ll_hooker.addAction("ll_forms_init", "ll_forms", function(vm) {
            initialize();
        });

        //Add action for when my custom event fires, which happens after validation is successful
        ll_hooker.addAction("ll_form_event_custom_fired", "ll_forms", function(vm) {
            //vm is the entire vue component. It contains the backend vue model and the frontend form info

            scan.vm = vm;
            scan.domain = vm.model["domain-1"].replace(/\/+$/, '');
            scan.protocol = vm.model.protocol;
            scan.do_cbr = $("#ll-form-3992 [name='cbr']").prop("checked") ? true : false;
            $(vm.$parent.$el).remove();
            scan.message_log.show();
            $(".scanning-container").find("h2").remove();
            scan.build_scan_steps();
            scan.do_scan_step(scan.current_step);
        });

        function initialize() {
            $(".v-select .form-control").prop("disabled", "true").prop("readOnly", true);
            $(".scanning-container").prepend("<div id='message-log'></div>");
            scan.message_log = $("#message-log");
            scan.form = $("#ll-form-3992").find("form");

            //Affected by Elementor and Limelight Checkout
            if (php_var.logged_in) {
                $(".scanning-container").html("<div>You must first log out before using this tool</div>");
                scan.form.remove();
            } else {

                const inputs = scan.form.find("[type=text]");
                $.each(inputs, function(index, input) {
                    $(input).attr("aria-required", "true");
                });

                let labels = scan.form.find('label');
                $.each(labels, function() {
                    $(this).css('font-size', '15px').css('margin-top', '10px');
                });

            }
        }
    };

})(jQuery)