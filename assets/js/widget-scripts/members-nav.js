(function($) {
    $(document).ready(function() {
        if (!$(".elementor-widget-onlineada-membersnav .mobile-menu-button").length) {
            var observer = new MutationObserver(function(mutations) {
                mutations.forEach(function(mutation) {
                    for (var i = 0; i <= mutation.addedNodes.length; i++) {
                        if ($(mutation.addedNodes[i]).hasClass("elementor-inner")) {

                            var subObserver = new MutationObserver(function(submutations) {
                                submutations.forEach(function(submutation) {
                                    for (var i = 0; i <= submutation.addedNodes.length; i++) {
                                        if ($(submutation.addedNodes[i]).hasClass("elementor-widget-container") &&
                                            $(submutation.addedNodes[i]).children(".mobile-menu-button-wrapper.limelight-navigation").length) {
                                            init();
                                            subObserver.disconnect();
                                            observer.disconnect();
                                        }
                                    }
                                });
                            });

                            var subNode = $(mutation.addedNodes[i])[0];
                            subObserver.observe(subNode, {
                                childList: true,
                                subtree: true
                            });

                        }
                    }
                });
            });
            var node = document.getElementsByTagName("body")[0];
            observer.observe(node, {
                childList: true,
                subtree: true
            });
        } else {
            init();
        }

        function init() {
            $(".elementor-widget-onlineada-membersnav").on("click", ".mobile-menu-button", function(e) {
                setNavHeight();
                var widget = $(this).closest('.elementor-widget-onlineada-membersnav');
                widget.toggleClass('open');

            });

            function setNavHeight() {
                var height = $(".elementor-widget-onlineada-membersnav .collapsible-container .mobile-menu.limelight-navigation .nav-mobile").outerHeight();

                $(".elementor-widget-onlineada-membersnav .collapsible-container").attr("style", "height:" + height + "px");
            }

            var width = $('.members-nav-left-container').outerWidth();
            $('.elementor-widget-onlineada-membersnav nav').css("width", width);

            var nav = $('.elementor-widget-onlineada-membersnav nav');
            var column = $(nav).closest('.elementor-top-column');
            var width = $(column).outerWidth();
            nav.css("width", width);
        }

    });

})(jQuery);