jQuery(document).ready(function($) {
    var form = $("#sitemap");
    $(".body-wrap").on("change", "[name='website-select']", function() {
        var option = $(this).find(":selected");
        form.attr("action", "/agency/reports/" + window.php_var.user_id + "/" + option.val());
    });

    $("[name='website-select']").trigger("change");
});