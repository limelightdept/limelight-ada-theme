(function($) {
    $(document).ready(function() {
        var form = $("form");
        $(".body-wrap").on("change", "[name='website-select']", function() {
            var option = $(this).find(":selected");
            form.attr("action", "/agency/new-scan/" + option.val() + "/options");
            $("#create").prop("checked", "true");
        });

        $("[name='website-select']").trigger("change");
    });
})(jQuery);