//This script is for the Sitemap Review page
jQuery(document).ready(function($) {
    function escapeHtml(string) {
        var entityMap = {
            //'&': '&amp;',
            '&': '&',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#39;',
            '/': '&#x2F;',
            '`': '&#x60;',
            '=': '&#x3D;'
        };
        return String(string).replace(/[&<>"'`=\/]/g, function(s) {
            return entityMap[s];
        });
    }
    var sitemap_empty = php_var.empty_sitemap;
    var sitemap = php_var.sitemap;
    console.log(php_var);
    var sitemap_list_parent = $(".sitemap");
    refresh_sitemap();

    function populate_sitemap_list() {
        var sitemap_list = "";

        if (sitemap_empty !== false) {
            $.each(sitemap.non_query_links, function(index, value) {
                sitemap_list += "<li name='non_query_links' ><p>" + escapeHtml(this) + "</p><a href='#' data-index='" + index + "' class='sitemap-remove' aria-label='Remove from sitemap'>Remove</a></li>";
            });

            $.each(sitemap.query_links, function(index, value) {
                sitemap_list += "<li name='query_links' ><p>" + escapeHtml(this) + "</p><a href='#' data-index='" + index + "' class='sitemap-remove' aria-label='Remove from sitemap'>Remove</a></li>";
            });
        } else {
            var message = "<div>Something went wrong.</div><a style='color:#3F3244;' href='/members/sitemap' >Go Back</a></div>";
            sitemap_list_parent.closest(".body-top").html(message);
            $(".body-top").addClass("msg");
            $(".sitemap-add").removeClass("sitemap-add").addClass("disabled");
            $('[name="add"]').attr("disabled", "true");
            $(".disabled").click(function(e) {
                e.preventDefault();
            });
        }

        sitemap_list_parent.html(sitemap_list);

        $("#sitemap-review [name='list']").val(JSON.stringify(sitemap));
        $("[name='add']").val("");
    }

    function add_to_sitemap(link) {
        link = "/" + link;
        if (link.indexOf("?") >= 0) {
            sitemap.query_links.push(link);
        } else {
            sitemap.non_query_links.push(link);
        }
    }

    function refresh_sitemap() {
        sitemap_list_parent.html("");
        populate_sitemap_list();
    }

    $(".sitemap").on("click", "li .sitemap-remove", function(e) {
        e.preventDefault();
        var list = $(this).closest("li").attr("name");
        var index = $(this).attr("data-index");
        sitemap[list].splice(index, 1);

        refresh_sitemap();
    });

    $(".sitemap-add").on("click", function(e) {
        e.preventDefault();
        add_to_sitemap($("[name='add']").val());
        refresh_sitemap();
    });

    $("[name='add']").keypress(function(e) {
        var key = e.which;
        if (key === 13) {
            $(".sitemap-add").trigger("click");
            return false;
        }
    });
});