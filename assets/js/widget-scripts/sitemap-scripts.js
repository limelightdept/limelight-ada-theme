jQuery(document).ready(function($) {
    var upload_wrap = $("#fileUpload").closest("div");
    showHideFileUpload();

    function showHideFileUpload() {
        if ($("#upload").prop("checked")) {
            upload_wrap.show();
        } else {
            upload_wrap.hide();
        }
    }
    $('input[type="radio"]').on("change", function() {
        var file = $("#fileUpload")[0].files[0];
        showHideFileUpload();
    });

    $(".submit-button").on("click", validate_uploaded_file);

    var form = $("#sitemap");

    function validate_uploaded_file(e) {
        e.preventDefault();
        if ($("[name='_sitemap']:checked").val() == "upload") {
            var file = $("#fileUpload")[0].files[0];
            if (file) {
                if (window.File && window.FileReader && window.FileList && window.Blob) {
                    var supportedFormats = ['csv', 'xml'];

                    if (file.size > 1048576) { //Do something if file upload size is larger than 1mb
                        alert(file.size + " bites\nToo big");
                    }
                    var result = file.name.split(".");
                    var ext = result[result.length - 1];
                    if (supportedFormats.includes(ext)) {
                        form.submit();
                    } else {
                        $(".msg").html("The file type of ." + ext + " is not supported.");
                    }

                }
            } else {
                $(".msg").html("Please select a file to upload");
            }
        } else {
            form.submit();
        }

    }

    $(".body-wrap").on("change", "[name='website-select']", function() {
        var option = $(this).find(":selected");
        if (option.attr("data-has-sitemap") == "true") {
            $("#existing").closest("div").show();
        } else {
            $("#existing").closest("div").hide();
        }
        form.attr("action", "/agency/sitemap/" + option.val() + "/review");
        $("#create").prop("checked", "true");
    });

    $("[name='website-select']").trigger("change");

});