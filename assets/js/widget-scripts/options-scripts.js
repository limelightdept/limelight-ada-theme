jQuery(document).ready(function($) {

    const scan = {
        stop: false,
        do_qs: 1,
        do_cbr: 0,
        current_step: 0,
        filter_step: 1,
        api_remote_base: php_var.api_remote_base,
        user_id: php_var.user_id,
        domain_id: php_var.domain_id,
        snapshot_url: php_var.api_remote_base + "/snapshot",
        message_log: $(".scan-message .message"),
        steps: [],
        api_url: php_var.this_site + "/wp-json/limelight/v1/request_data?_wpnonce=" + php_var.api_nonce,
        processed_pages: 0,
        go_to_summary: function() {
            $.ajax({
                url: ajaxurl,
                data: {
                    action: "complete_audit",
                    domain_id: scan.domain_id
                },
                success: function() {}
            });
            return window.location = php_var.this_site + "/agency/reports/" + this.user_id + "/" + this.domain_id;
        }
    };

    scan.exit = function() {
        scan.stop = true;
    };
    scan.next_step = function() {
        scan.current_step += 1;
        do_scan_step(scan.current_step);
    };
    scan.sitemap = [];
    php_var.sitemap["non_query_links"].forEach(function(value) {
        scan.sitemap.push(value);
    });

    $('[name="use_query_strings"]').on("change", function() {
        scan.do_qs = parseInt($(this).prop("checked", "true").val());
    });
    $('[name="use_color_blind"]').on("change", function() {
        scan.do_cbr = parseInt($(this).prop("checked", "true").val());
    });

    $(".start-scan").on("click", function(e) {
        e.preventDefault();
        $(".remove").remove();
        $(".scan-message .load").addClass("loader");

        start_scan();
    });

    function start_scan() {
        build_scan_steps();
        if (scan.do_qs) {
            php_var.sitemap["query_links"].forEach(function(value) {
                scan.sitemap.push(value);
            });
        }
        scan.total_pages = scan.sitemap.length;

        delete_previous_results();
    }

    function delete_previous_results() {
        $.ajax({
            url: ajaxurl,
            data: {
                action: 'delete_previous_results',
                domain_id: scan.domain_id,
                using_qs: scan.do_qs
            },
            success: function() {
                do_scan_step(scan.current_step);
            }
        });
    }

    function build_scan_steps() {

        scan.steps.push({
            desc: "Start Scan",
            message: "<div>Please be patient while we process your scan...</div><div>(This may take a few minutes)</div>"
        });

        if (scan.do_qs) {
            scan.steps.push({
                desc: "Check for query strings",
                message: "<div>Use pages with query strings is true</div>"
            });
        } else {
            scan.steps.push({
                desc: "Check for query strings",
                message: "<div>Use pages with query strings is false. Omitting pages with query strings</div>"
            });
        }
        scan.steps.push({
            desc: "Start Scan pages",
            message: "<div>Scanning your site...</div>"
        });
        scan.steps.push({
            desc: "End Scan pages",
            message: "<div>Completed scanning your site</div>"
        });

        if (scan.do_cbr) {
            scan.steps.push({
                desc: "Check for colorblind report",
                message: "<div>Initiating color blind report...</div>"
            });
            scan.steps.push({
                desc: "Do filters",
                message: "<div>Generating filter <span class='step-left'>1</span><span class='step-mid'>/</span><span class='step-right'>9</span></div>"
            });

            scan.steps.push({
                desc: "Create CBR PDF",
                message: "<div>Combining filters into 1 pdf</div>"
            });
            scan.steps.push({
                desc: "Complete CBR",
                message: "<div>Color blind report complete</div>"
            });
        } else {
            scan.steps.push({
                desc: "Check for colorblind report",
                message: "<div>Skipping color blind report</div>"
            });
        }

        scan.steps.push({
            desc: "Taking screenshot",
            message: "<div>Taking a screenshot of your homepage</div>"
        });
        scan.steps.push({
            desc: "Complete scan",
            message: "<div>Scan complete</div>"
        });

    }

    function do_scan_step(step) {
        step = scan.steps[step];
        if (!scan.stop) {
            switch (step.desc) {
                case "Start Scan":
                    scan.message_log.append(step.message);
                    scan.next_step();
                    break;
                case "Check for query strings":
                    scan.message_log.append(step.message);
                    scan.next_step();
                    break;
                case "Start Scan pages":
                    scan.message_log.append(step.message);
                    scan.message_log.append("<div>Processing " + scan.total_pages + " pages</div>");
                    scan.message_log.append("<div>Current page: <span class='current-scan-page'></span> ( <span class='pages-left'>0</span><span> of </span><span class='pages-right'>" + scan.total_pages + "</span> )</div>");

                    call_scan_api();
                    break;
                case "End Scan pages":
                    scan.message_log.append(step.message);
                    scan.next_step();
                    break;
                case "Check for colorblind report":
                    scan.message_log.append(step.message);
                    scan.next_step();
                    break;
                case "Do filters":
                    scan.message_log.append(step.message);
                    get_first_filter();
                    break;
                case "Create CBR PDF":
                    scan.message_log.append(step.message);
                    combine_filters();
                    break;
                case "Complete CBR":
                    scan.message_log.append(step.message);
                    scan.next_step();
                    break;
                case "Taking screenshot":
                    scan.message_log.append(step.message);
                    get_domain_screenshot();
                    break;
                case "Complete scan":
                    scan.message_log.append(step.message);
                    scan.message_log.append("<div>All reports complete</div>");
                    scan.message_log.append("<div>Redirecting to summary</div>");
                    scan.go_to_summary();
                    break;
            }
        }

    }

    function call_scan_api() {
        //If the sitemap is empty, go to next step
        if (scan.sitemap.length === 0) {
            return scan.next_step();
        }
        let page = scan.sitemap.splice(0, 1)[0];
        $('.current-scan-page').text(page);
        scan.processed_pages++;
        $('.pages-left').text(scan.processed_pages);

        $.ajax({
            url: scan.api_url,
            data: {
                page: page,
                domain_id: scan.domain_id
            },
            method: "POST",
            success: function(re) {
                let response = JSON.parse(re);
                if (!$.isEmptyObject(response.data)) {
                    save_results(response.data, page, response.scan_type);
                } else {
                    return call_scan_api();
                }
            },
            error: function(re) {
                scan.message_log.append("<div>There was a problem with the scan.</div>");
                scan.message_log.append("<div>" + re + "</div>");
                return scan.exit();
            }
        });
    }

    function save_results(toSend, page, scan_type) {
        var data = {
            domain_id: scan.domain_id,
            results: toSend,
            page: page,
            action: "save_scan_results",
        };

        $.ajax({
            url: ajaxurl,
            type: "POST",
            data: data,
            complete: function(response) {
                if (response.responseText === "success") {
                    if (scan_type == "free") {
                        //Page was transformed to be "/" by the API, so move to next step
                        return scan.next_step();
                    } else {
                        //Keep going through the sitemap
                        return call_scan_api();
                    }
                }
                //There was a problem storing the data
            }
        });
    }

    function get_first_filter() {
        let url = this.api_remote_base + "/cbc/" + scan.domain_id + "/init";
        $.ajax({
            url: url,
            type: "POST",
            data: {
                domain: php_var.homepage
            },
            success: function(response) {
                get_filter(scan.filter_step);
            },
            error: function(response) {
                scan.message_log.append("Something has gone wrong with generating the colorblind report. Check the console for more information");
                console.log(response);
                return scan.exit();
            }
        });
    }

    function get_filter(step) {
        let url = this.api_remote_base + "/cbc/" + scan.domain_id + "/" + step + "/create_filter";

        $.ajax({
            url: url,
            type: "GET",
            data: {
                domain: php_var.homepage
            },
            success: function(response) {
                save_filter(response);
            },
            error: function(response) {
                scan.message_log.append("Something has gone wrong with generating the colorblind report. Check the console for more information");
                console.log(response);
                return scan.exit();
            }
        });
    }

    function save_filter(response) {
        $.ajax({
            url: ajaxurl,
            type: "POST",
            data: {
                action: 'save_filter',
                step: scan.filter_step,
                homepage: php_var.homepage,
                domain_id: php_var.domain_id,
                response: response,
            },
            success: function() {
                if (scan.filter_step !== 9) {
                    scan.filter_step++;
                    $(".scan-message .message .step-left").html(scan.filter_step);
                    get_filter(scan.filter_step);
                }
                if (scan.filter_step === 9) {
                    return scan.next_step();
                }
            },
            error: function(re) {
                scan.message_log.append("Something has gone wrong with generating the colorblind report. Check the console for more information");
                console.log(re);
                return scan.exit();
            }
        });
    }

    function combine_filters() {
        $.ajax({
            url: ajaxurl,
            method: "POST",
            data: {
                action: "combine_filters",
                domain_id: scan.domain_id,
                domain: php_var.homepage
            },
            success: function(re) {
                return scan.next_step();
            },
            error: function(re) {
                scan.message_log.append("Something has gone wrong with generating the colorblind report. Check the console for more information");
                console.log(re);
                return scan.exit();
            }
        });
    }

    function get_domain_screenshot() {
        $.ajax({
            url: scan.snapshot_url,
            method: "POST",
            data: {
                domain: php_var.homepage,
                height: 800
            },
            success: (re) => {
                $.ajax({
                    url: ajaxurl,
                    method: "POST",
                    data: {
                        action: "save_snapshot",
                        b64: re,
                        user_id: scan.user_id,
                        domain: php_var.homepage
                    },
                    success: function(re) {
                        return scan.next_step();
                    },
                    error: function(re) {
                        scan.message_log.append("Something has gone wrong with creating a snapshot of your site. Check the console for more information");
                        console.log(re);
                        return scan.next_step();
                    }
                });
            },
        });
    }
});