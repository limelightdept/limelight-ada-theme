jQuery(document).ready(function($){
	$('body').on("click", '#bulk-license-button a', function(e){
		$(this).attr("href", "//onlineada.com/multi-compliance-signup/?license_option=" + $('[name="bulk-license-option"]').val());
	});
	$('body').on("change", "#bulk-license-body > select", function(){
		if($(this).val() == "10"){
			$("#bulk-license-price").text("$49");
		}else if($(this).val() == "5"){
			$("#bulk-license-price").text("$59");
		}
	});
});
