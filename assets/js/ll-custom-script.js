jQuery(document).ready(function($) {
    setTimeout(function() {
        var height = $("#main").height() + 50 + "px";
        $("#mobile-nav-height").html("body.mobile_nav_open .members-dashboard #mobile-nav{height:" + height + ";}");
    }, 500);

    function showHideShippingAddress($result) {
        if ($result === "No") {
            $(".shipping-address-wrap").css("display", "initial");
        } else {
            $(".shipping-address-wrap").css("display", "none");
        }
    }

    setTimeout(function() {
        $("#apply-now>a").attr("title", $("#apply-now > a").text());
        $(".ada-button > .ada-button").removeClass("ada-button");
        var $blogImg = $(".blog-featured-img a, .blog-img a");
        if ($blogImg) {
            $blogImg.on("mouseover focusin", function() {
                if (!$(this).closest(".blog-featured-img, .blog-img").hasClass("focused")) {
                    $(this).closest(".blog-featured-img, .blog-img").addClass("focused");
                }
                if ($(this).closest(".vc_grid-item-mini")) {
                    if (!$(this).closest(".vc_grid-item-mini").hasClass("vc_is-hover")) {
                        $(this).closest(".vc_grid-item-mini").addClass("vc_is-hover")
                    }
                }
            });
            $blogImg.on("mouseout focusout", function() {
                if ($(this).closest(".blog-featured-img, .blog-img").hasClass("focused")) {
                    $(this).closest(".blog-featured-img, .blog-img").removeClass("focused");
                }
                if ($(this).closest(".vc_grid-item-mini")) {
                    if ($(this).closest(".vc_grid-item-mini").hasClass("vc_is-hover")) {
                        $(this).closest(".vc_grid-item-mini").removeClass("vc_is-hover")
                    }
                }
            });
        }

        var $adaButton = $(".ada-button a, .ada-button button");
        if ($adaButton) {
            $adaButton.on("mouseover focusin", function() {
                if (!$(this).closest(".ada-button").hasClass("focused")) {
                    $(this).closest(".ada-button").addClass("focused");
                }
                if ($(this).closest(".vc_grid-item-mini")) {
                    if (!$(this).closest(".vc_grid-item-mini").hasClass("vc_is-hover")) {
                        $(this).closest(".vc_grid-item-mini").addClass("vc_is-hover")
                    }
                }
            });
            $adaButton.on("mouseout focusout", function() {
                if ($(this).closest(".ada-button").hasClass("focused")) {
                    $(this).closest(".ada-button").removeClass("focused");
                }
                if ($(this).closest(".vc_grid-item-mini")) {
                    if ($(this).closest(".vc_grid-item-mini").hasClass("vc_is-hover")) {
                        $(this).closest(".vc_grid-item-mini").removeClass("vc_is-hover")
                    }
                }
            });
        }
    }, 1000);

    $(".ada-button .vc_btn3-size-lg").parent(".ada-button").addClass("large");
    $(".ada-button .vc_btn3-size-md").parent(".ada-button").addClass("medium");
    $(".ada-button .vc_btn3-size-sm").parent(".ada-button").addClass("small");

    $(".cat-item").on("click", function(e) {
        e.preventDefault();
        var classTerm = $(this).attr("class").split(" ")[1];
        var term_id = classTerm.split("-")[2];
        var filter = $(".vc_grid-filter-item span[data-vc-grid-filter-value='.vc_grid-term-" + term_id + "']");
        var filter = $(filter).closest("li").trigger("click");
    });

    $('.domain-input').each(function() {
        var protocol_select = '<select class="ll-input protocol-select" name="protocol[]" ><option value="https">https://</option><option value="http">http://</option></select>';
        $(this).parent().addClass('flex-no-wrap').prepend(protocol_select);
    });

    $(".members-dashboard .logo a").attr("aria-label", "Online ada home");

});