<?php
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
$user = wp_get_current_user();
if(!in_array("administrator", $user->roles)){
	wp_redirect( home_url("/") );
}

get_header();

?>
<script>
jQuery(function($){
	
	$("input").val("https://facebook.com");
	
	$(".test-submit").on("click", function(){
		$.ajax({
			url: "https://scan.onlineada.com/webservice/v1/cbc/init",
			type: "POST",
			data: {
				domain: $(".ll-input").val(),
			},
			success: function(response){
				if(response.status === "success"){
					$.ajax({
						url: "https://scan.onlineada.com/webservice/v1/cbc/final",
						data: {
							b64_id: response.b64_id,
						},
						type: "POST",
						success: function(re){
							$(".cbc").attr("src", "data:image/png;base64, "+ re);
						}
					});
				}
			},
		});
	});
	
});
</script>
    <main id="main">
        <div class="outer-grid">
			<div class="grid">
				<img src='<?php echo $pub_path; ?>' >
			<?php
			//while (have_posts()) : the_post();
			?>
				<!--<article id="post-<?php //the_ID(); ?>" <?php //post_class(); ?>>
					<div class="entry-content">
						<?php ///the_content(); ?>
					</div>
				</article>-->
			<?php
			//endwhile;
			?>
			</div>
        </div>
    </main>
<?php
get_footer();
