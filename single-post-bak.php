<?php
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
get_header();
$facebook = 'https://www.facebook.com/sharer/sharer.php?u=';
$twitter = 'https://twitter.com/intent/tweet?url=';
?>
    <div id="main">
        <div class="outer-grid">
        <div class="grid">
<?php
while (have_posts()) : the_post();
    $author_id = get_the_author_meta("ID");
?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div id="blog-single" class="entry-content dark">
					<div class="content-header" style="text-align:left;">
						<div class="featured-img-wrapper" style="background-image:url(<?php the_post_thumbnail_url(); ?>);background-size:cover;background-repeat: no-repeat;"></div>
						<div class="content-header-bottom">
							<div class="content-title"><?php the_title(); ?></div>
							<hr class="divider" />
							<div class="flex">
								<span class="content-date"><?php the_date() ?></span>
								<span class="content-author"><?php the_author() ?></span>
								<div class="flex content-share">
									<span style="padding-right:10px;">Share: </span>
									<span> <a href="<?php echo $twitter . urlencode(get_permalink(get_the_ID())) ?>&text=<?php echo urlencode(get_the_title()); ?>"><img alt="Share this article on Twitter" src="<?php echo wp_get_attachment_image_src(385, "thumbnail", true)[0]; ?>"></a> </span>
									<span> <a href="<?php echo $facebook . urlencode(get_permalink(get_the_ID())) ?>"><img alt="Share this article on Facebook" src="<?php echo wp_get_attachment_image_src(384, "thumbnail", true)[0]; ?>"></a> </span>
								</div>
							</div>
						</div>
						<hr class="divider full" />
					</div>
					<main class="vc_col-sm-8">
						<?php the_content(); ?>
					</main>
                    <?php get_template_part("sidebar"); ?>
					<script>
						jQuery.each(jQuery(".ll-categories"), function(){
							if(jQuery(this).find(".widgettitle").text() == "Categories"){
								jQuery(this).hide();
							}
						});
					</script>
                    <div class="content-footer hidden">
                        <?php
                        echo do_shortcode('[vc_row content_placement="middle" el_class="subscribe-block" css=".vc_custom_1521065790330{padding-top: 30px !important;padding-bottom: 30px !important;}"][vc_column css=".vc_custom_1521066921025{padding-right: 45px !important;padding-left: 45px !important;}" el_class="align-center"][vc_column_text el_class="subscribe-block-header"]Subscribe to Receive Updates[/vc_column_text][contact-form-7 id="372"][/vc_column][/vc_row]'); ?>
                        <div class="author-wrapper">
                            <div class="author-header">About the Author</div>
                            <div class="flex">
                                <span class="author-img"><?php echo get_avatar($author_id, 96, '', 'Vanessa Salvia'); ?></span>
                                <span class="author-blurb">
                                    <div class="author-name"><?php the_author(); ?></div>
                                    <hr class="divider">
                                    <div class="author-info"><?php echo get_the_author_meta("description", $author_id); ?></div>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </article>			
<?php
endwhile;
?>
        </div>
        </div>
    </div>
<?php
get_footer();
