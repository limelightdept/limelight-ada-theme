<?php

if ( ! is_active_sidebar( 'limelight_sidebar' ) ) {
	return;
}
?>

<aside id="blog-sidebar" class="widget-area vc_col-sm-4" role="complementary" aria-label="Blog Sidebar">
	<?php dynamic_sidebar( 'limelight_sidebar' ); ?>
</aside>
