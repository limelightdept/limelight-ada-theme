<?php
/* Template Name: Shipping Form */
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
get_header();
?>
    <main id="main">
        <div class="outer-grid">
        <div class="grid">
<?php
while (have_posts()) : the_post();
$user = wp_get_current_user();
$user_shipping = get_user_meta($user->ID, "_limelight_shipping_data", true);
$user_shipping = json_encode($user_shipping);
?>

<script type="text/javascript">
    var shipping_info = <?php echo $user_shipping ?>;
    setTimeout(function(){
        setDefaultFieldValues();
    }, 100);

    var setDefaultFieldValues = function(){
        var businessName = jQuery("#business-name");
        var city = jQuery("#city");
        var street = jQuery("#street");
        var zip = jQuery("#zip");
        var phone = jQuery("#phone");
        var state = jQuery("#state");
        var webDomain = jQuery("#web-domain");

        city.val(shipping_info.city);
        state.val(shipping_info.state);
        street.val(shipping_info.street);
        zip.val(shipping_info.zip);
        phone.val(shipping_info.phone);
        businessName.val(shipping_info["business-name"]);
        webDomain.val(shipping_info["web-domain"]);
    }

</script>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="entry-content">
            <?php the_content(); ?>
        </div>
    </article>
<?php
endwhile;
?>
        </div>
        </div>
    </main>
<?php
get_footer();
