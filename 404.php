<?php
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
get_header();
//404 page ID = 1296
$page = get_post(1296);
?>
    <main id="main">
        <div class="outer-grid">
			<div class="grid">
				<?php echo apply_filters("the_content", $page->post_content); ?>
			</div>
        </div>
    </main>
<?php
get_footer();
