<?php
/* Template Name: Content Only */
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
get_header();

?>
    <div id="main">
        <div class="outer-grid">
        <div class="grid">
            <?php
            while (have_posts()) : the_post();
            ?>
                <main>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <div class="entry-content">
                            <div class="content-wrapper dark">
                                <div class="content-header">
                                    <div class="content-header-bottom">
                                        <div class="content-title"><?php the_title(); ?></div>
                                    </div>
                                    <hr class="divider full" />
                                </div>
                                <div class="content-body">
                                    <?php the_content() ?>
                                </div>
                            </div>
                        </div>
                    </article>
                </main>
            <?php
            endwhile;
            ?>
        </div>
        </div>
    </div>
<?php
get_footer();
