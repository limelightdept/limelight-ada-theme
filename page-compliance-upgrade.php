<?php
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
if(!is_user_logged_in() AND !in_array("administrator", wp_get_current_user()->roles)){
	wp_redirect( home_url("compliance-login-upgrade") );
	exit;
}
get_header();
$license_option = get_query_var("license_option");

?>
<style>
	#payment-form-1742{display:none;}
	#payment-form-1743{display:none;}
</style>
<script>
	jQuery(function($){
		<?php
			echo "var pre_selected = '';";
			if(!is_null($license_option)){
				switch($license_option){
					case "5":
						echo "pre_selected = '1742';";
						break;
					case "10":
						echo "pre_selected = '1743';";
						break;
				}
			}
			
			if( is_null($license_option) ){
				echo "pre_selected = '1741';";
			}
		?>
		$('[name="license-dropdown"] option').each(function(){
			if($(this).val() == pre_selected){
				$(this).prop("selected", "true");
				changeSelect();
			}	
		});
		
		/*
		//1741 = single
		//1742 = 5
		//1743 = 10
		*/
		function changeSelect(){
			var el = $('[name="license-dropdown"]');
			var selected = $(el).val();
			var all_ids = [ 1741, 1742, 1743 ];
			$.each(all_ids, function(){
				if(this != selected){
					$("#payment-form-" + this).hide();
				}else{
					$("#payment-form-" + this).show();
				}
			});
		}
		$('[name="license-dropdown"]').on("change", changeSelect);
	});
</script>
    <main id="main">
        <div class="outer-grid">
			<div class="grid">
			<?php
			while (have_posts()) : the_post();
			?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
				</article>
			<?php
			endwhile;
			?>
			</div>
        </div>
    </main>
<?php
get_footer();
