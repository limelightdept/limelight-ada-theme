<?php
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
get_header();
?>
    <main id="main">
        <div class="outer-grid">
            <div class="grid">
                <?php
                while (have_posts()) : the_post();
                    //Variables for building the rest URL
                    $user = wp_get_current_user();
                    $user_id = $user->ID;
                    $audit_id = get_the_ID();
                    $domain = get_post_meta($audit_id, "_ada_audit_domain", true);

                    $nonce = wp_create_nonce("audit_scan_nonce");
                	$link = admin_url("admin-ajax.php?action=audit_scan");
                ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <div class="entry-content">
                            <div class="vc_btn3-container ada-button white medium">
                                <?php echo "<button class='vc_general vc_btn3 vc_btn3-size-md vc_btn3-color-ada-button start-audit-scan' data-audit-domain='$domain' data-audit-id='$audit_id' data-user-id='$user_id' data-nonce='$nonce' href='$link'>START SCAN</button>"; ?>
                            </div>
                        </div>
                    </article>
                <?php
                endwhile;
                ?>
            </div>
        </div>
    </main>
<?php
get_footer();
