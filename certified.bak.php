<?php

/* Template Name: Badge Template */
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
get_header();

$badge_domain = sanitize_text_field(get_query_var("badge"));
$post_id = get_query_var("post_id");

if( !filter_var($post_id, FILTER_VALIDATE_INT) ){
    preg_match('/\/\d+$/', $_SERVER["REQUEST_URI"], $output);
    $post_id = ltrim($output[0], "/");
    $badge_domain = ltrim($_SERVER["REQUEST_URI"], "/certified/");
    $badge_domain = str_replace("/" . $post_id, "", $badge_domain);
}

$mp = "_ada_cert_";
$fail_message = "There was no certification found at this URL";

?>
<style>
    .badge-content{background-color:#eee;border:1px solid black;text-align:center;display:flex;height:auto;width:100%;justify-content: center;align-items:center;}
	body{background-image:none;background-color:#fff;color:#261928;}
</style>
<div id="main">
    <div class="outer-grid">
        <div class="grid">
            <main>
                <article <?php post_class(); ?>>
                    <div class="entry-content">
                        <div class="content-wrapper" style="text-align:center;">
							<h2>Certification of WCAG Compliance, Level A and AA</h2>
							<br>
                            <?php
                            if(!empty($badge_domain) AND !empty($post_id)){
                                $cert = get_post_meta($post_id);

                                if(!empty($cert[$mp."badge"][0]) AND $cert[$mp."status"][0] === "certified" AND $badge_domain === $cert[$mp."domain"][0]){
                                    $post = get_post($post_id);
                                    $user = get_user_by("ID", $post->post_author);
                                    ?>
									<h3>
										This digital certificate confirms that <?php if( $cert[$mp."domain"][0] == "mydigimag.rrd.com/publication/?i=528281"){ echo "the publisher of this content"; }else{ echo $cert[$mp."domain"][0];} ?> was audited and certified as WCAG-compliant (level A and AA) by Online ADA on <?php echo $cert[$mp."date_certified"][0]; ?>.
									</h3>
									<br>
                                    <h2>What This Means</h2>
									<br>
                                    <h3>
										By being independently audited and certified compliant, <?php if( $cert[$mp."domain"][0] == "mydigimag.rrd.com/publication/?i=528281"){ echo "the publisher of this content"; }else{ echo $cert[$mp."domain"][0];} ?> has signified their personal values and stressed the importance of making their website accessible to everyone, including those who may be using assistive device software to interpret their website. Certification of compliance is a good-faith effort by <?php if( $cert[$mp."domain"][0] == "mydigimag.rrd.com/publication/?i=528281"){ echo "the publisher of this content"; }else{ echo $cert[$mp."domain"][0];} ?> to ensure proper steps are taken to make the internet more equitable for people of all abilities. 
									</h3>
									<br>
                                    <h2>Website Accessibility</h2>
									<br>
									<h3>
										The independent audit and certification of compliance confirms that the website meets the standards set forth by WCAG, including levels A and AA. Digital accessibility is fluid, not linear, meaning that websites may temporarily slip out of compliance. If you feel like part of this website is not WCAG-compliant, please click here to <a href="<?php echo site_url("/contact"); ?>">contact us</a>.
									</h3>
									<br>
									<h2>About OnlineADA.com</h2>
									<br>
									<h3>
										<div>Online ADA is a national leader in digital accessibility. We make and manage the ADA Plugin for WordPress at <a href="https://adaplugin.com">Adaplugin.com</a>, a browser-based scanning software (<a href="<?php echo site_url("compliance-audit"); ?>">get it here</a>) and color-saturation checking tool. Click here to <a href="<?php echo site_url("audit"); ?>">learn more</a>.</div>
										<br>
										<div>We provide independent 3rd-party audits and certifications of WCAG Compliance to municipalities, governments, non-profits, and businesses across North America. We are proudly located in the gorgeous Pacific Northwest.</div>
									</h3>
                                    <?php
                                }else{
                                    echo $fail_message;
                                }
                            }else{
                                echo $fail_message;
                            }
                            ?>
                            
                        </div>
                    </div>
                </article>
            </main>
        </div>
    </div>
</div>
<?php
get_footer();
