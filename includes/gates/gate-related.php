<?php

use Limelight_Child_Helpers as LCH;

// function check_verify_user($user){
// 	$verified = get_user_meta($user->ID, "_limelight_email_verified", true);
// 	if(!$verified && isset($_GET["verify"])){
// 		$token = get_user_meta($user->ID, "_limelight_email_verify_token", true);
// 		if($token == $_GET["verify"]){
// 			update_user_meta($user->ID, "_limelight_email_verified", true);
// 			delete_user_meta($user->ID, "_limelight_email_verify_token");
// 		}
// 	}

// }

add_action('template_redirect', 'route_redirects');
function route_redirects(){
	$destination = $_SERVER["REQUEST_URI"];
	$origin = ( isset($_SERVER["HTTP_REFERER"]) ) ? $_SERVER["HTTP_REFERER"] : "";
	$user = wp_get_current_user();
	
	//Gate membership area
	
	if(!is_user_logged_in() AND is_members()){
		wp_safe_redirect("/login?redirect_to=".urlencode($destination));
	}
	
	//Gate free scan
	if(is_page("free-scan") AND 
		is_user_logged_in() AND 
		!in_array("administrator", wp_get_current_user()->roles) AND 
		!LCH::is_elementor() )
	{
		wp_redirect("/agency");
		exit;
	}

	if( is_page("manage-domains") && 
		!in_array("administrator", wp_get_current_user()->roles) &&
		in_array("free_compliance_audit_user", wp_get_current_user()->roles))
	{
		wp_redirect("/agency/previous-scan");
	}

	//Only allow Posting to review page
	if( is_page("review") AND (empty($_POST) OR empty(get_query_var("domain_id"))) AND !LCH::is_elementor() ){
		wp_redirect( home_url("/agency/sitemap") );
		exit;
	}
	
	//Scan results page
	if( is_page("reports") AND ( empty(get_query_var("domain_id")) OR empty(get_query_var("user_id")) )  AND !LCH::is_elementor() ){
		
		wp_redirect( home_url("/agency/previous-scan") );
		exit;
	}

	//Scan options page
	
	if( is_page("options") AND empty(get_query_var("domain_id")) AND !LCH::is_elementor() ){
		wp_redirect( home_url("/agency/new-scan") );
		exit;
	}
	
	if( is_page("new-scan") AND !LCH::is_elementor() ){
		$domains = LCH::get_domains(wp_get_current_user()->ID);
		
		if(empty($domains)){
			wp_redirect(home_url("/manage-domains"));
			exit;
		}
		$verified_domains = [];
		foreach($domains AS $ID => $domain){
			$path = LCH::get_spath($ID);
			
			if(is_file($path)){
				$verified_domains[$ID] = $domain;
			}
		}
		
		if(empty($verified_domains) AND (in_array("compliance_audit_user", $user->roles) || in_array("agency_user", $user->roles))){
			wp_redirect(home_url("/agency/sitemap"));
			exit;
		}
	}

}
