<?php
namespace OADA_CHILD\widgets\Elementor;


use \Elementor\Widget_Base;

class MembersNav extends Widget_Base {

    private $name = 'onlineada-membersnav';
    
    public function __construct( $data = array(), $args = null ){
        parent::__construct($data, $args);
        
    }

    public function widget_scripts(){
        \wp_enqueue_script("elem-widget-members-nav-js", get_stylesheet_directory_uri() . "/assets/js/widget-scripts/members-nav.js", ["jquery"], rand(), true);
    }

    public function widget_styles(){
        \wp_enqueue_style("elem-widget-members-nav-css", get_stylesheet_directory_uri() . "/assets/styles/widget-styles/members-nav.css", rand(), true);
    }

    public function get_name() {
		  return $this->name;
    }
    
    public function get_title() {
		  return __( 'Members Navigation', 'limelight-theme-online-ada' );
    }
    
    public function get_icon() {
		  return 'fa fa-bars';
    }
    
    public function get_categories() {
		  return [ 'online-ada' ];
    }
    
    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'limelight-theme-online-ada' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
        );

        $this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'background',
				'label' => __( 'Background', 'limelight-theme-online-ada' ),
				'types' => [ 'classic', 'gradient', 'video' ],
				'selector' => '{{WRAPPER}} .wrapper',
			]
		);
        
        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'header_content_typography',
				'label' => __( 'Header Typography', 'limelight-theme-online-ada' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .header-text',
			]
        );

        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'link_content_typography',
				'label' => __( 'Link Typography', 'limelight-theme-online-ada' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .mobile-menu a',
			]
        );

        $this->add_responsive_control(
			'header_padding',
			[
				'label' => __( 'Header Padding', 'limelight-theme-online-ada' ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .header-padding' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
        );
        
        $this->add_responsive_control(
			'link_padding',
			[
				'label' => __( 'Link Padding', 'limelight-theme-online-ada' ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .link-padding' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
        
		$this->end_controls_section();

    }
    
    protected function render() {
        $settings = $this->get_settings_for_display();        
        $this->widget_scripts();
        $this->widget_styles();
        ?>        
            <div class="mobile-menu-button-wrapper limelight-navigation">
                <button class="mobile-menu-button" aria-label="Menu">
                    <span class="menu-bars">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                    <span class="mobile-text">
                        <span class="mobile-hidden">Menu</span>
                        <span class="mobile-visible">Close</span>
                    </span>
                </button>
            </div>
            <div class="collapsible-container" style="height: 0px;">
                <div class="mobile-menu limelight-navigation wrapper">
                    <?php
                        $user = wp_get_current_user();
                        
                        if(in_array("free_compliance_audit_user", $user->roles)){
                        $args = array(
                            'menu'   => 'Free Scan Nav',
                            'walker' => new \Limelight_Walker()
                        );
                        }else{
                            $args = array(
                                'menu'   => 'Membership Nav',
                                'walker' => new \Limelight_Walker()
                            );
                        }
                        echo '<nav class="nav-menu nav-mobile nav-primary">';
                        wp_nav_menu( $args );
                        echo '</nav>';
                    ?>
                </div>
            </div>
        <?php
        
    }

    protected function _content_template() {
        ?>
            <div class="mobile-menu-button-wrapper limelight-navigation">
                <button class="mobile-menu-button" aria-label="Menu">
                    <span class="menu-bars">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                    <span class="mobile-text">
                        <span class="mobile-hidden">Menu</span>
                        <span class="mobile-visible">Close</span>
                    </span>
                </button>
            </div>
            <div class="collapsible-container">
                <div class="mobile-menu limelight-navigation wrapper">
                    <?php
                        $user = wp_get_current_user();
                        
                        if(in_array("free_compliance_audit_user", $user->roles)){
                        $args = array(
                            'menu'   => 'Free Scan Nav',
                            'walker' => new \Limelight_Walker()
                        );
                        }else{
                            $args = array(
                                'menu'   => 'Membership Nav',
                                'walker' => new \Limelight_Walker()
                            );
                        }
                        echo '<nav class="nav-menu nav-mobile nav-primary">';
                        wp_nav_menu( $args );
                        echo '</nav>';
                    ?>
                </div>
            </div>
        <?php
    }

}