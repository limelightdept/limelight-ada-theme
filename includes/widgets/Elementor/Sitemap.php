<?php
namespace OADA_CHILD\widgets\Elementor;

use Limelight\Plugins\ADA_Webservice\API;
use \Elementor\Widget_Base;
use Limelight_Child_Helpers AS LCH;

class Sitemap extends Widget_Base {
		
	private $domains,
	        $sitemaps;

    public function __construct( $data = array(), $args = null ){
        parent::__construct($data, $args);
    }

    private function init(){
        $user = wp_get_current_user();
        $this->domains = LCH::get_domains($user->ID);
        $this->sitemaps = [];

        foreach($this->domains AS $did => $domain){
            $this->sitemaps[$did] = LCH::get_sitemap($user->ID, $did);
        }
        
        $this->widget_scripts();
        $this->widget_styles();
    }
    
    public function widget_scripts(){
        \wp_enqueue_script("sitemap-scripts", get_stylesheet_directory_uri() . "/assets/js/widget-scripts/sitemap-scripts.js", ["jquery"], "1.0", true);
    }
    
    public function widget_styles(){
        \wp_enqueue_style("sitemap-styles", get_stylesheet_directory_uri() . "/assets/styles/widget-styles/sitemap-styles.css", "1.0", true);
    }

    public function get_name() {
		return 'onlineada-sitemap';
    }
    
    public function get_title() {
		return __( 'Sitemap', 'limelight-theme-online-ada' );
    }
    
    public function get_icon() {
		return 'fa fa-code';
    }
    
    public function get_categories() {
		return [ 'online-ada' ];
    }
    
    protected function _register_controls() {
        $this->start_controls_section(
            'content_section',
            [
                'label' => __( 'Content', 'limelight-theme-online-ada' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'content_align',
            [
                'label' => __( 'Alignment', 'limelight-theme-online-ada' ),
                'type' => \Elementor\Controls_Manager::CHOOSE,
                'options' => [
                        'left' => [
                                'title' => __( 'Left', 'limelight-theme-online-ada' ),
                                'icon' => 'fa fa-align-left',
                        ],
                        'center' => [
                                'title' => __( 'Center', 'limelight-theme-online-ada' ),
                                'icon' => 'fa fa-align-center',
                        ],
                        'right' => [
                                'title' => __( 'Right', 'limelight-theme-online-ada' ),
                                'icon' => 'fa fa-align-right',
                        ],
                ],
                'default' => 'center',
                'toggle' => true,
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'save_sitemap_button_section',
            [
                'label' => __( 'Save Sitemap Button', 'limelight-theme-online-ada' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Typography::get_type(),
            [
                'name' => 'sitemap_button_typography',
                'label' => __( 'Button Typography', 'limelight-theme-online-ada' ),
                'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} button',
            ]
        );

        $this->add_control(
            'save_sitemap_button',
            [
                'label' => __( 'Save Sitemap Button text', 'limelight-theme-online-ada' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( 'Submit', 'limelight-theme-online-ada' ),
                'placeholder' => __( 'Enter something...', 'limelight-theme-online-ada' ),
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Border::get_type(),
            [
                'name' => 'border',
                'label' => __( 'Border', 'limelight-theme-online-ada' ),
                'selector' => '{{WRAPPER}} button',
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Background::get_type(),
            [
                'name' => 'background',
                'label' => __( 'Background', 'plugin-domain' ),
                'types' => [ 'classic', 'gradient', 'video' ],
                'selector' => '{{WRAPPER}} button',
            ]
        );

        $this->add_control(
            'save_sitemap_border_radius',
            [
                'label' => __( 'Border Radius', 'elementor' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'select_max_width_section',
            [
                'label' => __( 'Select Max Width', 'limelight-theme-online-ada' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_responsive_control(
			'select_max_width',
			[
				'label' => __( 'Select Max Width', 'elementor' ),
				'type' => \Elementor\Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 960
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					]
				],
				'default' => [
					'size' => '50',
				],
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} select' => 'max-width: {{SIZE}}{{UNIT}}'
				]
			]
		);

        $this->end_controls_section();

        $this->start_controls_section(
            'new_scan_button_section',
            [
                'label' => __( 'New Scan Button', 'limelight-theme-online-ada' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Typography::get_type(),
            [
                'name' => 'ada_button_typography',
                'label' => __( 'New Scan Button Typography', 'limelight-theme-online-ada' ),
                'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .ada-button',
            ]
        );

        $this->add_control(
            'new_scan_button',
            [
                'label' => __( 'New Scan Button text', 'limelight-theme-online-ada' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( 'Submit', 'limelight-theme-online-ada' ),
                'placeholder' => __( 'Enter something...', 'limelight-theme-online-ada' ),
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Border::get_type(),
            [
                'name' => 'new_scan_border',
                'label' => __( 'Border', 'limelight-theme-online-ada' ),
                'selector' => '{{WRAPPER}} .ada-button',
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Background::get_type(),
            [
                'name' => 'new_scan_background',
                'label' => __( 'Background', 'plugin-domain' ),
                'types' => [ 'classic', 'gradient', 'video' ],
                'selector' => '{{WRAPPER}} .ada-button',
            ]
        );

        $this->add_control(
			'new_scan_text_color',
			[
				'label' => __( 'Text Color', 'elementor' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .ada-button' => 'color: {{VALUE}};'
				]
			]
		);

        $this->add_control(
            'ada_button_border_radius',
            [
                'label' => __( 'Border Radius', 'elementor' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .ada-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
			'ada_button_width',
			[
				'label' => __( 'Button Width', 'elementor' ),
				'type' => \Elementor\Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 960
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					]
				],
				'default' => [
                    'size' => '100',
                    'unit' => 'px'
				],
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .ada-button' => 'width: {{SIZE}}{{UNIT}}'
				]
			]
        );
        
        $this->add_control(
            'ada_button_padding',
            [
                'label' => __( 'Button Padding', 'elementor' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .ada-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'ada_button_align',
            [
                'label' => __( 'Button Alignment', 'limelight-theme-online-ada' ),
                'type' => \Elementor\Controls_Manager::CHOOSE,
                'options' => [
                    'margin-right:auto' => [
                        'title' => __( 'Left', 'limelight-theme-online-ada' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'margin:auto' => [
                        'title' => __( 'Center', 'limelight-theme-online-ada' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'margin-left:auto' => [
                        'title' => __( 'Right', 'limelight-theme-online-ada' ),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'default' => 'center',
                'toggle' => true,
            ]
		);

        $this->end_controls_section();
    }
    
    protected function render() {
        $settings = $this->get_settings_for_display();
        $this->init();
        
        ?>
            <div class="entry-content">
                <div class="content-body" style="text-align:<?php echo $settings["content_align"]; ?>">
                    <header class="head-wrap" style="margin-bottom:10px;">                                           
                        <h2>Select a website to manage</h2>
                    </header>
                    <div class="body-wrap select-wrap" style="margin-bottom:10px;">
                    
                        <select class="website-select ll-input" name="website-select">
                            <?php foreach($this->domains AS $did => $domain): ?>
                                <option data-has-sitemap="<?php echo $this->sitemaps[$did] ? "true" : "false"; ?>" value="<?php echo $did; ?>"><?php echo $domain['url']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <header class="head-wrap" style="margin-bottom:10px;">                                           
                        <h2>Choose an option</h2>
                    </header>
                    <div class="body-wrap">
                        <form id="sitemap" action="" method="POST" enctype="multipart/form-data">											
                            <div class="input-wrap" style="margin-bottom:10px;">
                                <div>
                                    <label style="padding-right: 5px;" for="create">Create new</label>
                                    <input id="create" type="radio" name="_sitemap" value="create_new" checked />
                                </div>
                                <div>
                                    <label style="padding-right: 5px;" for="upload">Upload sitemap</label>
                                    <input id="upload" type="radio" name="_sitemap" value="upload" />
                                </div>
                                <div style="display:none;">
                                    <label style="padding-right: 5px;" for="existing">Review existing</label>
                                    <input id="existing" type="radio" name="_sitemap" value="review_existing" />
                                </div>
                            </div>

                            <div style="display:none;">
                                <p>Accepted file formats include .xml and .csv</p>
                                <input aria-label="Upload File" type="file" name="fileUpload" id="fileUpload" accept=".xml,.csv" >
                            </div>
                            <button class="submit-button"><?php echo $settings["save_sitemap_button"]; ?></button>
                            <div class="msg"></div>
                            
                            <?php foreach($this->sitemaps AS $path): ?>
                                <?php if($path): ?>
                                    <div style="margin-bottom:10px;">Since you have a sitemap registered, you can start a scan</div>
                                        <a style="<?php echo $settings['ada_button_align']; ?>" class="ada-button" href="/agency/new-scan"><?php echo $settings["new_scan_button"]; ?></a>
                                    </div>
                                    <?php return; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </form>
                    </div>
                </div>
            </div>
        <?php
    }

    protected function _content_template() {
        ?>
            <div class="entry-content">
                <div class="content-body" style="text-align:{{{ settings.content_align}}}">
                    <header class="head-wrap" style="margin-bottom:10px;">                                           
                        <h2>Select a website to manage</h2>
                    </header>
                    <div class="body-wrap select-wrap">
                        <select style="margin-bottom:10px;" class="website-select ll-input" name="website-select">
                            
                        </select>
                    </div>
                    <header class="head-wrap" style="margin-bottom:10px;">                                           
                        <h2>Choose an option</h2>
                    </header>
                    <div class="body-wrap">
                        <form id="sitemap" action="" method="POST" enctype="multipart/form-data">											
                            <div class="input-wrap">
                                <div>
                                    <label style="padding-right: 5px;" for="create">Create new</label>
                                    <input id="create" type="radio" name="_sitemap" value="create_new" checked />
                                </div>
                                <div>
                                    <label style="padding-right: 5px;" for="upload">Upload sitemap</label>
                                    <input id="upload" type="radio" name="_sitemap" value="upload" />
                                </div>
                                <div style="display:none;">
                                    <label style="padding-right: 5px;" for="existing">Review existing</label>
                                    <input id="existing" type="radio" name="_sitemap" value="review_existing" />
                                </div>
                            </div>

                            <div style="display:none;">
                                <p>Accepted file formats include .xml and .csv</p>
                                <input aria-label="Upload File" type="file" name="fileUpload" id="fileUpload" accept=".xml,.csv" >
                            </div>
                            <button class="submit-button">{{{ settings.save_sitemap_button }}}</button>
                            <div class="msg"></div>
                            
                            <div>Since you have a sitemap registered, you can start a scan</div>
                            <a style="{{{settings.ada_button_align}}}" class="ada-button" href="/new-scan">{{{ settings.new_scan_button }}}</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        <?php
	}
}