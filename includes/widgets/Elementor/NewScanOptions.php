<?php
namespace OADA_CHILD\widgets\Elementor;

use \Elementor\Widget_Base;
use Limelight\Plugins\ADA_Webservice\API;
use Limelight_Child_Helpers as LCH;

class NewScanOptions extends Widget_Base {
		
    private $audit_id,
            $domain,
            $path,
            $user,
            $sitemap;

    public function __construct( $data = array(), $args = null ){
        parent::__construct($data, $args); 
    }

    private function init(){
        $this->user = wp_get_current_user();
        $this->domain = LCH::get_domain_by_id(get_query_var("domain_id"));
        $this->path = wp_upload_dir()["basedir"]. "/users/{$this->user->ID}/{$this->domain->ID}/sitemap.csv";
        $this->sitemap = LCH::get_sitemap_from_file($this->path, $this->domain->url);
        
        unset($this->sitemap["domain"]);

        $this->widget_scripts();
        $this->widget_localize();
        $this->widget_styles();
    }
    
    public function widget_scripts(){
        \wp_enqueue_script("elem-widget-new-scan-options-js", \get_stylesheet_directory_uri() . "/assets/js/widget-scripts/options-scripts.js", ["jquery"], "1.0", true);
    }

    public function widget_localize(){
        \wp_localize_script("elem-widget-new-scan-options-js", "php_var", [
            "domain_id" => $this->domain->ID,
            "sitemap" => $this->sitemap,
            "homepage" => $this->domain->url,
            "user_id" => $this->user->ID,
            "api_remote_base" => "https://scan.onlineada.com/webservice/v1",
            "this_site" => \home_url(),
            "api_nonce" => \wp_create_nonce('wp_rest')
        ]);
    }
    
    public function widget_styles(){
        \wp_enqueue_style("options-styles-fontawesome", "https://use.fontawesome.com/releases/v5.1.0/css/all.css", "1.0", true);
        \wp_enqueue_style("options-styles", \get_stylesheet_directory_uri() . "/assets/styles/widget-styles/options-styles.css" , "1.0", true);
    }

    public function get_name() {
		  return 'onlineada-newscanoptions';
    }
    
    public function get_title() {
		  return __( 'New Scan Options', 'limelight-theme-online-ada' );
    }
    
    public function get_icon() {
		  return 'fa fa-code';
    }
    
    public function get_categories() {
		  return [ 'online-ada' ];
    }
    
    protected function _register_controls() {

            $this->start_controls_section(
				'content_section',
				[
					'label' => __( 'Content', 'limelight-theme-online-ada' ),
					'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
				]
            );

            $this->add_control(
                'content_align',
                [
                    'label' => __( 'Alignment', 'limelight-theme-online-ada' ),
                    'type' => \Elementor\Controls_Manager::CHOOSE,
                    'options' => [
                        'left' => [
                            'title' => __( 'Left', 'limelight-theme-online-ada' ),
                            'icon' => 'fa fa-align-left',
                        ],
                        'center' => [
                            'title' => __( 'Center', 'limelight-theme-online-ada' ),
                            'icon' => 'fa fa-align-center',
                        ],
                        'right' => [
                            'title' => __( 'Right', 'limelight-theme-online-ada' ),
                            'icon' => 'fa fa-align-right',
                        ],
                    ],
                    'default' => 'center',
                    'toggle' => true,
                ]
            );


            $this->end_controls_section();

			$this->start_controls_section(
				'header_section',
				[
					'label' => __( 'Header', 'limelight-theme-online-ada' ),
					'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
				]
            );

            
			$this->add_control(
				'scan_options_header',
				[
					'label' => __( 'Scan Options Header', 'limelight-theme-online-ada' ),
					'type' => \Elementor\Controls_Manager::TEXT,
					'default' => __( 'Select options and start the scan', 'limelight-theme-online-ada' ),
					'placeholder' => __( 'Type your description here', 'limelight-theme-online-ada' ),
				]
			);

			$this->add_group_control(
				\Elementor\Group_Control_Typography::get_type(),
				[
					'name' => 'header_typography',
					'label' => __( 'Header Typography', 'limelight-theme-online-ada' ),
					'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} header.head-wrap h2',
				]
			);

			$this->end_controls_section();

			$this->start_controls_section(
				'query_string_section',
				[
					'label' => __( 'Query String', 'limelight-theme-online-ada' ),
					'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
				]
			);

			$this->add_control(
				'query_string_message',
				[
					'label' => __( 'Message', 'limelight-theme-online-ada' ),
					'type' => \Elementor\Controls_Manager::TEXT,
					'default' => __( 'Include pages with query strings?', 'limelight-theme-online-ada' ),
					'placeholder' => __( 'Type your description here', 'limelight-theme-online-ada' ),
				]
			);
					
			$this->add_group_control(
				\Elementor\Group_Control_Typography::get_type(),
				[
					'name' => 'qs_message_typography',
					'label' => __( 'Message Typography', 'limelight-theme-online-ada' ),
					'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .entry-content .qs-msg',
				]
			);

            $this->end_controls_section();
            
            $this->start_controls_section(
				'cbr_section',
				[
					'label' => __( 'Color Blind Report', 'limelight-theme-online-ada' ),
					'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
				]
			);

			$this->add_control(
				'cbr_message',
				[
					'label' => __( 'CBR Message', 'limelight-theme-online-ada' ),
					'type' => \Elementor\Controls_Manager::WYSIWYG,
					'default' => __( '<div>Include color blind report? </div><div><small>(This adds on average 3 minutes to the scan)</small></div>', 'limelight-theme-online-ada' ),
					'placeholder' => __( 'Type your description here', 'limelight-theme-online-ada' ),
				]
			);
					
			$this->add_group_control(
				\Elementor\Group_Control_Typography::get_type(),
				[
					'name' => 'cbr_message_typography',
					'label' => __( 'CBR Message Typography', 'limelight-theme-online-ada' ),
					'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .entry-content .cbr-msg',
				]
			);

			$this->end_controls_section();

			$this->start_controls_section(
				'button_section',
				[
					'label' => __( 'Button', 'limelight-theme-online-ada' ),
					'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
				]
			);

			$this->add_group_control(
				\Elementor\Group_Control_Typography::get_type(),
				[
					'name' => 'button_typography',
					'label' => __( 'Button Typography', 'limelight-theme-online-ada' ),
					'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} button',
				]
			);

			$this->add_control(
				'scan_button',
				[
					'label' => __( 'Button text', 'limelight-theme-online-ada' ),
					'type' => \Elementor\Controls_Manager::TEXT,
					'default' => __( 'Submit', 'limelight-theme-online-ada' ),
					'placeholder' => __( 'Enter something...', 'limelight-theme-online-ada' ),
				]
            );
            
            $this->add_group_control(
                \Elementor\Group_Control_Border::get_type(),
                [
                    'name' => 'border',
                    'label' => __( 'Border', 'limelight-theme-online-ada' ),
                    'selector' => '{{WRAPPER}} button',
                ]
            );

            $this->add_group_control(
			    \Elementor\Group_Control_Background::get_type(),
                [
                    'name' => 'background',
                    'label' => __( 'Background', 'plugin-domain' ),
                    'types' => [ 'classic', 'gradient', 'video' ],
                    'selector' => '{{WRAPPER}} button',
                ]
            );

            $this->add_control(
                'border_radius',
                [
                    'label' => __( 'Border Radius', 'elementor' ),
                    'type' => \Elementor\Controls_Manager::DIMENSIONS,
                    'size_units' => [ 'px', '%' ],
                    'selectors' => [
                        '{{WRAPPER}} button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                    ],
                ]
            );

			$this->end_controls_section();

    }
    
    protected function render() {
        $settings = $this->get_settings_for_display();
        $this->init();
        
        ob_start();
        ?>
            <div class="content-wrapper" style="text-align: <?php echo $settings['content_align']; ?>">
                <div class="content-header">
                    <header class="head-wrap remove">
                        <h2><?php echo $settings["scan_options_header"]; ?></h2>
                    </header>
                </div>
                <div class="entry-content">
                    <div class="body-wrap content-body">
                        <div class="body-wrap">
                            <div class="input-wrap qs-msg remove" style="padding:5px;" >
                                <div><?php echo $settings["query_string_message"]; ?></div>
                            </div>
                            <div class="input-wrap remove" style="padding-top:5px;padding-bottom:20px;">
                                <label style="padding-right:3px;" for="query-strings-yes">Yes</label>
                                <input style="margin-right:50px;" id="query-strings-yes" type="radio" name="use_query_strings" value="1" checked />
                                <label style="padding-right:3px;" for="query-strings-no">No</label>
                                <input style="padding-right:3px;" id="query-strings-no" type="radio" name="use_query_strings" value="0" />
                            </div>
                            <div class="input-wrap cbr-msg remove" style="padding:5px;" >
                                <?php echo $settings["cbr_message"]; ?>
                            </div>
                            <div class="input-wrap remove" style="padding-top:5px;padding-bottom:20px;">
                                <label style="padding-right:3px;" for="color-blind-yes">Yes</label>
                                <input style="margin-right:50px;" id="color-blind-yes" type="radio" name="use_color_blind" value="1" />
                                <label style="padding-right:3px;" for="color-blind-no">No</label>
                                <input style="padding-right:3px;" id="color-blind-no" type="radio" name="use_color_blind" value="0" checked />
                            </div>

                            <button class="start-scan remove"><?php echo $settings["scan_button"]; ?></button>
                            <div class="scan-message">
                                <div class="message"></div>
                                <div class="load"></div>
                            </div>

                            <a></a>
                        </div>
                    </div>
                </div>
                
            </div>
        <?php
        echo ob_get_clean();
    }

    protected function _content_template() {
				
        ob_start();
        ?>
            <div class="content-wrapper" style="text-align: {{{settings.content_align}}}">
                <div class="content-header">
                    <header class="head-wrap remove">
                        <h2>{{{ settings["new_scan_header"] }}}</h2>
                    </header>
                </div>
                <div class="entry-content">
                    <div class="body-wrap content-body">
                        <div class="body-wrap">
                            <div class="input-wrap remove" style="padding:5px;" >
                                <div>{{{ settings["query_string_message"] }}}</div>
                            </div>
                            <div class="input-wrap remove" style="padding-top:5px;padding-bottom:20px;">
                                <label style="padding-right:3px;" for="query-strings-yes">Yes</label>
                                <input style="margin-right:50px;" id="query-strings-yes" type="radio" name="use_query_strings" value="1" checked />
                                <label style="padding-right:3px;" for="query-strings-no">No</label>
                                <input style="padding-right:3px;" id="query-strings-no" type="radio" name="use_query_strings" value="0" />
                            </div>
                            <div class="input-wrap remove" style="padding:5px;" >
                                {{{ settings.cbr_message }}}
                            </div>
                            <div class="input-wrap remove" style="padding-top:5px;padding-bottom:20px;">
                                <label style="padding-right:3px;" for="color-blind-yes">Yes</label>
                                <input style="margin-right:50px;" id="color-blind-yes" type="radio" name="use_color_blind" value="1" />
                                <label style="padding-right:3px;" for="color-blind-no">No</label>
                                <input style="padding-right:3px;" id="color-blind-no" type="radio" name="use_color_blind" value="0" checked />
                            </div>

                            <button class="start-scan remove">{{{ settings.scan_button }}}</button>
                            <div class="scan-message">
                                <div class="message"></div>
                                <div class="load"></div>
                            </div>

                            <a></a>
                            <!--<a href='/wp-json/limelight/v1/test-pdf/' >PDF Test</a>-->
                        </div>
                    </div>
                </div>
                
            </div>
        <?php
        echo ob_get_clean();
	}
}