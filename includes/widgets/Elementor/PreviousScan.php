<?php
namespace OADA_CHILD\widgets\Elementor;

use Limelight\Plugins\ADA_Webservice\API;
use \Elementor\Widget_Base;
use Limelight_Child_Helpers AS LCH;

class PreviousScan extends Widget_Base {
		
    private $user,
            $filename,
            $audits,
            $domains,
            $verified_domains;

    public function __construct( $data = array(), $args = null ){
        parent::__construct($data, $args);
    }

    private function init(){
        $this->user = wp_get_current_user();
        $this->domains = LCH::get_domains($this->user->ID, true);
        
        foreach($this->domains AS $domain){
            $scan = LCH::get_scan_data($domain["ID"]);
            
            if(!empty($scan)){
                $this->verified_domains[] = $domain;
            }
        }

        $this->widget_scripts();
        $this->widget_styles();
        $this->widget_localize();
    }
    
    public function widget_scripts(){
        \wp_enqueue_script("previous-scan-scripts", get_stylesheet_directory_uri() . "/assets/js/widget-scripts/previous-scan-scripts.js", ["jquery"], "1.0", true);
    }
    
    public function widget_styles(){
        \wp_enqueue_style("previous-scan-styles", get_stylesheet_directory_uri() . "/assets/styles/widget-styles/previous-scan-styles.css", "1.0", true);
    }

    public function widget_localize(){
        \wp_localize_script("previous-scan-scripts", "php_var", ["user_id" => $this->user->ID ]);
    }

    public function get_name() {
		  return 'onlineada-previousscan';
    }
    
    public function get_title() {
		  return __( 'Previous Scan', 'limelight-theme-online-ada' );
    }
    
    public function get_icon() {
		  return 'fa fa-code';
    }
    
    public function get_categories() {
		  return [ 'online-ada' ];
    }
    
    protected function _register_controls() {
		
    }
    
    protected function render() {
        $settings = $this->get_settings_for_display();
        $this->init();
        ?>
            <div class="entry-content">
                <div class="content-body">
                    <?php if(!empty($this->domains)): ?>
                        <header class="head-wrap">
                            <h2>Choose a website to view scan results for</h2>
                        </header>
                        <div class="body-wrap">
                            <form id="sitemap" action="" method="POST" enctype="multipart/form-data">
                                <div class="input-wrap">
                                    <div class="select-wrap">
                                        <select class="website-select ll-input" name="website-select">
                                            <?php foreach($this->verified_domains AS $domain): ?>
                                                <option value="<?php echo $domain["ID"] ?>"><?php echo untrailingslashit($domain["url"]); ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <button class="submit-button">View Scan Results</button>
                            </form>
                        </div>
                    <?php else: ?>
                        <header class="head-wrap">
                            <h2>It looks like you haven't run any scans yet</h2>
                        </header>
                        <div class="body-wrap">
                            Start a new scan by clicking <a href="/agency/new-scan">here</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php
        
    }

    protected function _content_template() {
        ?>
            
        <?php
	}
}