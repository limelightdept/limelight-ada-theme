<?php
namespace OADA_CHILD\widgets\Elementor;

use \Elementor\Widget_Base;
use Limelight\Plugins\ADA_Webservice\API;
use Limelight_Child_Helpers as LCH;

class CertifiedBadgeContent extends Widget_Base {
		
    private $badge_domain,
            $post_id,
            $mp,
            $default_msg,
            $cert;

    public function __construct( $data = array(), $args = null ){
        parent::__construct($data, $args);
    }

    private function init(){
        if( LCH::is_elementor() ){
            $this->badge_domain = "www.co.todd.mn.us";
            $this->post_id = 3886;
        }else{
            $this->badge_domain = sanitize_text_field(get_query_var("badge"));
            $this->post_id = get_query_var("post_id");

            if( !filter_var($this->post_id, FILTER_VALIDATE_INT) ){
                preg_match('/\/\d+$/', $_SERVER["REQUEST_URI"], $output);
                if(!empty($output)){
                    $this->post_id = ltrim($output[0], "/");
                    $this->badge_domain = ltrim($_SERVER["REQUEST_URI"], "/certified/");
                    $this->badge_domain = str_replace("/" . $this->post_id, "", $this->badge_domain);
                }
            }
        }

        $this->cert = get_post_meta($this->post_id);
        $this->mp = "_ada_cert_";
    }

    private function filter_body_content($msg){
        $domain = $this->cert[$this->mp."domain"][0];
        $certified = $this->cert[$this->mp."status"][0] === "certified";
        $domains_match = $this->badge_domain === $this->cert[$this->mp."domain"][0];

        if( empty($this->badge_domain) || 
            empty($this->post_id) ||
            empty($domain) ||
            !$certified ||
            !$domains_match)
        {
            return false;
        }
        
        if($domain = "mydigimag.rrd.com/publication/?i=528281"){
            $domain = "the publisher of this content";
        }
        
        $msg = str_replace("[domain]", $this->cert[$this->mp."domain"][0], $msg);
        $msg = str_replace("[date-certified]", $this->cert[$this->mp."date_certified"][0], $msg);

        return $msg;
    }

    public function get_name() {
		  return 'onlineada-certificationbadgepage';
    }
    
    public function get_title() {
		  return __( 'Certification Badge Content', 'limelight-theme-online-ada' );
    }
    
    public function get_icon() {
		  return 'fa fa-code';
    }
    
    public function get_categories() {
		  return [ 'online-ada' ];
    }
    
    protected function _register_controls() {

        $this->start_controls_section(
            'content_section',
            [
                'label' => __( 'Content', 'limelight-theme-online-ada' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'content_align',
            [
                'label' => __( 'Alignment', 'limelight-theme-online-ada' ),
                'type' => \Elementor\Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __( 'Left', 'limelight-theme-online-ada' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'limelight-theme-online-ada' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'limelight-theme-online-ada' ),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'default' => 'center',
                'toggle' => true,
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'body_section',
            [
                'label' => __( 'Body', 'limelight-theme-online-ada' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'body_content',
            [
                'label' => __( 'Body Content: - use [domain] for the user\'s dynamic domain and [date-certified] for the date it was certified', 'limelight-theme-online-ada' ),
                'type' => \Elementor\Controls_Manager::WYSIWYG,
                'default' => __( "", 'limelight-theme-online-ada' ),
                'placeholder' => __( 'Type your description here', 'limelight-theme-online-ada' ),
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'fail_section',
            [
                'label' => __( 'Fail Message', 'limelight-theme-online-ada' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'fail_msg',
            [
                'label' => __( 'Fail Message', 'limelight-theme-online-ada' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( 'There was no certification found at this URL', 'limelight-theme-online-ada' ),
                'placeholder' => __( 'Enter something...', 'limelight-theme-online-ada' ),
            ]
        );

        $this->end_controls_section();

    }
    
    protected function render() {
        $settings = $this->get_settings_for_display();
        $this->init($settings);
        
        if(!LCH::is_elementor()){
            //$this->widget_scripts();
            //$this->widget_localize();
            //$this->widget_styles();
        }
        
        ob_start();
        ?>  
        
            <div class="entry-content">            
                <div class="content-wrapper" style="text-align:<?php echo $settings["content_align"]; ?>;">                                
                    <?php echo $this->filter_body_content($settings["body_content"]) ?: $settings["fail_msg"]; ?>                    
                </div>
            </div>
        <?php
        echo ob_get_clean();
    }

    protected function _content_template() { 
        ?>
            <div class="entry-content">
                <div class="content-wrapper" style="text-align:{{{settings.content_align}}};">
                    {{{settings.body_content}}}
                </div>
            </div>       
        <?php
	}
}