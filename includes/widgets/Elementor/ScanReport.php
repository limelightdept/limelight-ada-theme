<?php
namespace OADA_CHILD\widgets\Elementor;

use Limelight\Plugins\ADA_Webservice\API;
use Limelight_Child_Helpers AS LCH;
use \Elementor\Widget_Base;
use Carbon\Carbon;

class ScanReport extends Widget_Base {
		
    private $user_id,
            $user,
            $domain_id,
            $audit,
            $results,
            $cbr_report,
            $used_qs,
            $full_url,
            $trimmed,
            $snapshot,
            $snapshot_url,
            $sitemap,
            $icons,
            $total_pages,
            $get_snapshot,
            $assets_uri,
            $assets_folder;

    public function __construct( $data = array(), $args = null ){
        parent::__construct($data, $args);
    }

    private function init(){
        
        $this->user_id = get_query_var("user_id");
        $this->user = get_user_by("ID", $this->user_id);
        $this->domain_id = get_query_var("domain_id");

        $this->domain = LCH::get_domain_by_id($this->domain_id);
        $this->results = LCH::get_scan_data($this->domain_id);
        $this->cbr_report = LCH::get_file_uri("cbr", $this->domain_id);
        
        if (!file_exists($this->cbr_report)) {
            $this->cbr_report = false;
        }

         
        $this->trimmed = preg_replace('/^(http[s]?:[\/]{0,2})?/', "", $this->domain->url);
        $this->get_snapshot = false;
        $this->snapshot_path = LCH::get_file("snapshot", $this->domain_id);

        if (!file_exists($this->snapshot_path) || !is_file($this->snapshot_path)) {
            $this->get_snapshot = true;
        }

        if (file_exists($this->snapshot_path) || is_file($this->snapshot_path)) {
            $this->snapshot_url = LCH::get_file_uri("snapshot", $this->domain_id);
        }

        $this->total_pages = 1;
        
        if(!in_array("free_compliance_audit_user", $this->user->roles)){
            $path = LCH::get_spath($this->domain_id);
            $this->sitemap = LCH::get_sitemap_from_file($path, $this->domain->url);
            unset($this->sitemap["domain"]);
            if (empty($this->sitemap) || !$this->sitemap) {
                $this->sitemap = [];
            }

            $nq_pages = 0;
            $q_pages = 0;
            if (!empty($this->sitemap)) {
                $nq_pages = !empty($this->sitemap["non_query_links"]) ? count($this->sitemap["non_query_links"]) : 0;
                $q_pages = !empty($this->sitemap["query_links"]) ? count($this->sitemap["query_links"]) : 0;
            }

            $this->total_pages = $nq_pages + $q_pages;
            
        }

        $this->assets_uri = get_stylesheet_directory_uri() . "/assets/img/report-assets/";
        $this->assets_folder = get_stylesheet_directory() . "/assets/img/report-assets/";

        $this->icons = [
            "error" => $this->assets_uri . "error-icon.png",
            "contrast" => $this->assets_uri . "contrast-icon.png",
            "warning" => $this->assets_uri . "warning-icon.png",
            "star" => $this->assets_uri . "star.svg",
            "tooltip" => $this->assets_uri . "tooltip_arrow.png"
        ];

        $this->widget_scripts();
        $this->widget_localize();
        $this->widget_styles();
    }
    
    public function widget_scripts(){
        \wp_enqueue_script("reports-scripts", get_stylesheet_directory_uri() . "/assets/js/widget-scripts/reports-scripts.js", ["jquery"], "1.0", true);
    }
    
    public function widget_styles(){
        \wp_enqueue_style("scan-reports-css", get_stylesheet_directory_uri() . "/assets/styles/widget-styles/reports-styles.css", "1.0", true);
        \wp_enqueue_style("grid-css", get_stylesheet_directory_uri() . "/assets/styles/grid.css", "1.0", true);
        \wp_enqueue_style("elem-widget-scan-reports-fontawesome-css", "https://use.fontawesome.com/releases/v5.1.0/css/all.css", "1.0", true);
    }

    public function widget_localize(){
        wp_localize_script(
            "reports-scripts",
            "report_vars",
            [
                "results" => $this->results,
                "wcag" => json_encode(["guidelines" => LCH::getWcagArticlesModel(1, true), "articles" => LCH::getWcagArticlesModel(2)]),
                "domain_id" => $this->domain_id,
                "domain" => $this->full_url,
                "user_id" => $this->user->ID,
                "get_snapshot" => $this->get_snapshot,
                "icons" => $this->icons,
                "total_pages" => $this->total_pages,
                "site_api_url" => site_url() . "/wp-json/limelight/v1/"
            ]
        );
    }

    public function get_name() {
		  return 'onlineada-scanreport';
    }
    
    public function get_title() {
		  return __( 'Scan Report', 'limelight-theme-online-ada' );
    }
    
    public function get_icon() {
		  return 'fa fa-code';
    }
    
    public function get_categories() {
		  return [ 'online-ada' ];
    }
    
    protected function _register_controls() {

    }
    
    protected function render() {
        $settings = $this->get_settings_for_display();
        $this->init();
        
        ?>
            <div class="report-block">
                <!-- Section 1 -->
                <div class="title-section section ll-row">
                    <div class="ll-col-lg-7">
                        <div class="logo-wrap">
                            <img class="logo" src="<?php echo $this->assets_uri . "ada-logo-h-solid.svg"; ?>" alt="Online ADA Logo">
                        </div>
                        <div><h1 class="header">ACCESSIBILITY COMPLIANCE</h1></div>
                    </div>
                    <div class="date-wrap ll-col-lg-5">
                  
                    Scan completed: <?php echo Carbon::createFromTimestamp($this->domain->last_scanned)->format("F jS, Y"); ?>

                    </div>
                </div>
                <div class="section-divider summary">
                    <span class="left">Summary</span>
                    <span class="right"></span>
                </div>
                <!-- Section 2 -->
                <div class="summary-section section ll-row">
                    <div class="left ll-col-lg-4">
                        <img class="js-summary-img"
                            src="<?php echo !empty($this->snapshot_url) ? $this->snapshot_url : $this->assets_uri . "placeholder-600x400.png"; ?>"
                            alt="<?php echo $this->trimmed . ' homepage' ?>"
                        >
                    </div>
                    <div class="right ll-col-lg-8">
                        <div class="star-row">

                        </div>
                        <div class="score-row">
                            Scored <span class="global"></span> out of 5 stars for General Compliance
                        </div>
                        <div class="description-row">
                            Congratulations on taking the necessary steps to make your website accessibility compliant and make the internet a better place for everyone.
                        </div>
                        <div class="domain-row">
                            <?php echo ucfirst($this->trimmed); ?>
                        </div>
                    </div>
                </div>
                <div class="spacer-50"></div>
                <div class="ll-row section">
                    <div class="ll-col-lg-4">
                        <div class="pages-scanned-block">
                            <div class="header"></div>
                            <div class="sub-header">NONCOMPLIANT PAGES:</div>

                        </div>
                    </div>
                    <div class="ll-col-lg-8">
                        <div class="ll-row scores-row">

                        </div>
                    </div>
                </div>
                <div class="section-divider reports">
                    <span class="left">Detailed Reports</span>
                    <span class="right"></span>
                </div>
                <div class="section-divider errors-found">
                    <div class="left">Errors Found</div>
                    <div class="right">
                        <?php if ($this->cbr_report): ?>
                        <div class="img-wrap">
                            <img src="<?php echo $this->assets_uri . "eye-con.png" ?>" alt="" >
                        </div>
                        <div class="sub-header">Color Blindness</div>
                        <a target="blank" href="<?php echo $this->cbr_report; ?>">
                            <div class="pdf-viewer">
                                <span class="left"><img src="<?php echo $this->assets_uri . "download-icon-blue.png" ?>" alt="" ></span>
                                <span class="right">PDF Viewer</span>
                            </div>
                        </a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="section reports">
                    <div class="tabs">
                        <div data-tab-controls="1" class="tab tab-1 active">VIEW BY PAGE</div>
                        <div data-tab-controls="2" class="tab tab-2">VIEW BY OBJECT</div>
                    </div>
                    <!-- FILTERS -->
                    <div class="filter-wrap">
                        <div class="filters-row ll-row ll-no-gutters">
                            <div class="ll-col-lg-2 ll-col-sm-4">
                                <div class="filter">
                                    <label>
                                        <input type="checkbox" class="invisible" data-filter="flag-a" checked="checked">
                                        <div class="checkbox checked"></div>
                                        <span class="wcag-icon">A</span>
                                        <span>WCAG Level A</span>
                                    </label>
                                </div>
                            </div>
                            <div class="ll-col-lg-2 ll-col-sm-4">
                                <div class="filter">
                                    <label>
                                        <input type="checkbox" class="invisible" data-filter="flag-aa" checked="checked">
                                        <div class="checkbox checked"></div>
                                        <span class="wcag-icon">AA</span>
                                        <span>WCAG Level AA</span>
                                    </label>
                                </div>
                            </div>
                            <div class="ll-col-lg-2 ll-col-sm-4">
                                <div class="filter">
                                    <label>
                                        <input type="checkbox" class="invisible" data-filter="flag-s508" checked="checked">
                                        <div class="checkbox checked"></div>
                                        <span class="wcag-icon">508</span>
                                        <span>Section 508</span>
                                    </label>
                                </div>
                            </div>
                            <div class="ll-col-lg-2 ll-col-sm-4">
                                <div class="filter">
                                    <label>
                                        <input type="checkbox" class="invisible" data-filter="flag-contrast" checked="checked">
                                        <div class="checkbox checked"></div>
                                        <span class="wcag-icon no-bg">
                                            <img alt="" role="presentation" src="<?php echo $this->assets_uri . "contrast-icon.png"; ?>" >
                                        </span>
                                        <span>Contrast</span>
                                    </label>
                                </div>
                            </div>
                            <div class="ll-col-lg-2 ll-col-sm-4">
                                <div class="filter">
                                    <label>
                                        <input type="checkbox" class="invisible" data-filter="flag-error" checked="checked">
                                        <div class="checkbox checked"></div>
                                        <span class="wcag-icon no-bg">
                                            <img alt="" role="presentation" src="<?php echo $this->assets_uri . "error-icon.png"; ?>" >
                                        </span>
                                        <span>Errors</span>
                                    </label>
                                </div>
                            </div>
                            <div class="ll-col-lg-2 ll-col-sm-4">
                                <div class="filter">
                                    <label>
                                        <input type="checkbox" class="invisible" data-filter="flag-warning" checked="checked">
                                        <div class="checkbox checked"></div>
                                        <span class="wcag-icon no-bg">
                                            <img alt="" role="presentation" src="<?php echo $this->assets_uri . "warning-icon.png"; ?>" >
                                        </span>
                                        <span>Warnings</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END FILTERS -->
                    <div class="section downloads">
                        <div class="header ll-row">
                            <div class="left ll-d-flex">
                                <div class="ll-row" style="width:100%;">
                                    <!-- <div class="ll-col-12 ll-justify-content-center top">Advanced Filters</div>
                                    <div class="ll-col-6 ll-justify-content-center ll-d-flex ll-align-items-center">
                                        <div class="toggle-switch hide-repeat-errors">
                                            <label class="switch">
                                                <div class="label">Repeating Elements</div>
                                                <input type="checkbox">
                                                <span class="slider round"><span class="show">show</span><span class="hide">hide</span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="ll-col-6 ll-justify-content-center ll-d-flex ll-align-items-center">
                                        <div class="toggle-switch hide-unique-errors">
                                            <label class="switch">
                                                <div class="label">Unique Elements</div>
                                                <input type="checkbox">
                                                <span class="slider round"><span class="show">show</span><span class="hide">hide</span></span>
                                            </label>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <div class="right ll-d-flex ll-justify-content-sm-center ll-justify-content-lg-end">
                                <div class="right-wrap" style="text-align:center;">
                                    <div class="top">Download Your Report</div>
                                    <div class="bottom ll-d-flex ll-justify-content-center">
                                        <button style="visibility:hidden" class="ll-d-flex ll-align-items-center">
                                            <span class="wcag-icon">
                                                <img alt="" src="<?php echo $this->assets_uri . "download-icon-white.png"; ?>" >
                                            </span>
                                            <span class="text">PDF REPORT</span>
                                        </button>
                                        <button class="ll-d-flex ll-align-items-center csv">
                                            <span class="wcag-icon">
                                                <img alt="" src="<?php echo $this->assets_uri . "download-icon-white.png"; ?>" >
                                            </span>
                                            <span class="text">CSV REPORT</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- REPORT BY PAGE-->
                    <div data-tab="1" class="ll-tab-content tab-1 active">
                        <!-- All Pages -->
                        <div class="body ll-row ll-no-gutters">

                        </div>
                    </div>
                    <!-- END REPORT BY PAGE -->
                    <div data-tab="2" class="ll-tab-content tab-2">
                        <div class="body ll-row ll-no-gutters">

                        </div>
                    </div>
                </div>
            </div>
        <?php
        
    }

    protected function _content_template() {
				
        // ob_start();
        ?>
         
        <?php
        // echo ob_get_clean();
	}
}