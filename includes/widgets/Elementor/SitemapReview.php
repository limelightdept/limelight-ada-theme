<?php
namespace OADA_CHILD\widgets\Elementor;

use Limelight\Plugins\ADA_Webservice\API;
use \Elementor\Widget_Base;
use Limelight_Child_Helpers AS LCH;

class SitemapReview extends Widget_Base {
    
    private static $sitemap_cache = false;
    
    private $domain,
            $revised,
            $path,
            $errors,
            $empty_sitemap,
            $sitemap;

    private function init(){
        $this->domain = LCH::get_domain(get_query_var("domain_id"));
        if(LCH::is_elementor()){
            $this->domain = LCH::get_domain(223);
        }
        
        $this->path = LCH::get_spath($this->domain->ID);

        if(isset($_POST["list"])){ //When saving from the review page
            $this->revised = json_decode(stripslashes($_POST["list"]), true);
            
            //Rewrites the file with the revisions
            LCH::generate_file($this->path, $this->revised);
            if(!LCH::is_elementor()){
               
                wp_redirect( home_url("/agency/sitemap") );
                exit;
            }
        }
        
        switch($_POST["_sitemap"]){
            case "create_new":                
                API::create_sitemap($this->domain->url, true);
                $this->sitemap = API::get_sitemap();
                unset($this->sitemap["domain"]);
                LCH::generate_file( $this->path, $this->sitemap );
                break;
            case "upload":
                if(!self::$sitemap_cache){
                    $this->filename = $this->server_validate_upload( $_FILES["fileUpload"] );
                    $this->sitemap = LCH::get_sitemap_from_file( $this->path.$this->filename, $this->domain->url );
                    if(empty($this->sitemap)) { $this->sitemap = ["non_query_links" => [], "query_links" => []]; }
                    self::$sitemap_cache = $this->sitemap;
                }else{
                    $this->sitemap = self::$sitemap_cache;
                }
                break;
            case "review_existing":           
                $this->sitemap = LCH::get_sitemap_from_file( $this->path, $this->domain->url );
                break;
        }
        
        //Don't need domain key on frontend, we have it in a variable $domain already
        unset($this->sitemap["domain"]);
        $this->empty_sitemap = "true";

        //Check for an empty sitemap        
        $keys = array_keys($this->sitemap);
        
        foreach($keys AS $key){
            if(!empty($this->sitemap[$key])){ $this->empty_sitemap = "false"; }
        }
        if($this->empty_sitemap === "true"){
            //Get errors            
            $this->errors = API::get_errors();
        }

        $this->widget_scripts();
        $this->widget_localize();
        $this->widget_styles();
    }

    private function convert_xml_to_csv( $xml, $f ){
        foreach ( $xml->children() as $item )
        {
            $hasChild = (count($item->children()) > 0)?true:false;

            if( !$hasChild ){
                if($item->getName() === "loc"){
                    $put_arr = [ $item ];
                    fputcsv( $f, $put_arr ,',','"' );
                }
            }else{
                $this->convert_xml_to_csv( $item, $f );
            }
        }
    }

    private function server_validate_upload($file){
        $is_csv = true;
        $supportedFormats = ['csv','xml'];
	    $pathinfo = pathinfo($file['name']);
        
	    if( $pathinfo["extension"] !== "csv" ){ $is_csv = false; }

        if( in_array($pathinfo["extension"], $supportedFormats) ){
            //Store the uploaded file to the server

            $this->filename = "sitemap.".$pathinfo["extension"];
            $this->path = LCH::get_spath($this->domain->ID, false);
            
            if(!file_exists($this->path)){
                mkdir( $this->path, 0777, true );
            }

            $success = move_uploaded_file( $file["tmp_name"], $this->path.$this->filename);
           
            if($success){
                $new_filename = "sitemap.csv";
                //Convert the file to CSV
                switch($pathinfo["extension"]){
                    case "xml":
                        if (file_exists($this->path.$this->filename)){
                            $xml = simplexml_load_file($this->path.$this->filename);
                            $f = fopen($this->path.$new_filename, 'w');
                            $this->convert_xml_to_csv($xml, $f);
                            $stat = fstat($f);
                            ftruncate($f, $stat['size']-1);
                            fclose($f);
                            unlink($this->path.$this->filename);
                        }
                        break;
                }
                return $new_filename;
            }
        }

	    return false;
    }
    
    public function widget_scripts(){
        \wp_enqueue_script("review-scripts", get_stylesheet_directory_uri() . "/assets/js/widget-scripts/review-scripts.js", ["jquery"], "1.0", true);
    }
    
    public function widget_styles(){
        \wp_enqueue_style("review-styles", get_stylesheet_directory_uri() . "/assets/styles/widget-styles/review-styles.css", "1.0", true);
    }

    public function widget_localize(){
        \wp_localize_script("review-scripts", "php_var", [
            "sitemap" => $this->sitemap,
            "empty_sitemap" => $this->empty_sitemap,
            // "use_query_strings" => $use_query_strings,
            // "errors" => $errors
        ]);
    }

    public function get_name() {
		return 'onlineada-sitemapreview';
    }
    
    public function get_title() {
		return __( 'Sitemap Review', 'limelight-theme-online-ada' );
    }
    
    public function get_icon() {
		return 'fa fa-code';
    }
    
    public function get_categories() {
		return [ 'online-ada' ];
    }
    
    protected function _register_controls() {
        $this->start_controls_section(
            'content_section',
            [
                'label' => __( 'Content', 'limelight-theme-online-ada' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'content_align',
            [
                'label' => __( 'Alignment', 'limelight-theme-online-ada' ),
                'type' => \Elementor\Controls_Manager::CHOOSE,
                'options' => [
                        'left' => [
                                'title' => __( 'Left', 'limelight-theme-online-ada' ),
                                'icon' => 'fa fa-align-left',
                        ],
                        'center' => [
                                'title' => __( 'Center', 'limelight-theme-online-ada' ),
                                'icon' => 'fa fa-align-center',
                        ],
                        'right' => [
                                'title' => __( 'Right', 'limelight-theme-online-ada' ),
                                'icon' => 'fa fa-align-right',
                        ],
                ],
                'default' => 'center',
                'toggle' => true,
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'save_sitemap_button_section',
            [
                'label' => __( 'Save Sitemap Button', 'limelight-theme-online-ada' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Typography::get_type(),
            [
                'name' => 'sitemap_button_typography',
                'label' => __( 'Button Typography', 'limelight-theme-online-ada' ),
                'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} button',
            ]
        );

        $this->add_control(
            'save_sitemap_button',
            [
                'label' => __( 'Save Sitemap Button text', 'limelight-theme-online-ada' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( 'Submit', 'limelight-theme-online-ada' ),
                'placeholder' => __( 'Enter something...', 'limelight-theme-online-ada' ),
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Border::get_type(),
            [
                'name' => 'border',
                'label' => __( 'Border', 'limelight-theme-online-ada' ),
                'selector' => '{{WRAPPER}} button',
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Background::get_type(),
            [
                'name' => 'background',
                'label' => __( 'Background', 'plugin-domain' ),
                'types' => [ 'classic', 'gradient', 'video' ],
                'selector' => '{{WRAPPER}} button',
            ]
        );

        $this->add_control(
            'save_sitemap_border_radius',
            [
                'label' => __( 'Border Radius', 'elementor' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();
    }
    
    protected function render() {
        $settings = $this->get_settings_for_display();
        $this->init();
        ?>
            <div class="entry-content">
                <div class="content-body" style="text-align:<?php echo $settings['content_align']; ?>">
                    <header class="head-wrap">
                        <h2>Review sitemap</h2>
                    </header>
                    <div class="body-wrap">
                        <form id="sitemap-review" action="?" method="POST">
                            <div class="input-wrap">
                                <label for="add"><?php echo $this->domain->url . "/"; ?></label>
                                <input type="text" name="add" />
                                <a class="sitemap-add" aria-label="Add to sitemap" href="#">ADD</a>
                            </div>
                            <div class="body-bottom">
                                <input type="hidden" name="list" value=""/>
                                <button><?php echo $settings["save_sitemap_button"]; ?></button>
                            </div>
                            <div class="body-top">
                                <ul class="sitemap">
                                    <!--populated by above script -->
                                </ul>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        <?php
    }

    protected function _content_template() {
        ?>
            <div class="entry-content">
                <div class="content-body" style="text-align:{{{settings.content_align}}}">
                    <header class="head-wrap" style="margin-bottom:10px;">
                        <h2>Review sitemap</h2>
                    </header>
                    <div class="body-wrap">
                        <form id="sitemap-review" action="?" method="POST">
                            <div class="input-wrap" style="margin-bottom:10px;">
                                <label for="add">A DOMAIN/</label>
                                <input type="text" name="add" />
                                <a class="sitemap-add" aria-label="Add to sitemap" href="#">ADD</a>
                            </div>
                            <div class="body-bottom">
                                <input type="hidden" name="list" value=""/>
                                <button>{{{settings.save_sitemap_button}}}</button>
                            </div>
                            <div class="body-top">
                                <ul class="sitemap">
                                    <!--populated by above script -->
                                </ul>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        <?php
	}
}