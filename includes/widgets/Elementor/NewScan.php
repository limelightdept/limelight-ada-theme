<?php
namespace OADA_CHILD\widgets\Elementor;

use Limelight\Plugins\ADA_Webservice\API;
use \Elementor\Widget_Base;
use Limelight_Child_Helpers AS LCH;

class NewScan extends Widget_Base {
		
    private $user, 
            $basedir, 
            $filename, 
            $domain, 
            $path,
            $sitemap, 
            $create_path, 
            $verified_domains,
            $base_sitemap,
            $domains;

    public function __construct( $data = array(), $args = null ){
        parent::__construct($data, $args);
    }

    private function init(){
        $this->user = wp_get_current_user();
        $this->basedir = wp_upload_dir()["basedir"];
        $this->domains = LCH::get_domains($this->user->ID);
        $this->verified_domains = [];

        foreach($this->domains AS $did => $domain){
            $this->path = $this->basedir."/users/{$domain['user_id']}/{$did}/sitemap.csv";
            if(is_file($this->path)){
                $this->verified_domains[$did] = $domain;
            }
        }

        $this->widget_scripts();
        $this->widget_styles();
    }
    //region
    public function widget_scripts(){
            \wp_enqueue_script("elem-widget-new-scan-js", get_stylesheet_directory_uri() . "/assets/js/widget-scripts/new-scan-scripts.js", ["jquery"], "1.0", true);
    }
    
    public function widget_styles(){
        \wp_enqueue_style("elem-widget-new-scan-css", "https://use.fontawesome.com/releases/v5.1.0/css/all.css", "1.0", true);
    }

    public function get_name() {
        return 'onlineada-newscan';
    }
    
    public function get_title() {
        return __( 'New Scan', 'limelight-theme-online-ada' );
    }
    
    public function get_icon() {
        return 'fa fa-code';
    }
    
    public function get_categories() {
        return [ 'online-ada' ];
    }
    
    protected function _register_controls() {

        $this->start_controls_section(
            'content_section',
            [
                'label' => __( 'Content', 'limelight-theme-online-ada' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
                'content_align',
                [
                        'label' => __( 'Alignment', 'limelight-theme-online-ada' ),
                        'type' => \Elementor\Controls_Manager::CHOOSE,
                        'options' => [
                                'left' => [
                                        'title' => __( 'Left', 'limelight-theme-online-ada' ),
                                        'icon' => 'fa fa-align-left',
                                ],
                                'center' => [
                                        'title' => __( 'Center', 'limelight-theme-online-ada' ),
                                        'icon' => 'fa fa-align-center',
                                ],
                                'right' => [
                                        'title' => __( 'Right', 'limelight-theme-online-ada' ),
                                        'icon' => 'fa fa-align-right',
                                ],
                        ],
                        'default' => 'center',
                        'toggle' => true,
                ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'header_section',
            [
                'label' => __( 'Header', 'limelight-theme-online-ada' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'new_scan_header',
            [
                'label' => __( 'New Scan Header', 'limelight-theme-online-ada' ),
                'type' => \Elementor\Controls_Manager::WYSIWYG,
                'default' => __( 'Select a website to scan', 'limelight-theme-online-ada' ),
                'placeholder' => __( 'Type your description here', 'limelight-theme-online-ada' ),
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Typography::get_type(),
            [
                'name' => 'header_typography',
                'label' => __( 'Header Typography', 'limelight-theme-online-ada' ),
                'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .header h2',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'message_section',
            [
                'label' => __( 'Message', 'limelight-theme-online-ada' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'new_scan_message',
            [
                'label' => __( 'New Scan Message', 'limelight-theme-online-ada' ),
                'type' => \Elementor\Controls_Manager::WYSIWYG,
                'default' => __( 'If you don\'t see a website here but have registered the domain, make sure you\'ve uploaded a sitemap for it <a href="'.home_url('/agency/sitemap').'">here</a>', 'limelight-theme-online-ada' ),
                'placeholder' => __( 'Type your description here', 'limelight-theme-online-ada' ),
            ]
        );
                
        $this->add_group_control(
            \Elementor\Group_Control_Typography::get_type(),
            [
                'name' => 'message_typography',
                'label' => __( 'Message Typography', 'limelight-theme-online-ada' ),
                'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .entry-content p',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'button_section',
            [
                'label' => __( 'Button', 'limelight-theme-online-ada' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Typography::get_type(),
            [
                'name' => 'button_typography',
                'label' => __( 'Button Typography', 'limelight-theme-online-ada' ),
                'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} button',
            ]
        );

        $this->add_control(
            'new_scan_button',
            [
                'label' => __( 'Button text', 'limelight-theme-online-ada' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( 'Submit', 'limelight-theme-online-ada' ),
                'placeholder' => __( 'Enter something...', 'limelight-theme-online-ada' ),
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Border::get_type(),
            [
                    'name' => 'border',
                    'label' => __( 'Border', 'limelight-theme-online-ada' ),
                    'selector' => '{{WRAPPER}} button',
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Background::get_type(),
            [
                    'name' => 'background',
                    'label' => __( 'Background', 'plugin-domain' ),
                    'types' => [ 'classic', 'gradient', 'video' ],
                    'selector' => '{{WRAPPER}} button',
            ]
        );

        $this->add_control(
            'border_radius',
            [
                    'label' => __( 'Border Radius', 'elementor' ),
                    'type' => \Elementor\Controls_Manager::DIMENSIONS,
                    'size_units' => [ 'px', '%' ],
                    'selectors' => [
                            '{{WRAPPER}} button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                    ],
            ]
        );

        $this->end_controls_section();

    }
    //endregion
    protected function render() {
        $settings = $this->get_settings_for_display();
        $this->init();
        
        ?>
        <div class="content-wrapper" style="text-align: <?php echo $settings['content_align']; ?>">
            <div class="content-header">
                <header class="head-wrap">
                    <h2><?php echo $settings["new_scan_header"]; ?></h2>
                </header>
            </div>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="entry-content">
                    <div class="body-wrap content-body">
                        <form action="" method="post">
                            <div class="select-wrap">
                                <select class="website-select ll-input" style="width:initial;margin-bottom:16px;" name="website-select">
                                    <?php foreach($this->domains AS $k => $v): ?>
                                        <?php if( !empty($this->verified_domains[$k]) ): ?>
                                            <option value="<?php echo $k ?>">
                                                <?php echo untrailingslashit($v['url']); ?>
                                            </option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <button class="submit-button"><?php echo $settings["new_scan_button"]; ?></button>
                        </form>
                    <?php if(in_array("compliance_audit_user", $this->user->roles) || in_array("agency_user", $this->user->roles)): ?>
                        <p><?php echo $settings["new_scan_message"]; ?></p>
                    <?php endif; ?>
                    </div>
                </div>
            </article>
        </div>
        <?php
            
    }

    protected function _content_template() {
        ?>
        <div class="content-wrapper" style="text-align: {{{settings.content_align}}}">
            <div class="content-header">
                <header class="head-wrap">
                    <h2>{{{ settings["new_scan_header"] }}}</h2>
                </header>
            </div>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="entry-content">
                    <div class="body-wrap content-body">
                        <form action="" method="post">
                            <div class="select-wrap">
                                <select class="website-select ll-input" name="website-select">
                                    
                                </select>
                            </div>
                            <button class="submit-button">{{{settings["new_scan_button"]}}}</button>
                        </form>
                    
                        <p>{{{ settings["new_scan_message"] }}}</p>
                    
                    </div>
                </div>
            </article>
        </div>
        <?php
	}
}