<?php
namespace OADA_CHILD\widgets\Elementor;

use \Elementor\Widget_Base;
use Limelight\Plugins\ADA_Webservice\API;
use Limelight_Child_Helpers as LCH;

class CertificationBackend extends Widget_Base {
		
    private $post,
            $user,
            $domain,
            $status,
            $date_certified,
            $badge_link;

    public function __construct( $data = array(), $args = null ){
        parent::__construct($data, $args);
    }

    private function init(){
        
        //$this->post = get_post($wp_query->query_vars["ada_certification"]);
        $this->post = get_post(get_query_var("ada_certification"));
        $this->user = get_user_by("ID", $this->post->post_author);
        $this->domain = get_post_meta($this->post->ID, "_ada_cert_domain", true);
        $this->status = get_post_meta($this->post->ID, "_ada_cert_status", true);
        $this->date_certified = get_post_meta($this->post->ID, "_ada_cert_date_certified", true);
        $this->badge_link = get_post_meta($this->post->ID, "_ada_cert_badge", true);
        
        switch($this->status){
            case "in_review":
                $this->status = "In Review";
                break;
            case "not_certified":
                $this->status = "Not Certified";
                break;
        }

    }

    private function filter_body_content($msg){
        $msg = str_replace("[domain]", $this->domain, $msg);
        $msg = str_replace("[status]", "<span style='font-weight:bold;'>".$this->status."</span>", $msg);
        $msg = str_replace("[user-name]", $this->user->display_name, $msg);
        $msg = str_replace("[date-purchased]", get_post_meta($this->post->ID, "_ada_cert_purchase_date", true), $msg);
        $msg = str_replace("[date-certified]", $this->date_certified, $msg);

        $snippet = '<div class="badge-content" style="color:black;width:100%;">';
        $snippet .=     trim(esc_html($this->badge_link));
        $snippet .= '</div>';

        $src = content_url(get_post_meta($this->post->ID, "_ada_cert_badge_upload_path", true));

        if($this->status == "certified"){
            $msg = str_replace("[cond-certified:1]", "and was certified on ". $this->date_certified . ".", $msg);
            $certified_condition_2 = "<p>This is what your public badge will look like:</p>".
                                     "<img style='width:200px;' src='{$src}' />".
                                     "<p>Here is your Badge HTML:</p>".
                                     $snippet;
            $msg = str_replace("[cond-certified:2]", $certified_condition_2, $msg);
            $msg = str_replace("[cond-not-certified]", "", $msg);
        }else{
            $msg = str_replace("[cond-certified:1]", "", $msg);
            $msg = str_replace("[cond-certified:2]", "", $msg);
            $msg = str_replace("[cond-not-certified]", ".", $msg);
        }
        
        return $msg;
    }

    public function get_name() {
		  return 'onlineada-certificationbackend';
    }
    
    public function get_title() {
		  return __( 'Certification Backend', 'limelight-theme-online-ada' );
    }
    
    public function get_icon() {
		  return 'fa fa-code';
    }
    
    public function get_categories() {
		  return [ 'online-ada' ];
    }
    
    protected function _register_controls() {

        $this->start_controls_section(
            'content_section',
            [
                'label' => __( 'Content', 'limelight-theme-online-ada' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'content_align',
            [
                'label' => __( 'Alignment', 'limelight-theme-online-ada' ),
                'type' => \Elementor\Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __( 'Left', 'limelight-theme-online-ada' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'limelight-theme-online-ada' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'limelight-theme-online-ada' ),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'default' => 'center',
                'toggle' => true,
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'body_section',
            [
                'label' => __( 'Body', 'limelight-theme-online-ada' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'body_content',
            [
                'label' => __( 'Body Content: - Available variables: [domain], [date-certified], [status], [date-purchased], [user-name], [badge-snippet], [badge_img]', 'limelight-theme-online-ada' ),
                'type' => \Elementor\Controls_Manager::WYSIWYG,
                'default' => __( "", 'limelight-theme-online-ada' ),
                'placeholder' => __( 'Type your description here', 'limelight-theme-online-ada' ),
            ]
        );

        $this->end_controls_section();

    }
    
    protected function render() {
        $settings = $this->get_settings_for_display();
        $this->init($settings);
        
        if(!LCH::is_elementor()){
            //$this->widget_scripts();
            //$this->widget_localize();
            //$this->widget_styles();
        }
        
        ?>
            <style>
                .badge-content{background-color:#eee;border:1px solid black;text-align:center;display:flex;height:auto;width:100%;justify-content: center;align-items:center;box-sizing: border-box;padding: 20px 5px;}
            </style>
            <div class="entry-content">
                <div class="content-wrapper" style="text-align:<?php echo $settings["content_align"]; ?>;">
                    <?php echo $this->filter_body_content($settings["body_content"]); ?>                    
                </div>
            </div>
        <?php
    }

    protected function _content_template() { 
        ?>
            <div class="entry-content">
                <div class="content-wrapper" style="text-align:{{{settings.content_align}}};">
                    {{{settings.body_content}}}
                </div>
            </div>       
        <?php
	}
}