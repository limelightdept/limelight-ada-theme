<?php

class OADA_Elementor {

    private $widgets;
    
    public function __construct(){
        
        $this->widgets = [
            "OADA_CHILD\widgets\Elementor\NewScan", 
            "OADA_CHILD\widgets\Elementor\MembersNav",
            "OADA_CHILD\widgets\Elementor\NewScanOptions",
            "OADA_CHILD\widgets\Elementor\ScanReport",
            "OADA_CHILD\widgets\Elementor\CertifiedBadgeContent",
            "OADA_CHILD\widgets\Elementor\CertificationBackend",
            "OADA_CHILD\widgets\Elementor\Sitemap",
            "OADA_CHILD\widgets\Elementor\SitemapReview",
            "OADA_CHILD\widgets\Elementor\PreviousScan",
        ];
        /*Add Custom Categories*/
        add_action( 'elementor/elements/categories_registered', [$this, 'register_categories'] );
        /*Add Custom Widgets*/
        add_action( 'elementor/widgets/widgets_registered', [$this, 'register_widgets'] );
    }

    public function register_categories( $elements_manager ) {

        $elements_manager->add_category(
            'online-ada',
            [
                'title' => __( 'OnlineADA Theme', 'limelight-theme-ada-theme' ),
                'icon' => 'fa fa-plug',
            ]
        );
    }
    public function register_widgets( $widgets_manager ) {
        foreach($this->widgets AS $name){
            $widgets_manager->register_widget_type( new $name() );
        }
    }
}

$oada_elementor = new OADA_Elementor();