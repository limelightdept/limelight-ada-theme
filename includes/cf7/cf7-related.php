<?php
use Carbon\Carbon;

add_action( 'wpcf7_before_send_mail', 'wpcf7_submit', 10, 2 );
function wpcf7_submit( $form, $result ) {
	if(stripos($form->title(), "Domain Form") !== false){
		$user = get_user_by("ID", $_POST["user-id"]);
		$meta_prefix = "_ll_audit_";

		if(stripos($form->title(), "Free") !== false){
			$domain = untrailingslashit($_POST["domain-1"]);
			$protocol = $_POST['protocol'][0];

			$existing_domains = count(get_posts(["author" => $user->ID, "post_type" => "compliance_audit"]));

			if($existing_domains === 0){
				$audit_meta = [
					$meta_prefix."purchase_date"   => Carbon::now("UTC")->toDateString(),
					$meta_prefix."domain" 	       => $protocol . "://" . $domain,
				];

				$audit_data = [
					"post_type"    => "compliance_audit",
					"post_title"   => $protocol . $domain,
					"post_status"  => "publish",
					"post_author"  => $user->ID,
					"meta_input"   => $audit_meta,
				];

				$result = wp_insert_post($audit_data);

				if(!is_wp_error($result)){
					// $scan_url = home_url("audit/") . $result . "/";
					// update_post_meta($result, "_ll_audit_new_scan_url", $scan_url );
					update_user_meta($user->ID, "_limelight_domain_input_complete", 1);
					$user->add_cap( "member_area_access" );
				}
			}

		}else{
			$audit_meta = [];
			$meta = get_user_meta($user->ID, "_audit_data_to_save", true);
			$errors = [];
			foreach($meta AS $key => $v){
				$audit_meta[$meta_prefix . $key] = $v;
			}

			$license_option = get_user_meta($user->ID, "_limelight_license_option", true);
			$audits = get_posts(["author" => $user->ID, "post_type" => "compliance_audit"]);
			$existing_domains = [];
			foreach($audits AS $audit){
				$existing_domains[] = get_post_meta($audit->ID, "_ll_audit_domain", true);
			}
			$domains_used = count($existing_domains);

			for($i = 1; $i <= $license_option; $i++){
				$domain = untrailingslashit($_POST["domain-".$i]);
				$protocol = $_POST['protocol'][$i-1] . "://";


				if( !empty($domain) AND
					!in_array($domain, $existing_domains) AND
					$i > $domains_used AND
					$i <= $license_option)
				{
					$audit_data = [
						"post_type"    => "compliance_audit",
						"post_status"  => "publish",
						"post_title"   => $protocol . $domain,
						"post_author"  => $user->ID,
						"meta_input"   => $audit_meta,
					];
					$audit_meta[$meta_prefix."domain"] = $protocol . $domain;
					$audit_data["meta_input"] = $audit_meta;

					$result = wp_insert_post($audit_data);
					if(!is_wp_error($result)){
						// $scan_url = home_url("audit/") . $result . "/";
						// update_post_meta($result, "_ll_audit_new_scan_url", $scan_url );
					}
					if(is_wp_error($result)){
						$errors[] = $result;
					}
				}
			}
			if(empty($errors)){
				update_user_meta($user->ID, "_limelight_domain_input_complete", 1);
				$user->add_cap( "member_area_access" );
			}else{
				wp_die($errors);
			}
		}

		wp_redirect( home_url("/new-scan") );
		exit;
	}

	//if(stripos($form->title(), "Domain Form") !== false)
	//echo "<pre>";var_dump($form->title());die;
}

function validate_forms($default){
	return
    [
        "Certification Registration Form" =>
		[
			"user_pass"   	=> "your-password",
			"user_email"  	=> "your-email",
			"confirm_pass"  => "confirm-password",
			"first_name" 	=> "",
			"last_name" 	=> "",
			"role" 		  	=> "certification_user",
			"validation"    => ["registration"],
			"type"          => "user_registration",
		],
        "Certification Shipping Form" =>
		[
			"business-name" => "business-name",
			"street"        => "street",
			"city"          => "city",
			"state"         => "state",
			"zip"           => "zip",
			"phone"         => "phone",
			"web-domain"    => "web-domain",
			"validation"    => ["shipping"],
			"type"          => "process_meta",
		],
        "Contact Form" =>
		[
			"full-name"  => "full-name",
			"your-email" => "your-email",
			"phone"      => "phone",
			"message"    => "message",
			"validation" => ["contact"],
			"type"       => "process_meta"
		],
        "Audit Signup Form: Free" =>
		[
			"user_pass"   	=> "your-password",
			"user_email"  	=> "your-email",
			"confirm_pass"  => "confirm-password",
			"first_name" 	=> "first_name",
			"last_name" 	=> "",
			"role" 		  	=> "free_compliance_audit_user",
			"validation"    => ["registration"],
			"type"          => "user_registration",
		],
        "Audit Signup Form: Single" =>
		[
			"user_pass"   	=> "your-password",
			"user_email"  	=> "your-email",
			"confirm_pass"  => "confirm-password",
			"first_name" 	=> "first_name",
			"last_name" 	=> "",
			"role" 		  	=> "compliance_audit_user",
			"validation"    => ["registration"],
			"type"          => "user_registration",
		],
		"Audit Signup Form: Multi-5" =>
		[
			"user_pass"   	=> "your-password",
			"user_email"  	=> "your-email",
			"confirm_pass"  => "confirm-password",
			"first_name" 	=> "first_name",
			"last_name" 	=> "",
			"role" 		  	=> "compliance_audit_user",
			"validation"    => ["registration"],
			"type"          => "user_registration",
		],
		"Audit Signup Form: Multi-10" =>
		[
			"user_pass"   	=> "your-password",
			"user_email"  	=> "your-email",
			"confirm_pass"  => "confirm-password",
			"first_name" 	=> "first_name",
			"last_name" 	=> "",
			"role" 		  	=> "compliance_audit_user",
			"validation"    => ["registration"],
			"type"          => "user_registration",
		],
		"Domain Form: 10" =>
		[
			"domain-1"   	=> "domain-1",
			"domain-2"  	=> "domain-2",
			"domain-3"  	=> "domain-3",
			"domain-4" 		=> "domain-4",
			"domain-5" 		=> "domain-5",
			"domain-6" 		=> "domain-6",
			"domain-7" 		=> "domain-7",
			"domain-8" 		=> "domain-8",
			"domain-9" 		=> "domain-9",
			"domain-10" 	=> "domain-10",
			"validation"    => ["domain_10"],
			"type"          => "domain",
		],
		"Domain Form: 5" =>
		[
			"domain-1"   	=> "domain-1",
			"domain-2"  	=> "domain-2",
			"domain-3"  	=> "domain-3",
			"domain-4" 		=> "domain-4",
			"domain-5" 		=> "domain-5",
			"validation"    => ["domain_5"],
			"type"          => "domain",
		],
		"Domain Form: Single" =>
		[
			"domain-1"   	=> "domain-1",
			"validation"    => ["domain"],
			"type"          => "",
		],
		"Domain Form: Free" =>
		[
			"domain-1"   	=> "domain-1",
			"validation"    => ["domain"],
			"type"          => "",
		],
		"Free Scan Dom" =>
		[
			"domain-1"   	=> "domain-1",
			"validation"    => ["domain"],
			"type"          => "",
		],
		// "Free Scan Signup Form" =>
		// [
			// "user-name"   	=> "user-name",
			// "user-email"   	=> "user-email",
			// "validation"    => ["registration"],
			// "type"          => "user-registration",
		// ],
    ];
}
add_filter( "ll-theme-cf7-validate_forms", "validate_forms", 10, 2);

add_filter("ll-theme-cf7-create-message", "create_email_message", 10, 2);
function create_email_message($message, $userdata){
	//set the variables as global query variables so the template has access to them
	set_query_var("message", $message);
	set_query_var("data", $userdata);
	ob_start();
	get_template_part("templates/partials/email/certification-sign-up");
	return ob_get_clean();
}

/**
  * Intercepts the form action and replaces it with the login URL if the form is a login form.
  *
  * Uses the ID of the forms for which we are checking to identify if they are login forms, and if they are, replace the URL.
  *
  * @param string $url The original form action URL
  *
  * @return string The newly modified login URL OR the original URL if the form is not a login form.
  */
function filter_wpcf7_form_action_url($url){
    $login_forms = ["#wpcf7-f399", "#wpcf7-f665", "#wpcf7-f1316"];

    foreach($login_forms AS $id){
        if(strpos($url, $id) !== false){
            return wp_login_url();
        }
    }
	//Clean the urls
	if(stripos($url, "?") !== false){
		$url = explode("?", $url)[0];
	}
	if(stripos($url, "#") !== false){
		$url = explode("#", $url)[0];
	}
    return $url;
}
add_filter("wpcf7_form_action_url", "filter_wpcf7_form_action_url", 10, 1);

function filter_wpcf7_skip_mail( $skip_mail, $contact_form ) {
	$dont_skip_titles = [ "Contact Form", "Certification Lead Form"];
	$skip_mail = true;
    if( in_array( $contact_form->title(), $dont_skip_titles ) ){
		$skip_mail = false;
	}
    return $skip_mail;
};

add_filter( 'wpcf7_skip_mail', 'filter_wpcf7_skip_mail', 10, 2 );

add_filter( 'wpcf7_validate', 'ada_form_validation', 10, 2 );
function ada_form_validation($result, $tags){
	if(!isset($_POST)) return $result;
    if(!isset($_POST['_wpcf7'])) return $result;
	$post = get_post($_POST['_wpcf7']);
	if($post->post_title === "Free Scan Dom"){
		if(isset($_POST["domain-1"])){
			foreach($tags AS $tag){
				switch($tag->name){
					//case "domain-1":
						// $value = $_POST["domain-1"];
						// $regex = "/^https?:\/{2}[w]{0,3}[\w\d]+[\.]?[\w\d]*[\.]{1}[\w]{1,3}/";
						// if(!preg_match($regex, $value)){
							// $result->invalidate($tag, "Please enter a valid url starting with the protocol");
						// }
						//break;
				}
			}
		}
	}

	if($post->post_title === "Free Scan Signup Form"){
		foreach($tags AS $tag){
			switch($tag->name){
				case "user-email":
					$value = $_POST["user-email"];
					if(email_exists($value)){
						$result->invalidate($tag, "Sorry this e-mail address is in use.");
					}
					break;
				case "user-confirm":
					$value = $_POST["user-confirm"];
					if($_POST["user-password"] !== $value){
						$result->invalidate($tag, "Password and confirm password do not match");
					}
					break;
			}
		}
	}

	return $result;
}

add_filter("wpcf7_ajax_json_echo", function($response, $result){

	$form = wpcf7_contact_form( $result["contact_form_id"] );

	if($form->title() === "Free Scan Dom"){
		$data = WPCF7_Submission::get_instance();
		$data = $data->get_posted_data();

		$domain = $data["domain-1"];
		$protocol = $data["protocol"][0];

		$response["domain"] = $domain;
		$response["protocol"] = $protocol;
	}

	return $response;
}, 10, 2);
