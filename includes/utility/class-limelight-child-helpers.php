<?php
use Limelight\Plugins\ADA_Webservice\API;

if ( ! class_exists( 'Limelight_Child_Helpers' ) ) {
define("EXTERNAL_QUICKREF_URL", "https://www.w3.org/WAI/WCAG20/quickref/");
define("INTERNAL_QUICKREF_URL", "");
	class Limelight_Child_Helpers{
		static public $custom_cert_columns = [
			"cb" => "",
			"id" => "ID",
			"purchase_date" => "Purchase Date",
			"user" => "Owner",
			"status" => "Status",
			"domain" => "Domain",
			"order_id" => "Order ID",
			"badge" => "Badge"
		];
		
		static public $cert_columns_sort_map = [
			"cb" => "none",
			"id" => "ID",
			"domain" => "domain",
			"order_id" => "order_id",
			"owner" => "author",
		];
		
		static function is_elementor(){
			return \Elementor\Plugin::$instance->editor->is_edit_mode() || \Elementor\Plugin::$instance->preview->is_preview_mode();
		}
		
		static function create_table()
		{
			global $wpdb;

			$table_name = $wpdb->prefix . "domains";
			$charset_collate = $wpdb->get_charset_collate();

			$sql = "CREATE TABLE $table_name (
				ID bigint(20) unsigned NOT NULL AUTO_INCREMENT,
				user_id bigint(20) unsigned NOT NULL,
				url varchar(255) NOT NULL,
				last_scanned varchar(255) NOT NULL,
				PRIMARY KEY  (ID)
			) $charset_collate;";

			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($sql);
		}
		
		static function migrate_audit_data(){
			self::create_table();
			
			global $wpdb;

			//Update column name on audit_results
			$wpdb->query("ALTER TABLE {$wpdb->prefix}audit_results CHANGE auditID domainID bigint(20) unsigned");
			
			//get all audits
			$args = [
				"post_type" => "compliance_audit",
				"posts_per_page" => "-1"
			];
			$audits = get_posts($args);
			
			foreach($audits as $a){
				//Get the relevant columns from post meta data and add them to the domains table columns
				$full_domain = get_post_meta($a->ID, "_ll_audit_domain", true);
				$last_scanned = get_post_meta($a->ID, "_ll_audit-completion-date", true);

				$wpdb->query("INSERT INTO {$wpdb->prefix}domains (user_id, url, last_scanned) VALUES ({$a->post_author}, '{$full_domain}', '{$last_scanned}')");
				
				$dom_ob = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}domains WHERE user_id = '{$a->post_author}'")[0];
				
				//Move files into new file structure
				$basedir = wp_upload_dir()["basedir"];
				$domain_base = self::split_domain($full_domain)["domain"];
				
				$newdir = $basedir."/users/{$a->post_author}/{$dom_ob->ID}/";
				
				if(!is_dir($newdir)){
					mkdir($newdir, 0777, true);
				}
				
				$filename = "sitemap.csv";
				$old_path = $basedir."/{$a->post_author}/{$domain_base}/sitemap/";
				
				if(file_exists($old_path.$filename)){
					rename($old_path.$filename, $newdir.$filename);
					self::rrmdir($old_path);
				}

				$filename = "cbr.pdf";
				$old_path = $basedir."/{$a->post_author}/{$domain_base}/color-blind-report/";
				
				if(file_exists($old_path.$filename)){
					rename($old_path.$filename, $newdir.$filename);
					self::rrmdir($old_path);
				}

				$filename = "snapshot.png";
				$old_path = $basedir."/{$a->post_author}/{$domain_base}/";
				
				if(file_exists($old_path.$filename)){
					rename($old_path.$filename, $newdir.$filename);
					self::rrmdir($old_path);
				}

				//Update the audit results with new domain ids
				$wpdb->query("UPDATE {$wpdb->prefix}audit_results SET domainID={$dom_ob->ID} WHERE domainID={$a->ID}");

				//delete old audit data
				$wpdb->query("DELETE FROM {$wpdb->prefix}posts WHERE ID = {$a->ID}");
				$wpdb->query("DELETE FROM {$wpdb->prefix}postmeta WHERE post_id = {$a->ID}");
			}
			
			die("complete");
		}
		static function rrmdir($dir) {
			if (is_dir($dir)) {
				$objects = scandir($dir); 
				foreach ($objects as $object) { 
					if ($object != "." && $object != "..") { 
						if (is_dir($dir."/".$object))
							rrmdir($dir."/".$object);
						else
							unlink($dir."/".$object);
					}
				}
				rmdir($dir);
			}
		}
		static function add_domain($args){
			global $wpdb;
			$vals = implode(", ", array_values($args));
			
			$sql = "INSERT INTO {$wpdb->prefix}domains (user_id, url, last_scanned) VALUES ({$vals})";
			$wpdb->query($sql);
			$domain_id = $wpdb->get_results("SELECT ID FROM {$wpdb->prefix}domains WHERE user_id = {$args['user_id']} AND url = {$args['url']} AND last_scanned = {$args['last_scanned']}")[0]->ID;
			
			return $domain_id;
		}
		static function save_domains($domains){
			if(!is_user_logged_in()){
				return false;
			}
			
			global $wpdb;
			$user_id = get_current_user_id();
			$inserts = [];
			
			foreach($domains as $domain){
				$full_domain = sanitize_text_field($domain['protocol']).'://'.sanitize_text_field($domain['domain']);
				if(!empty($domain['last_scanned'])){
					$last_scanned = sanitize_text_field($domain['last_scanned']);
				}else{
					$last_scanned = "";
				}
				
				$inserts[] = "({$user_id}, '{$full_domain}', '{$last_scanned}')";
			}
			$insert_str = implode(",", $inserts);
			
			$sql = "DELETE FROM {$wpdb->prefix}domains WHERE user_id = {$user_id}";
			$wpdb->query($sql);

			$sql = "INSERT INTO {$wpdb->prefix}domains (user_id, url, last_scanned) VALUES ({$insert_str})";
			$wpdb->query($sql);
		}
		static function get_domains($uid, $zero_indexed = false){
			global $wpdb;

			$results =  $wpdb->get_results("SELECT * FROM {$wpdb->prefix}domains WHERE user_id = {$uid}");
			$domains = [];

			foreach($results AS $r){
				if($zero_indexed){
					$domains[] = (array)$r;
				}else{
					$domains[$r->ID] = (array)$r;
				}
				
			}

			return $domains;
		}
		static function get_domain($did){
			global $wpdb;

			$results =  $wpdb->get_results("SELECT * FROM {$wpdb->prefix}domains WHERE ID = {$did}");			

			return $results[0];
		}
		static function get_sitemap($uid, $did){
			$sitemap = false;
			
			$path = wp_upload_dir()["basedir"]."/users/{$uid}/{$did}/sitemap.csv";
			if(file_exists($path) && is_file($path)){
				$sitemap = $path;
			}
			
			return $sitemap;
		}
		static function get_domain_by_id($id){
			global $wpdb;
			$table_name = $wpdb->prefix . "domains";

			return $wpdb->get_results("SELECT * FROM {$table_name} WHERE ID = {$id}")[0];
		}
		static function get_file_uri($str, $did){
			if(!is_user_logged_in()){
				return false;
			}

			$uid = wp_get_current_user()->ID;
			$path = "";

			switch($str){
				case "cbr":
					$path = wp_upload_dir()["baseurl"]."/users/{$uid}/{$did}/cbr.pdf";
					break;
				case "sitemap":
					$path = wp_upload_dir()["baseurl"]."/users/{$uid}/{$did}/sitemap.csv";
					break;
				case "snapshot":
					$path = wp_upload_dir()["baseurl"]."/users/{$uid}/{$did}/snapshot.png";
					break;
			}

			return $path;
		}
		static function get_file($str, $did){
			if(!is_user_logged_in()){
				return false;
			}

			$uid = wp_get_current_user()->ID;
			$path = "";

			switch($str){
				case "cbr":
					$path = wp_upload_dir()["basedir"]."/users/{$uid}/{$did}/cbr.pdf";
					break;
				case "sitemap":
					$path = wp_upload_dir()["basedir"]."/users/{$uid}/{$did}/sitemap.csv";
					break;
				case "snapshot":
					$path = wp_upload_dir()["basedir"]."/users/{$uid}/{$did}/snapshot.png";
					break;
			}

			return $path;
		}
		static function get_spath($did, $include_filename = true){
			if(!is_user_logged_in()){
				return false;
			}
			$uid = wp_get_current_user()->ID;
			
			$path = wp_upload_dir()["basedir"]."/users/{$uid}/{$did}/sitemap.csv";
			if(!$include_filename){
				$path = wp_upload_dir()["basedir"]."/users/{$uid}/{$did}/";
			}
			
			return $path;
		}
		static function get_scan_data($domain_id)
		{
			if (empty($domain_id)) {
				return false;
			}

			global $wpdb;
			$table_name = $wpdb->prefix . "audit_results";

			$results = $wpdb->get_results("SELECT * FROM {$table_name} WHERE domainID = {$domain_id}");
			
			$sendBack = [
				"non-contrast" => [],
				"contrast" => []
			];
			foreach ($results as $result) {
				$result->page_results = maybe_unserialize($result->page_results);

				$sendBack["non-contrast"] = array_merge($sendBack["non-contrast"], $result->page_results["non-contrast"]);
				$sendBack["contrast"] = array_merge($sendBack["contrast"], $result->page_results["contrast"]);
			}

			return $sendBack;
		}
		static function save_scan_data($args)
		{
			if (gettype($args) == "array") {
				$args = (object)$args;
			}
			if (gettype($args) !== "object") {
				return false;
			}

			global $wpdb;

			$table_name = $wpdb->prefix . "audit_results";

			$store = [
				"non-contrast" => [],
				"contrast" => []
			];

			foreach ($args->page_results as $k => $v) {
				if ($k === "contrast") {
					$store[$k] = $v;
					continue;
				}

				$store["non-contrast"][] = $v;
			}

			$success = $wpdb->insert($table_name, array(
				"domainID" => $args->domainID,
				"page" => $args->page,
				"page_results" => maybe_serialize($store)
			));

			return $success;
		}
		static function delete_audit_data($domain_id)
		{
			if (empty($domain_id)) {
				return false;
			}

			global $wpdb;
			$table_name = $wpdb->prefix . "audit_results";

			$success = $wpdb->delete($table_name, ['domainID' => $domain_id]);
			return $success;
		}
		static function check_user_roles($role, $user=null){
			if(is_null($user)){
				$user = wp_get_current_user();
			}

			if(gettype($user) == "integer"){
				$user = get_user_by("ID", $user);
			}

			$role = trim(strtolower($role));
			return in_array($role, $user->roles);
		}
		static function checkAuditUser($user){
			if(in_array("free_compliance_audit_user", $user->roles)){
				return "free_compliance_audit_user";
			}
			if(in_array("compliance_audit_user", $user->roles)){
				return "compliance_audit_user";
			}
			return false;
		}
		static function get_sitemap_from_file($path, $domain){
			$api = new API;
			$row = 0;
			$headers = [ "query_links", "non_query_links" ];
			$arr = [];
			if (($handle = fopen( $path, "r" )) !== FALSE) {
				$key = "";
				while (($data = fgetcsv( $handle, 0, "," )) !== FALSE) {
					$data = array_shift( $data );
					if(in_array( $data, $headers )){
						$key = $data;
						continue;
					}

					$arr[] = $data;
					$row++;
				}
				fclose( $handle );
			}
			
			if(!empty($arr)){
				foreach($arr AS $index => $url){
					$api::parse_url($url, $domain);
				}
				
				return $api::get_sitemap();
			}
		}
		static function generate_file($path, $sitemap){
			//Logic only works for CSV
			//Convert all file types to CSV
			if(!file_exists($path)){
				mkdir( $basedir . $rel_path, 0777, true );
			}
			$file = fopen($path, "w");
			$headers = array_keys($sitemap);
			foreach($headers AS $key){
				if($key == "domain"){ return; }
				fputcsv($file, [$key]);
				foreach($sitemap[$key] AS $row){
					fputcsv($file, [$row]);
				}
			}
			//truncate empty line off end of file
			$stat = fstat($file);
			ftruncate($file, $stat['size']-1);
			fclose($file);
		}
		static function split_domain($domain){
			$protocol_regex = '/https?:\/{2}/';
			preg_match($protocol_regex, $domain, $protocol);
			$protocol = str_replace( "://", "", $protocol[0] );
			$domain = trim(preg_replace($protocol_regex, "", $domain), '"');
			return ["domain" => $domain, "protocol" => $protocol];
		}
		static function filter_vars($buffer, $vars){
			foreach($vars AS $k => $v){
			   $buffer = str_replace( "[[". $k ."]]", $vars[$k], $buffer );
			}
			return $buffer;
		}
		static function getSection508ChaptersByType(){
			$array = [
				"Image"  => ["a"],
				"iFrame" => ["i"],
				"Video"  => ["b", "j"],
				"Table"  => ["g", "h"],
				//"Media File" => ["a", "b"],
				"Audio"  => ["a", "b"],
				//"Object" => [],
				//"Embed"  => [],
				"Form"   => ["n"],
			];

			return $array;
		}
		static function getSection508ArticlesModel(){
			$s508 = [
				"guideline" => "1194.22",
				"a" => [
					'title' => 'A text equivalent for every non-text element shall be provided (e.g., via "alt", "longdesc", or in element content).',
					'description' => '',
				],
				"b" => [
					'title' => 'Equivalent alternatives for any multimedia presentation shall be synchronized with the presentation.',
					'description' => '',
				],
				"c" => [
					'title' => 'Web pages shall be designed so that all information conveyed with color is also available without color, for example from context or markup.',
					'description' => '',
				],
				"d" => [
					'title' => 'Documents shall be organized so they are readable without requiring an associated style sheet.',
					'description' => '',
				],
				"e" => [
					'title' => 'Redundant text links shall be provided for each active region of a server-side image map.',
					'description' => '',
				],
				"f" => [
					'title' => 'Client-side image maps shall be provided instead of server-side image maps except where the regions cannot be defined with an available geometric shape.',
					'description' => '',
				],
				"g" => [
					'title' => 'Row and column headers shall be identified for data tables.',
					'description' => '',
				],
				"h" => [
					'title' => 'Markup shall be used to associate data cells and header cells for data tables that have two or more logical levels of row or column headers.',
					'description' => '',
				],
				"i" => [
					'title' => 'Frames shall be titled with text that facilitates frame identification and navigation.',
					'description' => '',
				],
				"j" => [
					'title' => 'Pages shall be designed to avoid causing the screen to flicker with a frequency greater than 2 Hz and lower than 55 Hz.',
					'description' => '',
				],
				"k" => [
					'title' => 'A text-only page, with equivalent information or functionality, shall be provided to make a web site comply with the provisions of this part, when compliance cannot be accomplished in any other way. The content of the text-only page shall be updated whenever the primary page changes.',
					'description' => '',
				],
				"l" => [
					'title' => 'When pages utilize scripting languages to display content, or to create interface elements, the information provided by the script shall be identified with functional text that can be read by assistive technology.',
					'description' => '',
				],
				"m" => [
					'title' => 'When a web page requires that an applet, plug-in or other application be present on the client system to interpret page content, the page must provide a link to a plug-in or applet that complies with 1194.21(a) through (l).',
					'description' => '',
				],
				"n" => [
					'title' => 'When electronic forms are designed to be completed on-line, the form shall allow people using assistive technology to access the information, field elements, and functionality required for completion and submission of the form, including all directions and cues.',
					'description' => '',
				],
				"o" => [
					'title' => 'A method shall be provided that permits users to skip repetitive navigation links.',
					'description' => '',
				],
				"p" => [
					'title' => 'When a timed response is required, the user shall be alerted and given sufficient time to indicate more time is required.',
					'description' => '',
				],
			];

			return $s508;
		}
		static function getWcagArticlesModel(int $level = 0, $guidelinesOnly = false){
			$wcag = array(
				array(
					"principle_heading" => "<a href='". EXTERNAL_QUICKREF_URL.'#principle1' ."' target='_blank' title='Principle 1' rel='external'>Principle 1 - Perceivable</a>",
					"principle_sub_heading" => "Information and user interface components must be presentable to users in ways they can perceive.",
					"principle_guidelines" => [
						"1.1" => [
							"heading"    => "Guideline 1.1 – Text Alternatives",
							"subheading" => "Provide text alternatives for any non-text content so that it can be changed into other forms people need, such as large print, braille, speech, symbols or simpler language.",
							"level" => [
								"A"  => [
									'1.1.1' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#text-equiv-all',
										'url'   => INTERNAL_QUICKREF_URL.'#1.1.1',
										'title' => "Non-text Content",
										"message" => 'All non-text content that is presented to the user has a text alternative that serves the equivalent purpose.',
										'tip' => 'Non-text files such as images or PDFs need to have plain-english alt text descriptions that describe the contents of the image.',
									],
								],
								"AA" => [

								]
							]
						],
						"1.2" => [
							"heading"    => "Guideline 1.2 – Time-based Media",
							"subheading" => "Provide alternatives for time-based media.",
							"level" => [
								"A"  => [
									'1.2.1' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#media-equiv-av-only-alt',
										'url'   => INTERNAL_QUICKREF_URL.'#1.2.1',
										'title' => "Audio-only and Video-only (Prerecorded)",
										"message" => 'For prerecorded audio-only and prerecorded video-only media, the following are true, except when the audio or video is a media alternative for text and is clearly labeled as such:
											<div class="bullet"><span>Prerecorded Audio-only:</span> An alternative for time-based media is provided that presents equivalent information for prerecorded audio-only content.</div>
											<div class="bullet"><span>Prerecorded Video-only:</span> Either an alternative for time-based media or an audio track is provided that presents equivalent information for prerecorded video-only content</div>',
										'tip' => 'If video or audio is presented on your site, you need to present a text alternative, such as a transcript.',
									],
									'1.2.2' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#media-equiv-captions',
										'url'   => INTERNAL_QUICKREF_URL.'#1.2.2',
										'title' => "Captions (Prerecorded)",
										"message" => 'Captions are provided for all prerecorded audio content in synchronized media, except when the media is a media alternative for text and is clearly labeled as such.',
										'tip' => 'Synchronized media, such as a video with audio (YouTube video is a good example) needs captions displayed as part of the media file.',
									],
									'1.2.3' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#media-equiv-audio-desc',
										'url'   => INTERNAL_QUICKREF_URL.'#1.2.3',
										'title' => "Audio Description or Media Alternative (Prerecorded)",
										"message" => 'An alternative for time-based media or audio description of the prerecorded video content is provided for synchronized media, except when the media is a media alternative for text and is clearly labeled as such.',
										'tip' => 'Synchronized media, such as a video with audio (YouTube video is a good example) needs captions displayed as part of the media file.',
									],

								],
								"AA" => [
									'1.2.4' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#media-equiv-real-time-captions',
										'url'   => INTERNAL_QUICKREF_URL.'#1.2.4',
										'title' => "Captions are provided for all live audio content in synchronized media.",
										"message" => 'An alternative for time-based media or audio description of the prerecorded video content is provided for synchronized media, except when the media is a media alternative for text and is clearly labeled as such.',
										'tip' => 'If you’re displaying live video or audio, you need captions enabled (likely, this will be something to confirm through your 3rd-party media provider, such as LiveStream or your Webinar software)',
									],
									'1.2.5' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#media-equiv-audio-desc-only',
										'url'   => INTERNAL_QUICKREF_URL.'#1.2.5',
										'title' => "Audio Description (Prerecorded)",
										"message" => 'Audio description is provided for all prerecorded video content in synchronized media.',
										'tip' => '',
									],
								]
							]
						],
						"1.3" => [
							"heading"    => "Guideline 1.3 – Adaptable",
							"subheading" => "Create content that can be presented in different ways (for example simpler layout) without losing information or structure.",
							"level" => [
								"A"  => [
									'1.3.1' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#content-structure-separation-programmatic',
										'url'   => INTERNAL_QUICKREF_URL.'#1.3.1',
										'title' => "Info and Relationships",
										"message" => 'Information, structure, and relationships conveyed through presentation can be programmatically determined or are available in text.',
										'tip' => '',
									],
									'1.3.2' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#content-structure-separation-sequence',
										'url'   => INTERNAL_QUICKREF_URL.'#1.3.2',
										'level' => "A",
										'title' => "Meaningful Sequence",
										"message" => 'When the sequence in which content is presented affects its meaning, a correct reading sequence can be programmatically determined.',
										'tip' => 'You’ll need proper use of heading tags (H1, H2, etc…) and a text description or alternative to direct people to the order that content should be consumed.',
									],
									'1.3.3' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#content-structure-separation-understanding',
										'url'   => INTERNAL_QUICKREF_URL.'#1.3.3',
										'level' => "A",
										'title' => "Sensory Characteristics",
										"message" => 'Instructions provided for understanding and operating content do not rely solely on sensory characteristics of components such as shape, size, visual location, orientation, or sound.',
										'tip' => 'You can’t have an instruction on the website that refers to a sensory characteristic as the only way to identify something. For example, the text “Click on the round button to submit your application” wouldn’t be compliant, because the instruction refers only to a sensory characteristic, in this case a ‘round’ button, which would be indistinguishable to a blind person.',
									],
								],
								"AA" => [

								]
							]
						],
						"1.4" => [
							"heading"    => "Guideline 1.4 – Distinguishable",
							"subheading" => "Make it easier for users to see and hear content including separating foreground from background.",
							"level" => [
								"A"  => [
									'1.4.1' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#visual-audio-contrast-without-color',
										'url'   => INTERNAL_QUICKREF_URL.'#1.4.1',
										'title' => "Use of Color",
										"message" => 'Color is not used as the only visual means of conveying information, indicating an action, prompting a response, or distinguishing a visual element.',
										'tip' => 'A reference to color can’t be the only way to distinguish something. For example, the text “Click on the green button to submit your application” wouldn’t be compliant, because the instruction refers only to the color green, which would be indistinguishable to a blind person.',
									],
									'1.4.2' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#visual-audio-contrast-dis-audio',
										'url'   => INTERNAL_QUICKREF_URL.'#1.4.2',
										'title' => "Audio Control",
										"message" => 'If any audio on a Web page plays automatically for more than 3 seconds, either a mechanism is available to pause or stop the audio, or a mechanism is available to control audio volume independently from the overall system volume level.',
										'tip' => 'As a general best practice outside of accessibility compliance, you shouldn’t have any audio auto-playing. It’s a conversion killer. But if you do, you need a mechanism to be able to pause or stop the audio, and it needs to be navigable via keyboard.',
									],

								],
								"AA" => [
									'1.4.3' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#visual-audio-contrast-contrast',
										'url'   => INTERNAL_QUICKREF_URL.'#1.4.3',
										'title' => "Contrast (Minimum)",
										"message" => 'The visual presentation of text and images of text has a contrast ratio of at least 4.5:1, except for the following:
											<div class="bullet"><span>Large Text:</span> Large-scale text and images of large-scale text have a contrast ratio of at least 3:1.</div>
											<div class="bullet"><span>Incidental:</span> Text or images of text that are part of an inactive user interface component, that are pure decoration, that are not visible to anyone, or that are part of a picture that contains significant other visual content, have no contrast requirement.</div>
											<div class="bullet"><span>Logotypes:</span> Text that is part of a logo or brand name has no minimum contrast requirement.</div>',
										'tip' => 'As a general best practice outside of accessibility compliance, you shouldn’t have any audio auto-playing. It’s a conversion killer. But if you do, you need a mechanism to be able to pause or stop the audio, and it needs to be navigable via keyboard.',
									],
									'1.4.4' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#visual-audio-contrast-scale',
										'url'   => INTERNAL_QUICKREF_URL.'#1.4.4',
										'title' => "Resize text",
										"message" => 'Except for captions and images of text, text can be resized without assistive technology up to 200 percent without loss of content or functionality.',
										'tip' => 'We interpret that you’ll meet accessibility compliance as long as your screen can be zoomed in up to 200% by using your keyboard (on PCs, it’s CTRL and +) without losing site function.',
									],
									'1.4.5' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#visual-audio-contrast-text-presentation',
										'url'   => INTERNAL_QUICKREF_URL.'#1.4.5',
										'title' => "Resize text",
										"message" => "If the technologies being used can achieve the visual presentation, text is used to convey information rather than images of text except for the following:
											<div class='bullet'><span>Customizable:</span> The image of text can be visually customized to the user's requirements.</div>
											<div class='bullet'><span>Essential:</span> A particular presentation of text is essential to the information being conveyed.</div>",
										'tip' => 'Don’t bake text into an image file, because blind people can’t read text that’s saved as part of an image file. Logos are exempt from this, but your logo image file should have clear alt text that describes any text that is part of the logo.',
									],
								]
							]
						],
					]
				),
				array(
					"principle_heading" => "<a href='". EXTERNAL_QUICKREF_URL.'#principle2' ."' target='_blank' title='Principle 2'>Principle 2 - Operable</a>",
					"principle_sub_heading" => "User interface components and navigation must be operable.",
					"principle_guidelines" => [
						"2.1" => [
							"heading"    => "Guideline 2.1 – Keyboard Accessible",
							"subheading" => "Provide text alternatives for any non-text content so that it can be changed into other forms people need, such as large print, braille, speech, symbols or simpler language.",
							"level" => [
								"A"  => [
									'2.1.1' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#keyboard-operation-keyboard-operable',
										'url'   => INTERNAL_QUICKREF_URL.'#2.1.1',
										'title' => "Keyboard",
										"message" => "All functionality of the content is operable through a keyboard interface without requiring specific timings for individual keystrokes, except where the underlying function requires input that depends on the path of the user's movement and not just the endpoints.",
										'tip' => 'Your site needs to be navigable and functional by keyboard alone, meaning the user needs to be able to “tab” through all size pages, menus, buttons and forms, etc… And clicking “enter” submits anything that requires submission.',
									],
									'2.1.2' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#keyboard-operation-trapping',
										'url'   => INTERNAL_QUICKREF_URL.'#2.1.2',
										'title' => "No Keyboard Trap",
										"message" => "If keyboard focus can be moved to a component of the page using a keyboard interface, then focus can be moved away from that component using only a keyboard interface, and, if it requires more than unmodified arrow or tab keys or other standard exit methods, the user is advised of the method for moving focus away.",
										'tip' => '',
									],
								],
								"AA" => [

								]
							]
						],
						"2.2" => [
							"heading"    => "Guideline 2.2 – Enough Time",
							"subheading" => "Provide users enough time to read and use content.",
							"level" => [
								"A"  => [
									'2.2.1' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#time-limits-required-behaviors',
										'url'   => INTERNAL_QUICKREF_URL.'#2.2.1',
										'title' => "Timing Adjustable",
										"message" => "For each time limit that is set by the content, at least one of the following is true:
											<div class='bullet'><span>Turn off:</span> The user is allowed to turn off the time limit before encountering it, or</div>
											<div class='bullet'><span>Adjust:</span> The user is allowed to adjust the time limit before encountering it over a wide range that is at least ten times the length of the default setting, or</div>
											<div class='bullet'><span>Extend:</span> The user is warned before time expires and given at least 20 seconds to extend the time limit with a simple action (for example, 'press the space bar'), and the user is allowed to extend the time limit at least ten times, or</div>
											<div class='bullet'><span>Real-time Exception:</span> The time limit is a required part of a real-time event (for example, an auction), and no alternative to the time limit is possible, or</div>
											<div class='bullet'><span>Essential Exception:</span> The time limit is essential and extending it would invalidate the activity, or</div>
											<div class='bullet'><span>20 Hour Exception:</span> The time limit is longer than 20 hours.</div>",
										'tip' => '',
									],
									'2.2.2' =>[
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#time-limits-pause',
										'url'   => INTERNAL_QUICKREF_URL.'#2.2.2',
										'title' => "Pause, Stop, Hide",
										"message" => "For moving, blinking, scrolling, or auto-updating information, all of the following are true:
											<div class='bullet'><span>Moving, blinking, scrolling:</span> For any moving, blinking or scrolling information that (1) starts automatically, (2) lasts more than five seconds, and (3) is presented in parallel with other content, there is a mechanism for the user to pause, stop, or hide it unless the movement, blinking, or scrolling is part of an activity where it is essential, and</div>
											<div class='bullet'><span>Auto-updating:</span> For any auto-updating information that (1) starts automatically and (2) is presented in parallel with other content, there is a mechanism for the user to pause, stop, or hide it or to control the frequency of the update unless the auto-updating is part of an activity where it is essential.</div>",
										'tip' => '',
									],
								],
								"AA" => [

								]
							]
						],
						"2.3" => [
							"heading"    => "Guideline 2.3 – Seizures",
							"subheading" => "Do not design content in a way that is known to cause seizures.",
							"level" => [
								"A"  => [
									'2.3.1' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#seizure-does-not-violate',
										'url'   => INTERNAL_QUICKREF_URL.'#2.3.1',
										'title' => "Three Flashes or Below Threshold",
										"message" => "Web pages do not contain anything that flashes more than three times in any one second period, or the flash is below the general flash and red flash thresholds.",
										'tip' => '',
									],
								],
								"AA" => [

								]
							]
						],
						"2.4" => [
							"heading"    => "Guideline 2.4 – Navigable",
							"subheading" => "Provide ways to help users navigate, find content, and determine where they are.",
							"level" => [
								"A"  => [
									'2.4.1' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#navigation-mechanisms-skip',
										'url'   => INTERNAL_QUICKREF_URL.'#2.4.1',
										'title' => "Bypass Blocks",
										"message" => "A mechanism is available to bypass blocks of content that are repeated on multiple Web pages.",
										'tip' => 'Example: An online newspaper’s homepage might have a main story, 15 other stories, and 10 advertisements, which might total dozens of links and buttons, which would otherwise take a screen reader or keyboard a very long time to “tab” through and audibly read. You’ll need a link at the top of the page that says “jump to main story”, thus saving a blind person a lot of wasted time.',
									],
									'2.4.2' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#navigation-mechanisms-title',
										'url'   => INTERNAL_QUICKREF_URL.'#2.4.2',
										'title' => "Page Titled",
										"message" => "Web pages have titles that describe topic or purpose.",
										'tip' => '',
									],
									'2.4.3' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#navigation-mechanisms-focus-order',
										'url'   => INTERNAL_QUICKREF_URL.'#2.4.3',
										'title' => "Focus Order",
										"message" => "If a Web page can be navigated sequentially and the navigation sequences affect meaning or operation, focusable components receive focus in an order that preserves meaning and operability.",
										'tip' => 'The order in which someone navigates from action/link to action/link needs to be in a logical order. In other words, “tabbing” through a webpage can’t result in nonsensical jumping around. Things must be in logical order.',
									],
									'2.4.4' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#navigation-mechanisms-refs',
										'url'   => INTERNAL_QUICKREF_URL.'#2.4.4',
										'title' => "Link Purpose (In Context)",
										"message" => "The purpose of each link can be determined from the link text alone or from the link text together with its programmatically determined link context, except where the purpose of the link would be ambiguous to users in general.",
										'tip' => 'A link shouldn’t just say “click here”, or “link”. The text of the link should be descriptive to the contents of the link. Example: “There was much bloodshed during the Medieval period of history”, and “Medieval Period” is the link.',
									],
								],
								"AA" => [
									'2.4.5' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#navigation-mechanisms-mult-loc',
										'url'   => INTERNAL_QUICKREF_URL.'#2.4.5',
										'title' => "Multiple Ways",
										"message" => "More than one way is available to locate a Web page within a set of Web pages except where the Web Page is the result of, or a step in, a process.",
										'tip' => 'You need to have more than one way to access a webpage. For example, the website’s main navigation menu represents one way to access a page. A second way to access a page would be having inter-linking between pages, or a secondary menu (like a sidebar menu, for example).',
									],
									'2.4.6' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#navigation-mechanisms-descriptive',
										'url'   => INTERNAL_QUICKREF_URL.'#2.4.6',
										'title' => "Headings and Labels",
										"message" => "Headings and labels describe topic or purpose.",
										'tip' => 'This task permeates much of the site. Pages need to have proper headings to identify site content, and in the right order (H2 tags shouldn’t be in front of H1 tags, etc…). Also, embedded tables, objects, iFrames, YouTube videos, etc… all need to have proper headings. Assume that these heading tags won’t be provided by the embed code. You’ll need add them manually via HTML manipulation.',
									],
									'2.4.7' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#navigation-mechanisms-focus-visible',
										'url'   => INTERNAL_QUICKREF_URL.'#2.4.7',
										'title' => "Focus Visible",
										"message" => "Any keyboard operable user interface has a mode of operation where the keyboard focus indicator is visible.",
										'tip' => 'As someone “tabs” through the website, there needs to be a visible cue as to where the cursor is at, and it needs to meet the 4.5:1 color contrast ratio. An example would be underlining words/links, or illuminating buttons.',
									],
								]
							]
						],
					]
				),
				array(
					"principle_heading" => "<a href='". EXTERNAL_QUICKREF_URL.'#principle3' ."' target='_blank' title='Principle 3'>Principle 3 - Understandable</a>",
					"principle_sub_heading" => "Information and the operation of user interface must be understandable.",
					"principle_guidelines" => [
						"3.1" => [
							"heading"    => "Guideline 3.1 – Readable",
							"subheading" => "Make text content readable and understandable.",
							"level" => [
								"A"  => [
									'3.1.1' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#meaning-doc-lang-id',
										'url'   => INTERNAL_QUICKREF_URL.'#3.1.1',
										'title' => "Language of Page",
										"message" => "The default human language of each Web page can be programmatically determined.",
										'tip' => 'A lang attribute on the HTML element needs to be present.',
									],
								],
								"AA" => [
									'3.1.2' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#meaning-other-lang-id',
										'url'   => INTERNAL_QUICKREF_URL.'#3.1.2',
										'title' => "Language of Parts",
										"message" => "The human language of each passage or phrase in the content can be programmatically determined except for proper names, technical terms, words of indeterminate language, and words or phrases that have become part of the vernacular of the immediately surrounding text.",
										'tip' => '',
									],
								]
							]
						],
						"3.2" => [
							"heading"    => "Guideline 3.2 – Predictable",
							"subheading" => "Make Web pages appear and operate in predictable ways.",
							"level" => [
								"A"  => [
									'3.2.1' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#consistent-behavior-receive-focus',
										'url'   => INTERNAL_QUICKREF_URL.'#3.2.1',
										'title' => "On Focus",
										"message" => "When any component receives focus, it does not initiate a change of context.",
										'tip' => '',
									],
									'3.2.2' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#consistent-behavior-unpredictable-change',
										'url'   => INTERNAL_QUICKREF_URL.'#3.2.2',
										'title' => "On Input",
										"message" => "Changing the setting of any user interface component does not automatically cause a change of context unless the user has been advised of the behavior before using the component.",
										'tip' => '',
									],
								],
								"AA" => [
									'3.2.3' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#consistent-behavior-consistent-locations',
										'url'   => INTERNAL_QUICKREF_URL.'#3.2.3',
										'title' => "Consistent Navigation",
										"message" => "Navigational mechanisms that are repeated on multiple Web pages within a set of Web pages occur in the same relative order each time they are repeated, unless a change is initiated by the user.",
										'tip' => '',
									],
									'3.2.4' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#consistent-behavior-consistent-functionality',
										'url'   => INTERNAL_QUICKREF_URL.'#3.2.4',
										'title' => "Consistent Identification",
										"message" => "Components that have the same functionality within a set of Web pages are identified consistently.",
										'tip' => '',
									],
								]
							]
						],
						"3.3" => [
							"heading"    => "Guideline 3.3 – Input Assistance",
							"subheading" => "Help users avoid and correct mistakes.",
							"level" => [
								"A"  => [
									'3.3.1' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#minimize-error-identified',
										'url'   => INTERNAL_QUICKREF_URL.'#3.3.1',
										'title' => "Error Identification",
										"message" => "If an input error is automatically detected, the item that is in error is identified and the error is described to the user in text.",
										'tip' => 'This is common in forms. If someone enters incorrect data or skips a required field, a plain-text message or alert appears, letting the user know what to fix.',
									],
									'3.3.2' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#minimize-error-cues',
										'url'   => INTERNAL_QUICKREF_URL.'#3.3.2',
										'title' => "Labels or Instructions",
										"message" => "Labels or instructions are provided when content requires user input.",
										'tip' => 'Form fields need titles that describe what’s required of them.',
									],
								],
								"AA" => [
									'3.3.3' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#minimize-error-suggestions',
										'url'   => INTERNAL_QUICKREF_URL.'#3.3.3',
										'title' => "Error Suggestion",
										"message" => "If an input error is automatically detected and suggestions for correction are known, then the suggestions are provided to the user, unless it would jeopardize the security or purpose of the content.",
										'tip' => '',
									],
									'3.3.4' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#minimize-error-reversible',
										'url'   => INTERNAL_QUICKREF_URL.'#3.3.4',
										'title' => "Error Prevention (Legal, Financial, Data)",
										"message" => "For Web pages that cause legal commitments or financial transactions for the user to occur, that modify or delete user-controllable data in data storage systems, or that submit user test responses, at least one of the following is true:
											<div class='bullet'><span>Reversible:</span> Submissions are reversible.</div>
											<div class='bullet'><span>Checked:</span> Data entered by the user is checked for input errors and the user is provided an opportunity to correct them.</div>
											<div class='bullet'><span>Confirmed:</span> A mechanism is available for reviewing, confirming, and correcting information before finalizing the submission.</div>",
										'tip' => '',
									],
								]
							]
						],
					]
				),
				array(
					"principle_heading" => "<a href='". EXTERNAL_QUICKREF_URL.'#principle4' ."' target='_blank' title='Principle 4'>Principle 4 - Robust</a>",
					"principle_sub_heading" => "Content must be robust enough that it can be interpreted reliably by a wide variety of user agents, including assistive technologies.",
					"principle_guidelines" => [
						"4.1" => [
							"heading"    => "Guideline 4.1 – Compatible",
							"subheading" => "Maximize compatibility with current and future user agents, including assistive technologies.",
							"level" => [
								"A"  => [
									'4.1.1' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#ensure-compat-parses',
										'url'   => INTERNAL_QUICKREF_URL.'#4.1.1',
										'title' => "Parsing",
										"message" => "In content implemented using markup languages, elements have complete start and end tags, elements are nested according to their specifications, elements do not contain duplicate attributes, and any IDs are unique, except where the specifications allow these features.",
										'tip' => '',
									],
									'4.1.2' => [
										'ext-url'   => EXTERNAL_QUICKREF_URL.'#ensure-compat-rsv',
										'url'   => INTERNAL_QUICKREF_URL.'#4.1.2',
										'level' => "A",
										'title' => "Name, Role, Value",
										"message" => "For all user interface components (including but not limited to: form elements, links and components generated by scripts), the name and role can be programmatically determined; states, properties, and values that can be set by the user can be programmatically set; and notification of changes to these items is available to user agents, including assistive technologies.",
										'tip' => '',
									],
								],
								"AA" => [

								]
							]
						],

					]
				),
			);

			switch($level){
				case 0:
					//Start with Principles
					return $wcag;
					break;
				case 1:
					//Start with Guidelines
					$articles = [];
					foreach($wcag AS $article){
						foreach($article["principle_guidelines"] AS $key => $guideline){
							if($guidelinesOnly){ unset($guideline["level"]); }
							$articles[$key] = $guideline;
						}
					}
					return $articles;
					break;
				case 2:
					foreach($wcag AS $article){
						foreach($article["principle_guidelines"] AS $guideline){
							foreach($guideline["level"] AS $v){
								foreach($v AS $index => $item){
									$articles[$index] = $item;
								}
							}
						}
					}
					return $articles;
					break;
			}
		}
	}

}
