<?php

add_action("wp_ajax_nopriv_decode_base64", "decode_base64");
add_action("wp_ajax_decode_base64", "decode_base64");
function decode_base64(){
	$upload_path = wp_upload_dir()["basedir"];
	$cbc_path = "/cbc-temp/";
	
	if(!file_exists($upload_path.$cbc_path)){
		mkdir( $upload_path . $cbc_path, 0777, true );
	}
	//$filename = uniqid().".jpg";
	$filename = "test.pdf";
	$path = $upload_path.$cbc_path.$filename;
	$string = $_POST["string"];
	$writer = fopen($path, "wb");
	fwrite($writer, base64_decode($string));
	fclose($writer);
	$path = wp_upload_dir()["baseurl"].$cbc_path.$filename;
	echo $path;
	wp_die();
}