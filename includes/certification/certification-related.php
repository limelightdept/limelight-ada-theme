<?php
use Carbon\Carbon;
use Limelight_Child_Helpers as LCH;

/*
* Register the Certification Post type and add relevant hooks
*/

function register_certification_post_type() {

	/**
	 * Post Type: Certifications.
	 */

	$labels = \Limelight\Helpers\Help::generate_post_labels("Certification", "Certifications", "limelight-theme");

	$args = array(
		"label" => __( "Certifications", "limelight-theme" ),
		"labels" => $labels,
		"description" => "For storing data about certifications sold through the WCAG 2.0 Web Accessibility certification program. Stores Details",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "ada-certification",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "certification", "with_front" => false ),
		"query_var" => true,
		"menu_position" => 4,
		"menu_icon" => "dashicons-awards",
		"supports" => ["author"],
	);

	register_post_type( "ada_certification", $args );
}
add_action( 'init', 'register_certification_post_type' );

function certification_settings_page(){
    $prefix = "_ada_cert_";
    $cmb_certification_options = new_cmb2_box( array(
 		'id'           => 'certification_settings_metabox',
 		'title'        => esc_html__( 'Certification Settings', LIMELIGHT_THEME_TEXT_DOMAIN ),
 		'object_types' => array( 'options-page' ),
        "context"      => "normal",
        "priority"     => "high",
        "show_names"   => true,
 		'option_key'   => 'cert_settings', // The option key and admin menu page slug.
 		'menu_title'   => "Settings", // Falls back to 'title' (above).
 		'parent_slug'  => 'edit.php?post_type=ada_certification', // Make options page a submenu item of the themes menu.
 		'capability'   => 'manage_options', // Cap required to view options-page.
 	) );

    $cmb_certification_options->add_field(
      array(
     		'name' => "Available Template Tags:",
     		'desc' => "Tags available to use in the email templates:<br>
         - [[username]]:        Mapped to the certification author's display_name<br>
         - [[cert_page_link]]:  Displays an anchor tag to the certifications guid<br>
         - [[email]]:           The certification author's email<br>
         - [[badge]]:           Html for the badge link<br>
         - [[badge_url]]:       The link to the badge page without the HTML",
     		'id'   => $prefix.'template_tags',
     		'type' => 'text',
         'attributes' => ["class" => "hidden"],
     	)
   );
   $cmb_certification_options->add_field(
      array(
     		'name' => "'Pending' Email Template:",
     		'desc' => "Template used for emails sent to customers when the certificate status gets updated to 'Pending'",
     		'id'   => $prefix.'email_pending',
     		'type' => 'textarea',
     	)
   );
   $cmb_certification_options->add_field(
      array(
     		'name' => "'In Review' Email Template:",
     		'desc' => "Template used for emails sent to customers when the certificate status gets updated to 'In Review'",
     		'id'   => $prefix.'email_in_review',
     		'type' => 'textarea',
     	)
   );
   $cmb_certification_options->add_field(
      array(
     		'name' => "'Not Certified' Email Template:",
     		'desc' => "Template used for emails sent to customers when the certificate status gets updated to 'Not Certified'",
     		'id'   => $prefix.'email_not_certified',
     		'type' => 'textarea',
     	)
   );
   $cmb_certification_options->add_field(
      array(
     		'name' => "'Certified' Email Template:",
     		'desc' => "Template used for emails sent to customers when the certificate status gets updated to 'Certified'",
     		'id'   => $prefix.'email_certified',
     		'type' => 'textarea',
     	)
   );

   $cmb_certification = new_cmb2_box( array(
 		'id'           => 'certification_metabox',
 		'title'        => "Certification Data",
 		'object_types' => array( 'ada_certification' ),
        "context"      => "normal",
        "priority"     => "high",
        "show_names"   => true,

 	) );
    $cmb_certification->add_field(
        array(
     		'name'       => "Certification Status:",
     		'desc'       => "Sets the status of the certification",
     		'id'         => $prefix."status",
     		'type'       => 'radio_inline',
            'options' => array(
        		'pending'       => __( 'Pending', LIMELIGHT_THEME_TEXT_DOMAIN ),
        		'in_review'     => __( 'In Review', LIMELIGHT_THEME_TEXT_DOMAIN ),
        		'certified'     => __( 'Certified', LIMELIGHT_THEME_TEXT_DOMAIN ),
        		'not_certified' => __( 'Not Certified', LIMELIGHT_THEME_TEXT_DOMAIN ),
        	),
     	)
    );
    $cmb_certification->add_field(
        array(
     		'name'       => "Registered Domain:",
     		'desc'       => "The domain linked to this certification.",
     		'id'         => $prefix."domain",
     		'type'       => 'text',
     	)
    );
    $cmb_certification->add_field(
        array(
     		'name'       => "Reviews Remaining:",
     		'desc'       => "The number of reviews remaining on this certificate",
     		'id'         => $prefix."reviews_remaining",
     		'type'       => 'text_small',
         'attributes' => [
            "id"   => "reviews",
         ]
     	)
    );
    $cmb_certification->add_field(
        array(
     		'name'       => "Badge HTML:",
     		'desc'       => "The html of the public badge",
     		'id'         => $prefix."badge",
     		'type'       => 'textarea',
         'attributes' => [
            "readOnly"   => "true",
         ]
     	)
    );
    $cmb_certification->add_field(
        array(
     		'name'       => "Order ID:",
     		'desc'       => "The Limelight Payments order ID",
     		'id'         => $prefix."order_id",
     		'type'       => 'text_small',
         'attributes' => [
            "readOnly"   => "true",
         ]
     	)
    );
    $cmb_certification->add_field(
        array(
     		'name'       => "Path to SVG:",
     		'desc'       => "The relative path to the badge SVG",
     		'id'         => $prefix."badge_upload_path",
     		'type'       => 'textarea',
         'attributes' => [
            "readOnly"   => "true",
            "cols"       => "10",
            "rows"       => "3",
         ]
     	)
    );
    $cmb_certification->add_field(
        array(
     		'name'       => "Badge Page URL:",
     		'desc'       => "URL to the public facing badge page",
     		'id'         => $prefix."badge_url",
     		'type'       => 'textarea',
			'attributes' => [
            "readOnly"   => "true",
            "cols"       => "10",
            "rows"       => "3",
         ]
     	)
    );

   $cmb_roles = new_cmb2_box( array(
		'id'           => 'membership_access_settings',
		'title'        => esc_html__( 'Membership Role Access', LIMELIGHT_THEME_TEXT_DOMAIN ),
		'object_types' => array( 'page' ),
		"context"      => "side",
		"show_names"   => false,

 	) );

   $cmb_roles->add_field( array(
   	'name'    => 'Role Multi Checkbox',
   	'desc'    => 'A checked role will see this page in their member navigation menu and have access to the page',
   	'id'      => 'role_multicheckbox',
   	'type'    => 'multicheck',
   	'options' => array(
   		'certification_user'    	 => 'Certification Users',
   		'compliance_audit_user' 	 => 'Compliance Audit Users',
		'free_compliance_audit_user' => 'Free Compliance Audit Users',
   		'agency_user'  	 => 'Agency Users',
		'administrator'		 => 'Administrator Only'
   	),
   ) );
}
add_action("cmb2_admin_init", "certification_settings_page");

/*function create_certification_client_email($message, $result){
    ob_start();
    ?>
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="UTF-8">

      </head>
      <body style="color:black;font-size:16px;">
          <h1>Thank you for purchasing the Online ADA Certification!</h1>
          <br />
          <div>Here are the details of your order:</div>
          <br />
          <div>Product: <?php echo $result['Data'][0]['name'] ?> </div>
          <div>Price: $<?php echo $result['Data'][0]['price'] ?> </div>

          <?php if(isset($result['Data'][1])){ ?>
              <div>Discounts: </div>
              <div>-- code: <?php echo trim(explode(':', $result['Data'][1]['name'])[1]) ?> </div>
              <div>-- amount: <?php echo $result['Data'][1]['price'] ?>.00 </div>
              <?php $total = $result['Data'][0]['price'] - $result['Data'][1]['price']; ?>
          <?php }else{ ?>
              <div>Discounts: $0.00</div>
              <?php $total = $result['Data'][0]['price']; ?>
          <?php } ?>

          <div>Tax: $0.00</div>
          <div>Your total: $<?php echo $total ?> </div>

      </body>
    </html>
    <?php
    $message = ob_get_clean();

    return $message;
}*/
//add_filter("ll-payments-success-message", "create_certification_client_email", 2, 10);

//Create my own column headers
function certification_columns( $columns ) {
   return LCH::$custom_cert_columns;
}
add_filter('manage_edit-ada_certification_columns', 'certification_columns');

//Sets the row hover actions (Edit, Quick Edit, Trash, etc.) to a custom column
function list_table_primary_column( $default, $screen ) {
    if ( 'edit-ada_certification' === $screen ) {
        $default = 'id';
    }
    return $default;
}
add_filter( 'list_table_primary_column', 'list_table_primary_column', 10, 2 );

//Make my custom columns sortable
function certification_sortable_columns( $columns ) {
    return LCH::$cert_columns_sort_map;
}
add_filter('manage_edit-ada_certification_sortable_columns', 'certification_sortable_columns');

//Define rules for how the custom columns will sort
function certification_column_sort( $request ) {
   if( array_key_exists('orderby', $request )) {
      $meta_prefix = "_ada_cert_";
      foreach(LCH::$cert_columns_sort_map as $k => $v){
         if($v === $request['orderby']){
            switch($v){
               case "ID":
                  $request['orderby'] = $v;
                  break;
               default:
                  $request['orderby']  = "meta_value";
                  $request['meta_key'] = $meta_prefix.$k;
                  break;
            }
         }
      }
   }
   return $request;
}
add_filter('request', 'certification_column_sort');

//Output the data into the column
function certification_column( $colname, $cptid ) {
   $meta_prefix = "_ada_cert_";
	$meta_key = $meta_prefix;

   switch($colname){
      case "id":
         $link = "<a href='" . get_post_permalink($cptid) . "' target='_blank'>" . $cptid . "</a>";
         echo $link;
         break;
      case "user":
         $user_id = get_post((int)$cptid)->post_author;
         $email = get_user_by("ID", $user_id)->user_email;
         $link = "<a href='" . get_edit_user_link($user_id) . "' target='_blank'>" . $email . "</a>";
         echo $link;
         break;
      case "order_id":
         $meta_key .= $colname;
			$order_id = (int)get_post_meta($cptid, $meta_key, true);

			if($order_id){
				$order = get_post($order_id);
				$order_num = explode(" ", $order->post_title)[0];
				echo "<a href='" . get_edit_post_link($order_id) . "' target='_blank'>" . $order_num . "</a>";
			}
         break;
      case "badge":
			$badge = get_post_meta( $cptid, $meta_key."badge", true );

         if(!empty($badge)){
            $url = get_post_meta( $cptid, $meta_key."badge_url", true );
				$url = "<a target='_blank' href='" . $url . "'><i class='fa fa-certificate' aria-hidden='true'></i></a>";
				echo $url;
         }
         break;
      default:
         $meta_key .= $colname;
         echo get_post_meta( $cptid, $meta_key, true );
   }

}
add_action('manage_ada_certification_posts_custom_column', 'certification_column', 10, 2);

/*
* End Certification Post type section
*/
function on_certification_save( $ID, $post ) {

	if($post->post_type === "ada_certification"){
		if($post->post_status === "auto-draft"){return;}

		$post->post_title = $domain . " Certification Status";
		$status      = get_post_meta($ID, "_ada_cert_status", true);
		$badge       = "";
		$badge_url   = "";
		$user        = get_user_by("ID", $post->post_author);
		$domain      = get_post_meta($ID, "_ada_cert_domain", true);
		if(strpos($domain, 'https://') !== FALSE){ $domain = explode('https://', $domain)[1]; }
		if(strpos($domain, 'http://') !== FALSE){ $domain = explode('http://', $domain)[1]; }
		$db_rel_path = "";
		$upload_dir  = wp_upload_dir()["basedir"]. "/badge/$user->ID/$domain/";
		$date_certified = "";
		$filename = "badge.svg";
		$fullpath = $upload_dir.$filename;

		if($post->post_name != $ID){
			$result = wp_update_post(["ID" => $ID, "post_name" => $ID]);
		}

		if($status === "certified"){
			$db_rel_path = "/uploads/badge/$user->ID/$domain/badge.svg";
			$badge_svg_url = get_stylesheet_directory()."/assets/img/ada-certification-badge.svg";
			$date_certified = Carbon::now('UTC')->format("n-j-Y");

			ob_start();
			echo file_get_contents($badge_svg_url);
			$new_file = ob_get_clean();
			//Replace the certified date with today's date
			$new_file = preg_replace('/Certified: 1\/1\/2018/', 'Certified: '. $date_certified, $new_file, -1);

			if(!is_dir($upload_dir)){
				mkdir($upload_dir, 0777, true);
			}

			if(!file_exists($fullpath)){
				file_put_contents($fullpath, $new_file);
			}

			$badge_url = site_url("certified/".$domain."/".$ID);
			$badge = "<a href='$badge_url' target='_blank' ><img alt='ADA Certification Badge' style='width:200px;height:auto;' src='".content_url($db_rel_path)."' /></a>";
		}else{
			if(file_exists($fullpath) AND is_file($fullpath)){
				unlink($fullpath);
			}
		}

		update_post_meta($ID, "_ada_cert_badge", trim($badge));
		update_post_meta($ID, "_ada_cert_badge_url", $badge_url);
		update_post_meta($ID, "_ada_cert_badge_upload_path", $db_rel_path);
		update_post_meta($ID, "_ada_cert_date_certified", $date_certified);
		update_post_meta($ID, "_ada_cert_reviews_remaining", 2);

		$vars = [
			"username"        => $user->display_name,
			"email"           => $user->user_email,
			"cert_page_link"  => "<a href='" . $post->guid . "'>View certification page</a>",
			"badge"           => $badge,
			"badge_url"       => $badge_url,
			"badge_escaped"   => esc_html($badge),
		];

		//$html = Limelight\Limelight_Helpers::get_option("cert_settings", "_ada_cert_email_".$status);
		//ob_start();
		//echo $html;
		//$message = Limelight_Child_Helpers::filter_vars(ob_get_clean(), $vars);

		//wp_mail($user->user_email, "Your ADA Accessibility certificate status has changed", $message);
	}
}
add_action( 'save_post', 'on_certification_save', 99, 3 );

function delete_badge($id){
	global $post_type;
    if ( $post_type != 'ada_certification' ) return;
    $domain  = get_post_meta($id, "_ada_cert_domain", true);
    $user_id = get_post($id)->post_author;
    $path    = wp_upload_dir()["basedir"]."/badges/$user_id/$domain/";
    $svg     = wp_upload_dir()["basedir"]."/badges/$user_id/$domain/badge.svg";
    if(file_exists($svg) AND is_file($svg)){ unlink($svg); }
    if(is_dir($path)){ rmdir($path); }
}
add_action('before_delete_post', 'delete_badge');

add_action('init', 'limelight_certification_rewrite');

function limelight_certification_rewrite($rules){
	add_rewrite_rule('^certification/([^/]*)/?','index.php?p=$matches[1]&post_type=ada_certification&ada_certification=$matches[1]','top');
	add_rewrite_rule('^certified/([^/]*)/([^/]*)?','index.php?pagename=certified&badge=$matches[1]&post_id=$matches[2]','top');

	add_rewrite_tag('%ada_certification%', '([^&]+)');
	add_rewrite_tag('%badge%', '([^&]+)');
}
