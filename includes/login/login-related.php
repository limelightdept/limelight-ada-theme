<?php

use Limelight_Child_Helpers AS LCH;

function oada_after_login($response, $form, $event, $model){
	if($event->getName() == "wp_login" && $form->getID() == 3150 && $response["status"] == "OK"){
		$destination = urldecode($_SERVER["HTTP_REFERER"]);
		$response["redirect_to"] = "";
		$user = wp_get_current_user();
		if(stripos($destination, "redirect_to")){			
			$response["redirect_to"] = explode(home_url("/login/?"), $destination)[1];
			$response["redirect_to"] = explode("redirect_to=/", $response["redirect_to"])[1];
		}
		
		if($response["redirect_to"] !== site_url("wp-admin/") && !empty($response["redirect_to"])){
			$response["redirect_to"] = home_url($response["redirect_to"]);
			return $response;
		}
		
		if( in_array("free_compliance_audit_user", $user->roles) ){
			$domains = LCH::get_domains($user->ID, true);
			
			$response["redirect_to"] = home_url("agency/reports/{$user->ID}/{$domains[0]['ID']}");
			return $response;
		}else{
			$response["redirect_to"] = home_url("agency");
			return $response;
		}
		
	}

	return $response;
	
}
add_filter("Limelight\Forms\EventResponse", "oada_after_login", 10, 4);


add_action("init", "login_page_redirect");
function login_page_redirect(){
    $page = basename($_SERVER['REQUEST_URI']);
	
    if( $page === "wp-login.php" && $_SERVER['REQUEST_METHOD'] === 'GET') {
        wp_redirect(home_url("/login"));
        exit;
    }
}

add_action('wp_logout','auto_redirect_after_logout');
function auto_redirect_after_logout(){
	if(isset($_GET["action"]) && $_GET["action"] == "rp"){ return; }
    wp_redirect( home_url() );
    exit();
}

