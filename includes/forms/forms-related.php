<?php

namespace Limelight\Plugin\Forms\Events;

use Limelight\Forms\Package;

use Limelight\Forms\Events\Event;

use Limelight_Child_Helpers AS LCH;

class RegisterDomains extends Event{

   /**
    * Construct coupon
    */
    public function __construct($args = []){

        $this->name = "register_domains";

        $this->label = "Register Domains";

        parent::__construct($args);

    }

    public function fireEvent($model){

        //$code = Package::instance()->map($this->coupon, $model);\
        LCH::save_domains($model['domains']);

        $response = [
            "status" => "OK",
            "message" => "Chyea",
            "domains" => $model['domains']
        ];

        return $response;
    }

}