<?php
use Carbon\Carbon;
function after_payment($form_id, $event, $response, $model){
	
	if( $form_id === 1766 ){
		
		//Record purchase data and create a new Certification
		$meta_prefix = "_ada_cert_";
		$certification_metadata = [
			$meta_prefix."purchase_date"     => Carbon::now("UTC")->toDateString(),
			$meta_prefix."business_name"     => $model["business_name"],
			$meta_prefix."reviews_remaining" => "2",
			$meta_prefix."status"            => "pending",
			$meta_prefix."order_id"          => $response["event_data"]["order_id"],
			$meta_prefix."domain"            => $model["web_domain"],
			$meta_prefix."protocol"          => $model["protocol"],
		];
		$certification_data = [
			"post_type"    => "ada_certification",
			"post_title"   => $model["web_domain"] ." Certification",
			"post_status"  => "publish",
			"author_id"    => get_current_user_id(),
			"meta_input"   => $certification_metadata,
		];

		$result = wp_insert_post($certification_data);
	}
	
	if( $form_id === 2936 && $response["status"] == "OK"){
		//Create a user - user gets created from limelight forms
		//Create a domain - don't actually need one, as there isn't a need

		$user = wp_get_current_user();
		$result = update_user_meta($user->ID, "agency_name", $model["agency_name"]);
		$user->add_role("agency_user");
	}
}
add_action("Limelight\Forms\EventFired\checkout", "after_payment", 10, 4);