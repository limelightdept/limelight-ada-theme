<?php

require_once get_stylesheet_directory() . '/includes/fpdf/fpdf.php';
use Carbon\Carbon;
use League\Csv\Writer;
use Limelight_Child_Helpers AS LCH;
use iio\libmergepdf\Merger;
use Limelight\Helpers\Active;

// function register_compliance_audit_post_type() {

// 	/**
// 	 * Post Type: Site Audit.
// 	 */

// 	$labels = \Limelight\Helpers\Help::generate_post_labels("Audit", "Audits", "limelight-theme");

// 	$args = array(
// 		"label" => __( "Audits", "limelight-theme" ),
// 		"labels" => $labels,
// 		"description" => "For storing data about compliance audits sold through the WCAG 2.0 Web Accessibility certification program.",
// 		"public" => true,
// 		"publicly_queryable" => true,
// 		"show_ui" => true,
// 		"show_in_rest" => true,
// 		"rest_base" => "compliance-audit",
// 		"has_archive" => false,
// 		"show_in_menu" => true,
// 		"exclude_from_search" => true,
// 		"capability_type" => "post",
// 		"map_meta_cap" => true,
// 		"hierarchical" => true,
// 		"rewrite" => array( "slug" => "audit", "with_front" => false ),
// 		"query_var" => true,
// 		"menu_position" => 4,
// 		"menu_icon" => "dashicons-visibility",
// 		"supports" => ["author"],
// 	);

// 	register_post_type( "compliance_audit", $args );
// }
// add_action( 'init', 'register_compliance_audit_post_type' );

$sub_pages = array(
    'new-scan' 		=> 'New Scan',
    'sitemap' 		=> 'Sitemap',
    'scan-history' 	=> 'Scan History'
);

add_action('init', 'limelight_audit_rewrite');

function limelight_audit_rewrite($rules){
	add_rewrite_rule('^agency/audit/([^/]*)/([^/]*)/?','index.php?pagename=agency%2F$matches[2]&domain_id=$matches[1]','top');
	add_rewrite_rule('^agency/reports/([^/]*)/([^/]*)?','index.php?pagename=agency%2Freports&domain_id=$matches[2]&user_id=$matches[1]','top');
	add_rewrite_rule('^agency/sitemap/([^/]*)/review?','index.php?pagename=agency%2Fsitemap%2Freview&domain_id=$matches[1]','top');
	add_rewrite_rule('^agency/new-scan/([^/]*)/options?','index.php?pagename=agency%2Fnew-scan%2Foptions&domain_id=$matches[1]','top');
	add_rewrite_rule('^agency/new-scan/([^/]*)/scan-summary?','index.php?pagename=agency%2Fnew-scan%2Fscan-summary&domain_id=$matches[1]','top');

	add_rewrite_tag('%domain_id%', '([^&]+)');
	add_rewrite_tag('%user_id%', '([^&]+)');
}

add_action("wp_ajax_save_snapshot", 'save_snapshot');
add_action("wp_ajax_nopriv_save_snapshot", 'save_snapshot');
function save_snapshot()
{
	if (!isset($_POST["b64"]) || !isset($_POST["user_id"]) || !isset($_POST["domain_id"])) return false;
	$image_b64 = base64_decode($_POST["b64"]);
	$file = wp_upload_dir()["basedir"] . '/users/' . $_POST["user_id"] . '/' . $_POST["domain_id"] . "/snapshot.png";
	$result = file_put_contents($file, $image_b64);

	echo wp_upload_dir()["baseurl"] . '/users/' . $_POST["user_id"] . '/' . $_POST["domain_id"] . "/snapshot.png";

	wp_die();
}

add_action("wp_ajax_complete_audit", 'complete_audit');
add_action("wp_ajax_nopriv_complete_audit", 'complete_audit');
function complete_audit()
{
	if (!isset($_GET["domain_id"])) return false;
	
	global $wpdb;
	$time = time();
	$wpdb->query("UPDATE {$wpdb->prefix}domains SET last_scanned = {$time} WHERE ID = {$_GET['domain_id']}");

	wp_die();
}

add_action("wp_ajax_nopriv_save_filter", "save_filter");
add_action("wp_ajax_save_filter", "save_filter");
function save_filter(){

	if(!isset($_POST["response"]["filter_name"])){return "failed";}
	$status = "success";
	$filename = $_POST["response"]["filter_name"] . ".pdf";

	$user_id = get_post($_POST["domain_id"])->post_author;

	$pdf_data = $_POST["response"]["b64es"];
	$dir_path = wp_upload_dir()["basedir"] . '/users/' . $user_id . '/' . $_POST["domain_id"];

	if( !file_exists($dir_path) ){ mkdir($dir_path, 0777, true); }
	$file_path = $dir_path . "/" . $filename;
	$data = base64_decode($pdf_data);
	$result = file_put_contents($file_path, $data);
	
	echo $status;
	wp_die();
}

add_action("wp_ajax_nopriv_combine_filters", "combine_filters");
add_action("wp_ajax_combine_filters", "combine_filters");
function combine_filters(){

	//Create PDF from all of the images and delete the images
	//Create pub path for the PDF and update post meta

	if( !isset($_POST["domain"]) || !isset($_POST["domain_id"]) ){ return false; }

	$user_id = get_post($_POST["domain_id"])->post_author;

	$dir_path = wp_upload_dir()["basedir"] . '/users/' . $user_id . '/' . $_POST["domain_id"];
	$pub_path = wp_upload_dir()["baseurl"] . '/users/' . $user_id . '/' . $_POST["domain_id"];
	$filters = format_filters($dir_path);

	$merger = new Merger;

	foreach ($filters as $filter) {
		$file_path = $dir_path . "/" . $filter;
		$merger->addFile($file_path);
	}

	$filename = "cbr" . ".pdf";
	$file_path = $dir_path . "/" . $filename;
	$new = $merger->merge();

	$file = fopen($file_path, "w");
	fwrite($file, $new);
	fclose($file);

	//Delete remnant files
	foreach ($filters as $filter) {
		$file_path = $dir_path . "/" . $filter;
		unlink($file_path);
	}

	echo "finished";
	wp_die();
}

function format_filters($dir_path){
	$scanned_files = scandir($dir_path);
	$formatted = [];

	foreach($scanned_files AS $f){
		switch($f){
			case "normal.pdf":
				$formatted[0] = $f;
				break;
			case "achromatomaly.pdf":
				$formatted[1] = $f;
				break;
			case "achromatopsia.pdf":
				$formatted[2] = $f;
				break;
			case "deuteranomaly.pdf":
				$formatted[3] = $f;
				break;
			case "deuteranopia.pdf":
				$formatted[4] = $f;
				break;
			case "protanomaly.pdf":
				$formatted[5] = $f;
				break;
			case "protanopia.pdf":
				$formatted[6] = $f;
				break;
			case "tritanomaly.pdf":
				$formatted[7] = $f;
				break;
			case "tritanopia.pdf":
				$formatted[8] = $f;
				break;
		}
	}

	ksort($formatted);

	return $formatted;
}

add_action("wp_ajax_nopriv_delete_previous_results", "delete_previous_results");
add_action("wp_ajax_delete_previous_results", "delete_previous_results");
function delete_previous_results(){
	if(!isset($_GET["domain_id"])){ return false; }
	
	$result = LCH::delete_audit_data($_GET["domain_id"]);
	echo $result;
	wp_die();
}

add_action("wp_ajax_nopriv_save_scan_results", "save_scan_results");
add_action("wp_ajax_save_scan_results", "save_scan_results");
function save_scan_results(){

	if(!isset($_POST["domain_id"]) || !isset($_POST["results"]) || !isset($_POST["page"])){ return false; }

	$domain_id = $_POST["domain_id"];
	$results = $_POST["results"];
	$page = sanitize_text_field($_POST['page']);

	if ($page == " ") {
		$page = "/";
	}

	try {
		$args = [
			"domainID" => $domain_id,
			"page" => $page,
			"page_results" => $results
		];
		$result = LCH::save_scan_data($args);

		echo $result ? "success" : "failed";
		wp_die();
	} catch (Exception $e) {
		echo 'Caught exception: ', $msg, "\n";
	}
}

//FREE SCAN LOGIC
add_action("wp_ajax_free_scan", "free_scan");
add_action("wp_ajax_nopriv_free_scan", "free_scan");
function free_scan(){
	
	$email = sanitize_text_field($_POST["user_data"]["user_email"]);
	$name = sanitize_text_field($_POST["user_data"]["user_name"]);
	$password = sanitize_text_field($_POST["user_data"]["user_password"]);
	$domain = sanitize_text_field($_POST["domain"]);
	$protocol = sanitize_text_field($_POST["protocol"]);

	$userdata = [
		'user_pass'             => $password,
		'user_login'            => $email,
		'user_nicename'         => $name,
		'user_url'              => '',
		'user_email'            => $email,
		'display_name'          => $email,
		'nickname'              => '',
		'first_name'            => $name,
		'last_name'             => '',
		'description'           => '',
		'rich_editing'          => '',
		'syntax_highlighting'   => '',
		'comment_shortcuts'     => '',
		'admin_color'           => '',
		'use_ssl'               => '',
		'user_registered'       => '',
		'show_admin_bar_front'  => '',
		'role'                  => 'free_compliance_audit_user',
		'locale'                => ''
	];

	$user_id = wp_insert_user( $userdata );
	if ( !is_wp_error($user_id) ){
		update_user_meta($user_id, "_limelight_domain_input_complete", 1);
		update_user_meta($user_id, "_limelight_license_option", "free");
		
		//Create a domain
		$args = [
			"user_id" => $user_id,
			"url" => "'" . $protocol . "://" . $domain . "'",
			"last_scanned" => "'" . Carbon::now("UTC")->toDateString(). "'"
		];

		$domain_id = LCH::add_domain($args);
		
		if ( $domain_id !== FALSE ) {
			$url = home_url("/reports/".$user_id."/". $domain_id);
			$message = "<h1>Congratulations! Your scan results are in. Follow the link below to review them:</h1>".
					   "<div><a href='" . $url . "'>" . $url . "</a></div>".
					   "<h2>Your login information is below</h2>".
					   "<div>Username: ".$email."</div>";
			$headers = ["from:" => "OnlineADA <service@onlineada.com>", "content-type:" => 'text/html; charset=UTF-8'];
			wp_mail($email, "Your scan results are in", $message, $headers, []);
			$dir_path = wp_upload_dir()["basedir"] . '/users/' . $user_id . '/' . $domain_id;
			mkdir($dir_path, 0777, true);
			$ids = ["user_id" => $user_id, "domain_id" => $domain_id];
			wp_send_json(["result" => "OK", "user_id" => $user_id, "domain_id" => $domain_id], 200);
		}
		wp_send_json_error(["result" => "Error", "msg" => "There was an issue saving the domain ". $protocol . "://" . $domain . " to the database", "details" => $domain_id]);
	}
}

// function generateToken($length = 13) {

// 	if (function_exists("random_bytes")) {

// 		$bytes = random_bytes(ceil($length / 2));

// 	} elseif (function_exists("openssl_random_pseudo_bytes")) {

// 		$bytes = openssl_random_pseudo_bytes(ceil($length / 2));

// 	} else {

// 		return uniqid();

// 	}

// 	return substr(bin2hex($bytes), 0, $length);

// }

function page_csv( $request ){
	$domain_id = $request->get_param("id");
	$results = LCH::get_scan_data($domain_id);
	$articles = LCH::getWcagArticlesModel(2);
	$guidelines = LCH::getWcagArticlesModel(1, true);

	$csv = Writer::createFromFileObject(new \SplTempFileObject(), 'w');
	$csv->setNewline("\r\n");
	$report = [];

	//Create report
	//Type: contrast or non-contrast
	foreach($results as $type => $issues){
		foreach($issues as $p){
			$html = stripcslashes($p["html"]);
			if($type === "non-contrast"){
				$a = $p["article"];

				$report[$p["page"]][$type][] = [
					$a . " " . $articles[$a]["title"],
					$html,
					$p["message"]
				];
			}
			if ($type === "contrast") {
				$a = "1.4.3";

				$report[$p["page"]][$type][] = [
					"",
					$html,
					"Actual contrast: " . round($p["contrast"]["ratio"], 2),
					"Font size: " . strtolower($p["font"]["full"]),
					"Bold: " . $p["bold"],
					"Min contrast: " . $p["contrast"]["min_ratio"]
				];
			}
		}
	}

	foreach($report as $page => $types){
		$csv->insertOne(["Page", $page]);
		foreach ($types as $type => $array) {
			if($type === "contrast"){
				$csv->insertOne(["", "1.4.3 Contrast (minimum)"]);
			}
			foreach ($array as $row) {
				array_unshift($row, "");
				$csv->insertOne($row);
			}
			$csv->insertOne([""]);
		}
	}

	header('Content-Type: text/csv; charset=UTF-8');
	header('Content-Description: File Transfer');

	$csv->setOutputBOM(Writer::BOM_UTF8);
	$csv->output("page-report.csv");
}

add_action( 'rest_api_init', 'csv_page_download' );
function csv_page_download() {
   register_rest_route( 'limelight/v1', '/pagecsv/(?P<id>\d+)', array(

       'methods'  => 'GET',
       'callback' => 'page_csv'

   ) );
}

// function audit_settings_page(){
//    $cmb_audit_options = new_cmb2_box( array(
//  		'id'           => 'compliance_audit_metabox',
//  		'title'        => "Compliance Audit Data",
//  		'object_types' => array( 'compliance_audit'),
//         "context"      => "normal",
//         "priority"     => "high",
//         "show_names"   => true,

//  	) );
//     $cmb_audit_options->add_field(
//         array(
//      		'name'       => "Registered Domain:",
//      		'desc'       => "The domain linked to this compliance audit",
//      		'id'         => "_ll_audit_domain",
//      		'type'       => 'text',
//      	)
//     );

//     $cmb_audit_options->add_field(
//         array(
//      		'name'       => "Order ID:",
//      		'desc'       => "The Limelight Payments order ID",
//      		'id'         => "_ll_audit_order_id",
//      		'type'       => 'text_small',
// 			'attributes' => [
// 				"readOnly"   => "true",
// 			]
//      	)
//     );
//     $cmb_audit_options->add_field(
//         array(
//      		'name'       => "Subscription ID:",
//      		'desc'       => "The Limelight Payments subscription type",
//      		'id'         => "_ll_audit_subscription_id",
//      		'type'       => 'text_small',
// 			'attributes' => [
// 				"readOnly"   => "true",
// 			]
//      	)
//     );
// 	$cmb_audit_options->add_field(
//         array(
//      		'name'       => "Sitemap Path:",
//      		'desc'       => "The relative path to the sitemap in the uploads folder",
//      		'id'         => "_sitemap_path",
//      		'type'       => 'text',
// 			'attributes' => [
// 				"readOnly"   => "true",
// 			]
//      	)
//     );
// }
// add_action("cmb2_admin_init", "audit_settings_page");

// function compliance_audit_columns( $columns ) {
//     return LCH::$custom_audit_columns;
// }
// add_filter('manage_edit-compliance_audit_columns', 'compliance_audit_columns');

// function list_audit_table_primary_column( $default, $screen ) {
//     if ( 'edit-compliance_audit' === $screen ) {
//         $default = 'id';
//     }
//     return $default;
// }
// add_filter( 'list_table_primary_column', 'list_audit_table_primary_column', 10, 2 );

// function compliance_audit_orderby( $query ){
// 	if( ! is_admin() ){ return; }
// 	global $post_type;
// 	if ($post_type !== "compliance_audit") { return; }

// 	$orderby = $query->get( 'orderby');
// 	switch($orderby){
// 		case "domain":
// 			$query->set('meta_key', '_ll_audit_domain');
// 			$query->set('orderby', 'meta_value');
// 			break;
// 		case "order_id":
// 			$query->set('meta_key', '_ll_audit_order_id');
// 			$query->set('orderby', 'meta_value_num');
// 			break;
// 		case "subscription_id":
// 			$query->set('meta_key', '_ll_audit_subscription_id');
// 			$query->set('orderby', 'meta_value_num');
// 			break;
// 	}

// }
// add_action( "pre_get_posts", "compliance_audit_orderby" );

// function compliance_audit_sortable_columns( $columns ) {
//     return LCH::$audit_columns_sort_map;
// }
// add_filter('manage_edit-compliance_audit_sortable_columns', 'compliance_audit_sortable_columns');

// //Define rules for how the custom columns will sort
// function compliance_audit_column_sort( $request ) {
// 	global $post_type;
// 	if ($post_type !== "compliance_audit") { return $request; }
// 	if( array_key_exists('orderby', $request )) {

// 		$meta_prefix = "_ll_audit_";
// 		foreach(LCH::$custom_audit_columns as $k => $v){
// 			if($v === $request['orderby']){
// 				switch($v){
// 					case "ID":
// 						$request['orderby'] = $v;
// 						break;
// 					case "owner";
// 						$request['orderby'] = $v;
// 						break;
// 					default:
// 						$request['orderby']  = "meta_value";
// 						$request['meta_key'] = $meta_prefix.$k;
// 						break;
// 				}
// 			}
// 		}
// 	}

// 	return $request;
// }
// add_filter('request', 'compliance_audit_column_sort');

// function compliance_audit_column( $colname, $cptid ) {
// 	$meta_prefix = "_ll_audit_";
// 	$meta_key = $meta_prefix;

// 	switch($colname){
// 		case "id":
// 			$sitemap = get_post_meta($cptid, "_sitemap_path", true);
// 			$link = "<a href=".site_url("/agency/sitemap")."' target='_blank'>" . $cptid . "</a>";
// 			if(!empty($sitemap)){
// 				$link = "<a href='https://onlineada.com/agency/new-scan/" . $cptid . "/options ' target='_blank'>" . $cptid . "</a>";
// 			}
// 			echo $link;
// 			break;
// 		case "owner":
// 			$user_id = get_post((int)$cptid)->post_author;
// 			$email = get_user_by("ID", $user_id)->user_email;
// 			$link = "<a href='" . get_edit_user_link($user_id) . "' target='_blank'>" . $email . "</a>";
// 			echo $link;
// 			break;
// 		case "order_id":
// 			$meta_key .= $colname;
// 			$order_id = get_post_meta($cptid, $meta_key, true);
// 			if($order_id !== "free"){
// 				echo "<a href='https://onlineada.com/wp-admin/post.php?post=". $order_id ."&action=edit' target='_blank'>" . $order_id . "</a>";
// 			}else{
// 				echo $order_id;
// 			}
// 			break;
// 		case "subscription_id":
// 			$meta_key .= $colname;
// 			$sub_id = get_post_meta($cptid, $meta_key, true);
// 			if(!empty($sub_id)){
// 				echo "<a href='https://onlineada.com/wp-admin/post.php?post=". $sub_id ."&action=edit' target='_blank'>" . $sub_id . "</a>";
// 			}else{
// 				echo "No Sub";
// 			}
// 			break;
// 		case "sitemap":
// 			$sitemap = get_post_meta($cptid, "_sitemap_path", true);
// 			$complete = "Not Completed";
// 			if(!empty($sitemap)){
// 				$complete = "Completed";
// 			}
// 			echo $complete;
// 			break;
// 		case "domain":
// 			$domain = get_post_meta( $cptid, "_ll_audit_domain", true );
// 			echo "<a href='".$domain."'>".$domain."</a>";
// 			break;
// 		default:
// 			$meta_key .= $colname;
// 			echo get_post_meta( $cptid, $meta_key, true );
// 	}

// }
// add_action('manage_compliance_audit_posts_custom_column', 'compliance_audit_column', 10, 2);

// function compliance_audit_column_width(){
// 	global $post_type;
// 	if($post_type !== "compliance_audit"){ return; }
// 	echo "<style type='text/css'>";
// 	echo "#id{width:15%;}";
// 	echo "#domain{width:30%;}";
// 	echo "#order_id{width:10%;}";
// 	echo "#subscription_id{width:10%;}";
// 	echo "#owner{width:20%;}";
// 	echo "</style>";
// }
// add_action('admin_head', 'compliance_audit_column_width');

add_action('rest_api_init', 'pre_send_page');
function pre_send_page(){
	register_rest_route('limelight/v1', '/request_data', array(

		'methods' => 'POST',
		'callback' => function (WP_REST_Request $request) {

			$params = $request->get_params();

			if(is_user_logged_in()){
				$user = wp_get_current_user();
				
				//For security, compare the User ID to the author of the audit

				if (!isset($params["domain_id"])) {
					return "Failed: No audit ID present";
				}

				$post = LCH::get_domain_by_id($params["domain_id"]);
				
				if((int)$post->user_id !== (int)$user->ID){
					return "Failed: audit does not belong to current user";
				}

				if (empty($post)) {
					return "Failed: There was an issue retrieving audit #" . $params["domain_id"];
				}
				
				$domain_data = LCH::split_domain($post->url);

				$data = [
					"domain" => $domain_data["domain"],
					"protocol" => $domain_data["protocol"],
					"page" => $params["page"],
					"full_version" => in_array("compliance_audit_user", $user->roles) || in_array("agency_user", $user->roles)
				];
			}

			if(!is_user_logged_in()){
				$data = [
					"domain" => $params["domain"],
					"protocol" => $params["protocol"],
					"page" => "/",
					"full_version" => false
				];
			}
			
			$toSend = http_build_query($data);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://scan.onlineada.com/webservice/v1/scan/audit");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $toSend);
			$return_data = curl_exec($ch);
			curl_close($ch);
			
			if(strpos($return_data, "ProcessTimedOutException")){
				wp_send_json(["status" => "fail", "msg" => "The site " . $data["domain"] . " took too long to load"], 418);
			}

			return $return_data;
		}

	));
}

add_action('before_delete_post', 'll_delete_audit_data');
function ll_delete_audit_data($id)
{
	global $post_type;
	if ($post_type != 'compliance_audit') return;
	$domain = get_post_meta($id, "_ll_audit_domain", true);
	$user_id = get_post($id)->post_author;
	$path = wp_upload_dir()["basedir"] . "/$user_id/$domain";
	$file = $path . "/snapshot.png";
	LCH::delete_audit_data($id);

	if (file_exists($file) and is_file($file)) {
		unlink($file);
	}
	if (is_dir($path)) {
		rmdir($path);
	}
}

add_action('delete_user', 'll_delete_user');
function ll_delete_user($id){

	$args = [
		"author" => $id,
		"post_type" => "compliance_audit",
		"numberposts" => -1
	];

	$audits = get_posts($args);

	if(!empty($audits)){
		foreach($audits as $audit){
			$domain = get_post_meta($audit->ID, "_ll_audit_domain", true);

			$root_path = wp_upload_dir()["basedir"] . "/$id";
			$path = $root_path . "/$domain";
			$file = $path . "/snapshot.png";
			LCH::delete_audit_data($audit->ID);

			if (file_exists($file) and is_file($file)) {
				unlink($file);
			}
			if (is_dir($path)) {
				rmdir($path);
			}
			if(is_dir($root_path)){
				rmdir($root_path);
			}
		}
	}
}






