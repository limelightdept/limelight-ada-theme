<?php
//Custom styles for the certification screens
function custom_admin_styles(){
    if(get_current_screen()->id === "edit-ada_certification"){
      echo '<style>
        #wpseo-filter, #wpseo-readability-filter, li.yoast_cornerstone, li.publish{display:none;}
        #id.column-id{width:13%;}
        #user.column-user{width:20%;}
        </style>
        ';
        // <script>
        //     setTimeout(function(){
        //        var str = jQuery(".subsubsub li:nth-child(4)").html();
        //        str = str.slice(0, -2);
        //        jQuery(".subsubsub li:nth-child(4)").html(str);
        //        str = jQuery(".subsubsub li:nth-child(2)").html();
        //        str = str.slice(0, -2);
        //        jQuery(".subsubsub li:nth-child(2)").html(str);
        //     }, 200);
        // </script>'
    }elseif(get_current_screen()->id === "ada_certification"){
      echo '<style>
         #certification_metabox>h2, certification_metabox>button{display:none;}
         .cmb2-id--ada-cert-reviews-remaining > .cmb-td .middle{padding:0 15px;}
         .cmb2-id--ada-cert-reviews-remaining > .cmb-td{display: inline-flex;align-items: center;}
         .cmb2-id--ada-cert-reviews-remaining > .cmb-td button:focus{outline:0;}
         .cmb2-id--ada-cert-reviews-remaining > .cmb-td button:active{
            background: #eee;
            border-color: #999;
            box-shadow: inset 0 2px 5px -3px rgba(0,0,0,.5);
            -webkit-transform: translateY(1px);
            transform: translateY(1px);
            outline:0;
         }
         .cmb2-id--ada-cert-reviews-remaining > .cmb-td button{
            color: #555;
            border-color: #ccc;
            background-color: #f7f7f7;
            box-shadow: 0 1px 0 #ccc;
            vertical-align: top;
            font-size: 13px;
            line-height: 26px;
            height: 28px;
            margin: 0;
            padding: 0 10px 1px;
            cursor: pointer;
            border-width: 1px;
            border-style: solid;
            -webkit-appearance: none;
            border-radius: 3px;
            white-space: nowrap;
            box-sizing: border-box;
         }
         .cmb2-id--ada-cert-reviews-remaining > .cmb-td .right{padding-right:5px;}
         </style>';
         $number_html = '<span class="left"><button type="button">-</button></span><span class="middle"></span><span class="right"><button type="button">+</button></span>';
      echo "<script>
         setTimeout(function(){
            jQuery('#reviews').addClass('hidden');";
      echo "jQuery('.cmb2-id--ada-cert-reviews-remaining > .cmb-td').prepend('" . $number_html . "');";
      echo "var default_val = jQuery('#reviews').val();
            console.log(default_val);
            jQuery('.middle').append(default_val);
            jQuery('.cmb2-id--ada-cert-reviews-remaining .left').on('click', function(){
               var total = jQuery('#reviews').attr('value') -1;
               jQuery('#reviews').attr('value', total);
               jQuery('.middle').html(total);
            });
            jQuery('.cmb2-id--ada-cert-reviews-remaining .right').on('click', function(){
               var total = parseInt(jQuery('#reviews').attr('value')) +1;
               jQuery('#reviews').attr('value', total);
               jQuery('.middle').html(total);
            });
         }, 10);
      </script>";
    }
}
add_action('admin_head', 'custom_admin_styles');
