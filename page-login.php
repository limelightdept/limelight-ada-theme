<?php
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
get_header();

$redirect_to = get_query_var("redirect_to");
if(!empty($redirect_to)){
	?>
	<script>
		jQuery(function($){
			$("form").prepend("<input type='hidden' name='redirect_to' value='<?php echo $redirect_to; ?>'>");
        });
    </script>
	<?php
}

$reset_link = "<a class='reset-password' href='". wp_lostpassword_url( get_permalink() ) ."' >Reset Password</a>";
?>
<script>
    document.addEventListener("ll_hooker_init", function(e, data){
        initHookr(e.detail);
    });

    function initHookr(ll_hooker){
        ll_hooker.addAction("ll_form_init", "ll_form", function(vm){
            jQuery("[name=remember_me]").closest(".ll-element-wrap").children("div").css("display", "flex");
            jQuery("[name=remember_me]").parent().after("<?php echo $reset_link; ?>");
        });
        ll_hooker.addAction("ll_form_event_wp_login_success", "ll_form", function(Vue, response){
            if(Vue.request.form == "3150"){
                window.location = response.redirect_to;
            }
        });
    }
    
</script>
<style>
    .elementor-message-success{
        display:none;
    }
    .ll-message.ll-success{
        display:none;
    }
    [name=remember_me]{
        margin: 5px 10px;
    }
    .reset-password{
        color:#fff;
    }
    .reset-password:hover{
        color:#f93a25;
    }
</style>
    <main id="main">
        <div class="outer-grid">
            <div class="grid">
                <?php
                while (have_posts()) : the_post();
                ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <div class="entry-content">
                            <?php the_content(); ?>
                        </div>
                    </article>
                <?php
                endwhile;
                ?>
            </div>
        </div>
    </main>
<?php
get_footer();
