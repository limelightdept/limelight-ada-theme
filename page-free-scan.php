<?php
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}

get_header();

wp_localize_script("free-scan-scripts", "php_var", [
	"api_nonce" => wp_create_nonce("wp_rest"),
	"logged_in" => is_user_logged_in(),
	"site_url" => site_url(),
	"remote_uri" => "https://scan.onlineada.com"
]);

?>
<style>
	.scanning-container{flex-direction:column;}
	.processing{
		display: flex;
    	flex-direction: column;
    	align-items: center;
    	text-align: center;
	}
	.processing .header{
		font-size:24px;
		font-family: "Open Sans";
	}
	.processing .loader{
		width:50px;
		height:50px;
	}
	.processing .comment{

	}
</style>
    <div id="main">
        <div class="outer-grid">
			<div class="grid">
			<?php
			while (have_posts()) : the_post();
			?>
				<main id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<div class="content-body">
							<?php the_content(); ?>
						</div>
					</div>
				</main>
			<?php
			endwhile;
			?>
			</div>
        </div>
    </div>
<?php
get_footer();
