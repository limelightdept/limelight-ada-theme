<?php
use Carbon\Carbon;
require_once 'includes/utility/class-limelight-child-helpers.php';
require_once 'includes/admin-styles/styles.php';
require_once 'includes/certification/certification-related.php';
require_once 'includes/cf7/cf7-related.php';
require_once 'includes/gates/gate-related.php';
require_once 'includes/login/login-related.php';
require_once 'includes/limelight-payments/payments-related.php';
require_once 'includes/site-audit/site-audit-related.php';
require_once 'includes/color-blind/color-blind-checker-related.php';
require_once 'includes/widgets/widget-related.php';
require_once 'includes/elementor/elementor-related.php';
require_once 'includes/forms/forms-related.php';
require_once 'vendor/autoload.php';


use iio\libmergepdf\Merger;
use iio\libmergepdf\Pages;
use Limelight_Child_Helpers as LCH;

add_action("admin_init", function(){
	//LCH::migrate_audit_data();
});

add_filter( 'wp_mail_from_name', "ada_change_from_name");
function ada_change_from_name($from_name){
	return "Online ADA";
}

add_filter( 'wp_mail_from', "ada_change_from_address");
function ada_change_from_address($from_email){
	return "service@onlineada.com";
}

function is_members() {
	//Members Dashboard is page ID 935
	global $post;
	if(is_page()&&($post->post_parent==935||is_page(935))) 
        return true;
	else 
        return false; 
};

function oada_add_footer_shortchode(){	
	ob_start();

	global $page_display_options;

    if(!is_array($page_display_options)) $page_display_options = array();

    $hide_navigation = in_array("hide_footer_navigation", $page_display_options);
    $show_policies = in_array("always_show_policies", $page_display_options);
    $policy_pages = [
        'privacy' => limelight_get_option("policy_page_privacy"),
        'terms' => limelight_get_option("policy_page_terms"),
        'disclaimer' => limelight_get_option("policy_page_disclaimer"),
    ];
    // set the image url
    $image_id = limelight_get_option("logo_footer_id");

    $logo = limelight_get_option("logo_footer_id");

    // retrieve the thumbnail size of our image
    $logo = wp_get_attachment_image($image_id, 'full');

    $logo_html = limelight_get_option("logo_footer_html");
    if(!empty($logo_html)) $logo = $logo_html;
        
    $hide_limelight_logo = limelight_get_option("logo_footer_hide");    

    $footer_layout = limelight_get_option("footer_layout");
    if(empty($footer_layout))
        $footer_layout = "default";
	?>
<footer id="site-footer" class="elementor">
    <div class="elementor-section elementor-section-boxed">
        <div class="elementor-container site-footer-<?php echo $footer_layout; ?>">
        <?php
        
        if(file_exists(get_stylesheet_directory()."/templates/footer/footer-".$footer_layout.".php"))
            include(get_stylesheet_directory()."/templates/footer/footer-".$footer_layout.".php");
        else
            include(get_template_directory()."/templates/footer/footer-".$footer_layout.".php");
        ?>
        </div>
    </div>
</footer>
<?php
return ob_get_clean();
}
add_shortcode("footer-logo_center", 'oada_add_footer_shortchode');

function enqueue_custom_script(){
    wp_register_script( "ll-custom-script", get_stylesheet_directory_uri() . "/assets/js/ll-custom-script.js", array("jquery"), "1.0", true );
    wp_localize_script( "ll-custom-script", "ajax", array( "url" => admin_url( "admin-ajax.php" ) ) );
    wp_enqueue_script( "ll-custom-script" );

	//Scripts and styles should be named {slug}-scripts.js, {slug}-styles.css for this to work.
	//This code block ensures only necessary styles and scripts are loaded on required pages
	$assets_uri = get_stylesheet_directory_uri() . "/assets/";
	$assets_folder = get_stylesheet_directory() . "/assets/";

	$pages = [		
		'free-scan' 		=> [  ],
		'gettheguide' 		=> [  ]
	];

	wp_enqueue_script("free-scan");

	foreach($pages AS $slug => $extra){
		$script_uri = $assets_uri."js/".$slug."-scripts.js";
		$script_file = $assets_folder."js/".$slug."-scripts.js";

		$stylesheet_uri = $assets_uri."styles/".$slug."-styles.css";
		$stylesheet_file = $assets_folder."styles/".$slug."-styles.css";
		if(file_exists($script_file) AND is_file($script_file) AND is_page($slug)){
			$handle = $slug."-scripts";
			wp_register_script($handle, $script_uri, array("jquery"), filemtime($script_file), true);
			wp_enqueue_script($handle);
		}

		if(file_exists($stylesheet_file) AND is_file($stylesheet_file) AND is_page($slug)){
			$handle = $slug."-styles";
			wp_enqueue_style($handle, $stylesheet_uri);
		}
	}

	if(is_page("manage-domains")){
		wp_register_script("manage-domains", $assets_uri."js/manage-domains.js", ["jquery"], rand(), true);
		wp_enqueue_script("manage-domains");
		
		$domains = LCH::get_domains(wp_get_current_user()->ID);
		$domains = json_encode($domains);
		
		wp_localize_script("manage-domains", "php_var", ["domains" => $domains]);
	}
	
	// 	if(isset($extra["extra_params"]) AND is_page($slug)){
	// 		if(isset($extra["extra_params"]["styles"])){
	// 			foreach($extra["extra_params"]["styles"] AS $handle => $path){
	// 				wp_enqueue_style($handle, $path);
	// 			}
	// 		}

	// 		if(isset($extra["extra_params"]["scripts"])){
	// 			foreach($extra["extra_params"]["scripts"] AS $data){
	// 				wp_enqueue_script($data["handle"], $data["path"], $data["deps"], $data["ver"], true );
	// 			}
	// 		}
	// 	}
	// }
}
add_action("wp_enqueue_scripts", "enqueue_custom_script");

function enqueue_admin_style(){
   wp_enqueue_style("fontawesome5", "https://use.fontawesome.com/releases/v5.1.0/css/all.css", [], "5.1.0");
}
add_action("admin_enqueue_scripts", "enqueue_admin_style");

function register_tertiary_menu() {
	register_nav_menu('footer_tertiary',__( 'Footer - Tertiary' ));
}
add_action( 'init', 'register_tertiary_menu' );

function set_html_content_type(){
    return 'text/html';
}
add_filter( 'wp_mail_content_type','set_html_content_type' );

function password_reset_link_fix($message, $key, $user_login, $user_data){
	$message = "Someone has requested a password reset for the following account:

	" . sprintf(__('%s'), $user_data->user_email) . "
	
	If this was a mistake, just ignore this email and nothing will happen.
	
	To reset your password, visit the following address:
	
	" . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . "\r\n";
	
	return $message;
}
add_filter("retrieve_password_message", "password_reset_link_fix", 99, 4);

function register_query_vars( $vars ) {
	$vars[] = 'badge';
    $vars[] = "post_id";
	$vars[] = "license_option";
	$vars[] = "key";
	$vars[] = "verify";
	$vars[] = "redirect_to";
	return $vars;
}
add_filter( 'query_vars', 'register_query_vars' );

function nav_items_filter($items, $menu, $args){
	
	$user = wp_get_current_user();
	if($menu->name !== "Membership Nav" AND $menu->name !== "Free Scan Nav"){
		foreach($items AS $index => $item ){
			if($item->object === "page"){
				$roles_access = get_post_meta($item->object_id, "role_multicheckbox", true);

				if(!empty($roles_access) AND !is_user_logged_in()){
					unset($items[$index]);
				}

				$all_roles = in_array("all-roles", (array)$roles_access);

				if(!$all_roles AND !empty($roles_access)){
					$result = array_intersect((array) $roles_access, (array) $user->roles);
					//User does not have access to this page
					if(empty($result) AND !in_array('administrator', $user->roles) ){
						unset($items[$index]);
					}
				}

			}elseif($item->object === "custom"){
				$roles_access = $item->classes;
				$role_classes = [
					"certification_user",
					"free_compliance_audit_user",
					"compliance_audit_user",
					"agency_user",
					"all_roles",
					"administrator"
				];
				$result = array_intersect($roles_access, $role_classes);
				if(!empty($result) AND !is_user_logged_in()){
					unset($items[$index]);
				}
			}
		}
	}
	if($menu->name === "Free Scan Nav"){
		$cert_args = [
			'author'    => $user->ID,
			'post_type' => 'ada_certification',
		];
		$audit_args = [
			'author'    => $user->ID,
			'post_type' => 'compliance_audit',
		];
		
		$certs  = get_posts($cert_args);
		$audits = get_posts($audit_args);

		foreach($items AS $index => $item ){

			//Change the default members-audit URL to be the user's audit URL if they have one or more audits
			if(stripos( $item->url, "/members-audit/" ) !== false){
				//unset($items[$index]);
				if(count($audits) === 1){
					$item->old_url = $item->url;
					$item->url = "#";
				}
				elseif(count($audits) > 1){
					$item->old_url = $item->url;
					$item->url = "#";
				}else{
					unset($items[$index]);
				}
			}
		}
		
	}
	if($menu->name === "Membership Nav"){
		global $certs;
		global $audits;

		$user_roles = [];
		if(is_user_logged_in()){
			//$user_roles = get_userdata($user->ID)->roles;
			$user_roles = $user->roles;
		}
		
		$cert_args = [
			'author'    => $user->ID,
			'post_type' => 'ada_certification',
		];
		$audit_args = [
			'author'    => $user->ID,
			'post_type' => 'compliance_audit',
		];

		$certs  = get_posts($cert_args);
		$audits = get_posts($audit_args);

		foreach($items AS $index => $item ){
			if($item->object === "page"){
				$roles_access = get_post_meta($item->object_id, "role_multicheckbox", true);
				
			}elseif($item->object === "custom"){
				$roles_access = $item->classes;
			}
			

			$all_roles = in_array("all-roles", $roles_access);

			if(!$all_roles){
				$result = array_intersect($roles_access, $user_roles);
				
				//User does not have access to this page
				if(empty($result) AND !in_array('administrator', $user_roles)){
					unset($items[$index]);
				}
			}
			//Change the default members-certification URL to be the user's certification URL if they have one or more certifications
			if(stripos( $item->url, "/members-certification/" ) !== false){
				if(count($certs) === 1){
					$item->old_url = $item->url;
					$item->url = get_permalink($certs[0]->ID);
				}
				elseif(count($certs) > 1){
					$item->old_url = $item->url;
					$item->url = "#";
				}
			}
			//Change the default members-audit URL to be the user's audit URL if they have one or more audits
			if(stripos( $item->url, "/members-audit/" ) !== false){
				if(count($audits) === 1){
					$item->old_url = $item->url;
					$item->url = "#";
				}
				elseif(count($audits) > 1){
					$item->old_url = $item->url;
					$item->url = "#";
				}
			}
		}
	}

	return $items;
}
add_filter('wp_get_nav_menu_items', 'nav_items_filter', 10, 3);

class Limelight_Walker extends Walker_Nav_Menu {

	public function start_el(&$output, $item, $depth=0, $args=array(), $id = 0) {
		$user = wp_get_current_user();
		$cert_args = [
			'author'    => $user->ID,
			'post_type' => 'ada_certification',
		];
		$certs  = get_posts($cert_args);
		$audit_args = [
			'author'    => $user->ID,
			'post_type' => 'compliance_audit',
		];

		$audits = get_posts($audit_args);
		$object = $item->object;
		$type = $item->type;
		$title = $item->title;
		$permalink = $item->url;

		if(stripos( $item->url, "/members-certification/" ) !== false AND count($certs) > 1){
			$output .= "<li class='menu-item-has-children "  . implode(" ", $item->classes) . "'>";
		}else if(stripos( $item->url, "/members-audit/" ) !== false AND count($audits) > 1){
			$output .= "<li class='menu-item-has-children "  . implode(" ", $item->classes) . "'>";
		}else{
			$output .= "<li class='" . implode(" ", $item->classes) . "'>";
		}
		//$output.= "<div class='effect-wrap'><div class='effect'></div>";
		if( $permalink && $permalink != '#' ) {
			$output .= '<a class="link-padding" href="' . $permalink . '">';
		} else {
			$output .= '<span class="header-text header-padding">';
		}
		
		if(stripos( $item->url, "/members-certification/" ) !== false || stripos( $item->old_url, "/members-certification/" ) !== false)
		{
			if(count($certs) === 1)
			{
				$output .= $certs[0]->post_title;
			}
			else
			{
				$output .= $title;
			}
		}
		
		else
		{
			$output .= $title;
		}

		if( $permalink && $permalink != '#' ) {
			$output .= '</a>';
		} else {
			$output .= '</span>';
		}
		//$output.= "</div>";
    }

	public function end_el( &$output, $item, $depth = 0, $args = array() ) {
		if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
			$t = '';
			$n = '';
		} else {
			$t = "\t";
			$n = "\n";
		}
		$output .= "</li>{$n}";

		$user = wp_get_current_user();
		$cert_args = [
			'author'    => $user->ID,
			'post_type' => 'ada_certification',
		];
		$certs  = get_posts($cert_args);
		$audit_args = [
			'author'    => $user->ID,
			'post_type' => 'compliance_audit',
		];

		$audits = get_posts($audit_args);
		
		$url = isset($item->old_url) ? $item->old_url : $item->url;

		if(stripos( $url, "/members-certification/") !== false AND count($certs) > 1){
			$output .= '<ul class="sub-menu">';
			foreach($certs AS $c){
				$output .= '<li class="menu-item menu-item-type-custom menu-item-object-custom"><a class="link-padding" href="' . get_post_meta($c->ID, "_ll_audit_new_scan_url", true) . '">' . $c->post_title . '</a></li>';
			}
			$output .= '</ul>';
		}
		
		if(stripos($url, "/members-audit/") !== false && in_array("free_compliance_audit_user", $user->roles) && count($audits) > 1){
			// URL to a report:  /agency/reports/{user->ID}/{audit->ID}
			//NOT CURRENTLY IN USE
			$output .= '<ul class="sub-menu">';
			foreach($audits AS $c){
				$title = str_replace("http://", "", $c->post_title);
				$title = str_replace("https://", "", $c->post_title);
				$output .= '<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-long">';
				$output .= '<ul class="sub-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom">';
				$output .= "<form action='/agency/reports/{$user->ID}/{$c->ID}' method='POST'>";
				$output .= "<button>'.$title.'</button></form>";
				$output .= "</li></ul></li>";
			}
			$output .= '</ul>';

		}else if( stripos( $url, "/members-audit/" ) !== false && count($audits) === 1 && in_array("free_compliance_audit_user", $user->roles) ){
			$title = str_replace("http://", "", $audits[0]->post_title);
			$title = str_replace("https://", "", $audits[0]->post_title);
			$output .= '<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-long">';
			$output .=		"<form action='/agency/reports/{$user->ID}/{$audits[0]->ID}' method='POST'>";
			$output .= '<button>'.$title.'</button></form></li>';
		}
	}
}

add_filter("Limelight\\Forms\\Events\\RegisteredEvents", "register_events");
add_filter("Limelight\\Forms\\BuilderEvents\\ll-form", "add_events");

function register_events($events){

	$events = array_merge($events, [
		"register_domains" => "Limelight\Plugin\Forms\Events\RegisterDomains",
	]);

	return $events;

}
function add_events($events){
	array_unshift($events, "register_domains");
	return $events;
}