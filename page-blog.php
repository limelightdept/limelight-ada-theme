<?php
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
get_header();
?>
    <div id="main">
        <div class="outer-grid">
			<div class="grid">
			<?php
			while (have_posts()) : the_post();
			?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<span style="padding:30px;font-family:'Josefin Sans';color:#ffffff;font-size:26px;width:100%;display:block;">Digital Accessibility Articles</span>
					<main id="blog-main" class="vc_col-sm-8 entry-content">
						<?php the_content(); ?>
					</main>
					<?php get_template_part("sidebar"); ?>
				</div>
			<?php
			endwhile;
			?>
			</div>
        </div>
    </div>
	
<?php
get_footer();
