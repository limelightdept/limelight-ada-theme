<?php
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
get_header();
$license_option = get_user_meta(wp_get_current_user()->ID, "_limelight_license_option", true);

switch($license_option){
	case "5":
		$form_to_hide = "1732";
		break;
	case "10":
		$form_to_hide = "1731";
		break;
}

?>
<style>
	#payment-form-<?php echo $form_to_hide; ?>{display:none;}
</style>
<script>
	jQuery(document).ready(function($){
		$("#payment-form-<?php echo $form_to_hide; ?>").remove();
	});
</script>
    <main id="main">
        <div class="outer-grid">
			<div class="grid">
			<?php
			while (have_posts()) : the_post();
			?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
				</article>
			<?php
			endwhile;
			?>
			</div>
        </div>
    </main>
<?php
get_footer();
