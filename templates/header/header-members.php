<style id="mobile-nav-height">

</style>
<div class="header-wrap">
    <div class="header-content-left mobile-nav-override">
        <div class="mobile-menu-button-wrapper limelight-navigation">
            <button class="mobile-menu-button" aria-label="Menu">
                <span class="menu-bars">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
                <span class="mobile-text">
                    <span class="mobile-hidden">Menu</span>
                    <span class="mobile-visible">Close</span>
                </span>
            </button>
        </div>
        <div id="mobile-nav">
            <div class="left-column">

            </div>
            <div class="right-column">
                <div class="mobile-menu limelight-navigation">
                    <?php
                        $user = wp_get_current_user();
                        if(in_array("free_compliance_audit_user", $user->roles)){
                            $args = array(
                                'menu'   => 'Free Scan Nav',
                                'walker' => new Limelight_Walker()
                            );
                        }else{
                            $args = array(
                                'menu'   => 'Membership Nav',
                                'walker' => new Limelight_Walker()
                            );
                        }
                        echo '<nav class="nav-menu nav-mobile nav-primary">';
                        wp_nav_menu( $args );
                        echo '</nav>';
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="header-content-right">
        <a <?php if(!$hide_header_navigation){ ?> href="/"<?php } ?> class="logo">
            <?php
            // display the image
            echo '<img src="' . content_url("themes/limelight-theme-online-ada/assets/img/Logo-Hor.svg") . '"class="logo" alt="ADA Online Logo">';
            ?>
        </a>
    </div>	
</div>
<?php
