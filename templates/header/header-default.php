<div class="header header-default">
    <a <?php if(!$hide_header_navigation){ ?> href="/"<?php } ?> class="logo">
        <?php
        // display the image
        echo $logo;
        ?>
    </a>
    <div class="limelight-navigation">
    <?php

    // Primary Menu
    if ( has_nav_menu('header_primary') ) {
            $args = array(
                    'theme_location' => 'header_primary',
                    'menu'           => 'Header - Primary',
                    'container'      => '',
                    'container_id'   => '',
                    'menu_class'     => '',
                    'items_wrap'     => '<ul class="nav-list">%3$s</ul>',
            );

            echo '<nav class="nav-menu nav-header nav-primary">';
            wp_nav_menu($args);
            echo '</nav>';
    }

    // Secondary Menu
    if ( has_nav_menu('header_secondary') ) {
            $args = array(
                    'theme_location' => 'header_secondary',
                    'menu'           => 'Header - Secondary',
                    'container'      => '',
                    'container_id'   => '',
                    'menu_class'     => '',
                    'items_wrap'     => '<ul class="nav-list">%3$s</ul>',
            );

            echo '<nav class="nav-menu nav-header nav-secondary">';
            wp_nav_menu($args);
            echo '</nav>';
    }
    ?>

    </div>
</div>
