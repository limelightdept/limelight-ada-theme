<?php
use Limelight\Plugins\ADA_Webservice\API;
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}

global $domain;

$audits = get_posts(["author" => $user->ID, "post_type" => "compliance_audit", "numberposts" => -1]);

$domains = [];

foreach($audits AS $audit){
	$domain = get_post_meta($audit->ID, "_ll_audit_domain", true);
	$domain = Limelight_Child_Helpers::split_domain($domain);
	$path = $basedir.get_post_meta($audit->ID, "_sitemap_path", true);
	$has_sitemap = "true";

	if(!is_file($path)){
		$has_sitemap = "false";
	}
	if(is_file($path)){
		$domains[$audit->ID] = $domain;
	}
	$domains[$audit->ID] = $domain;
	$domains[$audit->ID]["has-sitemap"] = $has_sitemap;
	//If free user does not have a sitemap
	if(!is_file($path) AND in_array("free_compliance_audit_user", $user->roles)){
		$rel_path = "/".$user->ID."/".$domain['domain']."/sitemap/";
		$filename = "sitemap.csv";
		$create_path = $basedir.$rel_path.$filename;
		API::create_sitemap($domain["domain"], true);
		$sitemap = API::get_sitemap();
		unset($sitemap["domain"]);
		Limelight_Child_Helpers::generate_file( $create_path, $sitemap, $basedir, $rel_path );
		update_post_meta( $audit->ID, "_sitemap_path", $rel_path.$filename );
		$domains[$audit->ID] = $domain;
		$domains[$audit->ID]["has-sitemap"] = "true";
	}
}

get_header();

?>
    <main id="main" class="page-template-members">
        <div class="outer-grid">
            <div class="grid">
                <?php
                while (have_posts()) : the_post();
                ?>
                    <div class="content-wrapper dark">
                        <div class="content-header">
							<header class="head-wrap">
								<h2>Select a website to scan</h2>
							</header>
                        </div>
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <div class="entry-content">
                                <div class="body-wrap content-body">
									<form action="" method="post">
										<div class="select-wrap">
											<select class="website-select ll-input" name="website-select">
												<?php foreach($domains AS $audit_id => $domain): ?>
													<?php if( $domains[$audit_id]["has-sitemap"] === "true" ): ?>
														<option value="<?php echo $audit_id ?>"><?php echo untrailingslashit($domain["protocol"]."://".$domain["domain"]); ?></option>
													<?php endif; ?>
												<?php endforeach; ?>
											</select>
										</div>
										<button class="submit-button">SUBMIT</button>
									</form>
									<?php if(in_array("compliance_audit_user", $user->roles)): ?>
										<p>If you don't see a website here but have registered the domain, make sure you've uploaded a sitemap for it <a href="<?php echo home_url("sitemap"); ?>">here</a></p>
									<?php endif; ?>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php
                endwhile;
                ?>
            </div>
        </div>
    </main>
<?php
get_footer();
