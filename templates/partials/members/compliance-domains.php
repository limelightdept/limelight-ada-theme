<?php

if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
if(!is_user_logged_in()){
	$token = get_query_var("verify");
	if(!is_null($token) AND $token === "true"){
		wp_redirect( home_url("/login?verify=true") );
		exit;
	}
    wp_redirect( home_url("/login") );
	exit;
}

$user = wp_get_current_user();

if(is_user_logged_in() AND in_array("free_compliance_audit_user", $user->roles) AND !metadata_exists("user", $user->ID, "_limelight_email_verified")){
	update_user_meta($user->ID, "_limelight_email_verified", "verified");
}

$license_option = get_user_meta($user->ID, "_limelight_license_option", true);

switch($license_option){
	case "free":
		$ids_to_hide = ["#single-license-content", "#multi-5-license-content", "#multi-10-license-content"];
		break;
	case "1":
		$ids_to_hide = ["#free-license-content", "#multi-5-license-content", "#multi-10-license-content"];
		break;
	case "5":
		$ids_to_hide = ["#single-license-content", "#free-license-content", "#multi-10-license-content"];
		break;
	case "10":
		$ids_to_hide = ["#single-license-content", "#multi-5-license-content", "#free-license-content"];
		break;
}

$audits = get_posts(["author" => $user->ID, "post_type" => "compliance_audit", "order" => "ASC"]);

$existing_domains = [];
if(!empty($audits)){
	foreach($audits AS $audit){
		$existing_domains[] = get_post_meta($audit->ID, "_ll_audit_domain", true);
	}
}

get_header();

?>
<style>
	<?php foreach($ids_to_hide AS $id): ?>
		<?php echo $id; ?>{display:none;}
	<?php endforeach; ?>
	.disabled{position:relative;z-index:0;}
	.disabled:after{
		content:" ";
		position:absolute;
		top:0px;
		bottom:0px;
		left:0px;
		right:0px;
		z-index:1;
		background-color:black;
		opacity:.1;
	}
</style>
<script>
	<?php $ids = implode(",", $ids_to_hide); ?>
	jQuery(function($){
		$("<?php echo $ids; ?>").remove();
		<?php foreach($existing_domains AS $i => $domain): ?>
			$("[name='domain-<?php echo $i+1; ?>']").attr("value", "<?php echo $domain; ?>").attr("readOnly", "true").parent().addClass("disabled");
		<?php endforeach; ?>
		var all_domains_filled = true;
		$(".ll-input").each(function(){			
			if( typeof $(this).attr("readonly") == "undefined"){
				all_domains_filled = false;
			}
		});
		if(all_domains_filled){
			$(".wpcf7-submit").remove();
		}
	});
</script>

    <main id="main">
        <div class="outer-grid">
            <div class="grid">
                <?php
                while (have_posts()) : the_post();
                ?>
                    <div class="content-wrapper dark">
                        <div class="content-header">
                            <div class="content-header-bottom">
                                <div class="content-title"><?php the_title(); ?></div>
                            </div>
                            <hr class="divider full" />
                        </div>
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <div class="entry-content">
                                <div class="content-body">
									<?php the_content() ?>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php
                endwhile;
                ?>
            </div>
        </div>
    </main>
<?php
get_footer();
