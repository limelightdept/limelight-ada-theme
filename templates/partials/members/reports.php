<?php

use Limelight_Child_Helpers AS LCH;

$user_id = get_query_var("user_id");
$audit_id = get_query_var("audit");
$audit = get_post($audit_id);
$results = LCH::get_scan_data($audit_id);
$cbr_report = get_post_meta($audit_id, "_ll_audit_cbr_path", true);
$user_id = get_current_user_id();
$used_qs = get_post_meta($audit_id, "using_query_strings", true);

if (empty($cbr_report)) {
	$cbr_report = false;
}

$full_url = get_post_meta($audit_id, "_ll_audit_domain", true);
$trimmed = preg_replace('/^(http[s]?:[\/]{0,2})?/', "", $full_url);

$get_snapshot = false;
$snapshot = wp_upload_dir()["basedir"] . "/" . $user_id . "/" . $trimmed . "/snapshot.png";

if (!file_exists($snapshot) || !is_file($snapshot)) {
	$get_snapshot = true;
}

if (file_exists($snapshot) || is_file($snapshot)) {
	$snapshot_url = wp_upload_dir()["baseurl"] . "/" . $user_id . "/" . $trimmed . "/snapshot.png";
}

$total_pages = 1;
if(!in_array("free_compliance_audit_user", $user->roles)){
	$path = wp_upload_dir()["basedir"] . get_post_meta($audit_id, "_sitemap_path", true);
	$sitemap = LCH::get_sitemap_from_file($path, $full_url);
	unset($sitemap["domain"]);
	if (empty($sitemap) || !$sitemap) {
		$sitemap = [];
	}

	if (!empty($sitemap)) {
		$nq_pages = !empty($sitemap["non_query_links"]) ? count($sitemap["non_query_links"]) : 0;
		$q_pages = !empty($sitemap["query_links"]) ? count($sitemap["query_links"]) : 0;
	}

	$total_pages = $nq_pages;
	if($used_qs){ $total_pages + $q_pages; }
}

$assets_uri = get_stylesheet_directory_uri() . "/assets/img/report-assets/";
$assets_folder = get_stylesheet_directory() . "/assets/img/report-assets/";

$icons = [
	"error" => $assets_uri . "error-icon.png",
	"contrast" => $assets_uri . "contrast-icon.png",
	"warning" => $assets_uri . "warning-icon.png",
	"star" => $assets_uri . "star.svg",
	"tooltip" => $assets_uri . "tooltip_arrow.png"
];

get_header();

wp_localize_script(
	"reports-scripts",
	"report_vars",
	[
		"results" => $results,
		"wcag" => json_encode(["guidelines" => LCH::getWcagArticlesModel(1, true), "articles" => LCH::getWcagArticlesModel(2)]),
		"audit_id" => $audit_id,
		"domain" => $full_url,
		"user_id" => $user->ID,
		"get_snapshot" => $get_snapshot,
		"icons" => $icons,
		"total_pages" => $total_pages,
		"site_api_url" => site_url() . "/wp-json/limelight/v1/"
	]
);

?>
    <div id="main">
        <div class="outer-grid">
			<div class="grid">
			<?php
			while (have_posts()) : the_post();
			?>
				<main id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="report-block">
						<!-- Section 1 -->
						<div class="title-section section ll-row">
							<div class="ll-col-lg-7">
								<div class="logo-wrap">
									<img class="logo" src="<?php echo $assets_uri . "ada-logo-h-solid.svg"; ?>" alt="Online ADA Logo">
								</div>
								<div><h1 class="header">ACCESSIBILITY COMPLIANCE</h1></div>
							</div>
							<div class="date-wrap ll-col-lg-5">
							Scan completed: <?php echo date("F jS, Y", get_post_meta($audit_id, "_ll_audit-completion-date", true)); ?>

							</div>
						</div>
						<div class="section-divider summary">
							<span class="left">Summary</span>
							<span class="right"></span>
						</div>
						<!-- Section 2 -->
						<div class="summary-section section ll-row">
							<div class="left ll-col-lg-4">
								<img class="js-summary-img"
									src="<?php echo !empty($snapshot_url) ? $snapshot_url : $assets_uri . "placeholder-600x400.png"; ?>"
									alt="<?php echo $trimmed . ' homepage' ?>"
								>
							</div>
							<div class="right ll-col-lg-8">
								<div class="star-row">

								</div>
								<div class="score-row">
									Scored <span class="global"></span> out of 5 stars for General Compliance
								</div>
								<div class="description-row">
									Congratulations on taking the necessary steps to make your website accessibility compliant and make the internet a better place for everyone.
								</div>
								<div class="domain-row">
									<?php echo ucfirst($trimmed); ?>
								</div>
							</div>
						</div>
						<div class="spacer-50"></div>
						<div class="ll-row section">
							<div class="ll-col-lg-4">
								<div class="pages-scanned-block">
									<div class="header"></div>
									<div class="sub-header">NONCOMPLIANT PAGES:</div>

								</div>
							</div>
							<div class="ll-col-lg-8">
								<div class="ll-row scores-row">

								</div>
							</div>
						</div>
						<div class="section-divider reports">
							<span class="left">Detailed Reports</span>
							<span class="right"></span>
						</div>
						<div class="section-divider errors-found">
							<div class="left">Errors Found</div>
							<div class="right">
								<?php if ($cbr_report): ?>
								<div class="img-wrap">
									<img src="<?php echo $assets_uri . "eye-con.png" ?>" alt="" >
								</div>
								<div class="sub-header">Color Blindness</div>
								<a target="blank" href="<?php echo $cbr_report["pub_path"]; ?>">
									<div class="pdf-viewer">
										<span class="left"><img src="<?php echo $assets_uri . "download-icon-blue.png" ?>" alt="" ></span>
										<span class="right">PDF Viewer</span>
									</div>
								</a>
								<?php endif; ?>
							</div>
						</div>
						<div class="section reports">
							<div class="tabs">
								<div data-tab-controls="1" class="tab tab-1 active">VIEW BY PAGE</div>
								<div data-tab-controls="2" class="tab tab-2">VIEW BY OBJECT</div>
							</div>
							<!-- FILTERS -->
							<div class="filter-wrap">
								<div class="filters-row ll-row ll-no-gutters">
									<div class="ll-col-lg-2 ll-col-sm-4">
										<div class="filter">
											<label>
												<input type="checkbox" class="invisible" data-filter="flag-a" checked="checked">
												<div class="checkbox checked"></div>
												<span class="wcag-icon">A</span>
												<span>WCAG Level A</span>
											</label>
										</div>
									</div>
									<div class="ll-col-lg-2 ll-col-sm-4">
										<div class="filter">
											<label>
												<input type="checkbox" class="invisible" data-filter="flag-aa" checked="checked">
												<div class="checkbox checked"></div>
												<span class="wcag-icon">AA</span>
												<span>WCAG Level AA</span>
											</label>
										</div>
									</div>
									<div class="ll-col-lg-2 ll-col-sm-4">
										<div class="filter">
											<label>
												<input type="checkbox" class="invisible" data-filter="flag-s508" checked="checked">
												<div class="checkbox checked"></div>
												<span class="wcag-icon">508</span>
												<span>Section 508</span>
											</label>
										</div>
									</div>
									<div class="ll-col-lg-2 ll-col-sm-4">
										<div class="filter">
											<label>
												<input type="checkbox" class="invisible" data-filter="flag-contrast" checked="checked">
												<div class="checkbox checked"></div>
												<span class="wcag-icon no-bg">
													<img alt="" role="presentation" src="<?php echo $assets_uri . "contrast-icon.png"; ?>" >
												</span>
												<span>Contrast</span>
											</label>
										</div>
									</div>
									<div class="ll-col-lg-2 ll-col-sm-4">
										<div class="filter">
											<label>
												<input type="checkbox" class="invisible" data-filter="flag-error" checked="checked">
												<div class="checkbox checked"></div>
												<span class="wcag-icon no-bg">
													<img alt="" role="presentation" src="<?php echo $assets_uri . "error-icon.png"; ?>" >
												</span>
												<span>Errors</span>
											</label>
										</div>
									</div>
									<div class="ll-col-lg-2 ll-col-sm-4">
										<div class="filter">
											<label>
												<input type="checkbox" class="invisible" data-filter="flag-warning" checked="checked">
												<div class="checkbox checked"></div>
												<span class="wcag-icon no-bg">
													<img alt="" role="presentation" src="<?php echo $assets_uri . "warning-icon.png"; ?>" >
												</span>
												<span>Warnings</span>
											</label>
										</div>
									</div>
								</div>
							</div>
							<!-- END FILTERS -->
							<div class="section downloads">
								<div class="header ll-row">
									<div class="left ll-d-flex">
										<div class="ll-row" style="width:100%;">
											<!-- <div class="ll-col-12 ll-justify-content-center top">Advanced Filters</div>
											<div class="ll-col-6 ll-justify-content-center ll-d-flex ll-align-items-center">
												<div class="toggle-switch hide-repeat-errors">
													<label class="switch">
														<div class="label">Repeating Elements</div>
														<input type="checkbox">
														<span class="slider round"><span class="show">show</span><span class="hide">hide</span></span>
													</label>
												</div>
											</div>
											<div class="ll-col-6 ll-justify-content-center ll-d-flex ll-align-items-center">
												<div class="toggle-switch hide-unique-errors">
													<label class="switch">
														<div class="label">Unique Elements</div>
														<input type="checkbox">
														<span class="slider round"><span class="show">show</span><span class="hide">hide</span></span>
													</label>
												</div>
											</div> -->
										</div>
									</div>
									<div class="right ll-d-flex ll-justify-content-sm-center ll-justify-content-lg-end">
										<div class="right-wrap" style="text-align:center;">
											<div class="top">Download Your Report</div>
											<div class="bottom ll-d-flex ll-justify-content-center">
												<button style="visibility:hidden" class="ll-d-flex ll-align-items-center">
													<span class="wcag-icon">
														<img alt="" src="<?php echo $assets_uri . "download-icon-white.png"; ?>" >
													</span>
													<span class="text">PDF REPORT</span>
												</button>
												<button class="ll-d-flex ll-align-items-center csv">
													<span class="wcag-icon">
														<img alt="" src="<?php echo $assets_uri . "download-icon-white.png"; ?>" >
													</span>
													<span class="text">CSV REPORT</span>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- REPORT BY PAGE-->
							<div data-tab="1" class="ll-tab-content tab-1 active">
								<!-- All Pages -->
								<div class="body ll-row ll-no-gutters">

								</div>
							</div>
							<!-- END REPORT BY PAGE -->
							<div data-tab="2" class="ll-tab-content tab-2">
								<div class="body ll-row ll-no-gutters">

								</div>
							</div>
						</div>
					</div>
				</main>
			<?php
			endwhile;
			?>
			</div>
        </div>
    </div>
<?php

get_footer();