<style>
	/* .default{
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
	}
	.default .default-message{font-size: 20px;padding-bottom:40px;}
	.default .default-button{font-size: 20px;font-weight: 700;} */
    
</style>

<?php
	// $default_content = "";
	// $role = array_shift($user->roles);
	
	// if($role === "free_compliance_audit_user" OR ($role === "compliance_audit_user" AND get_user_meta($user->ID, "_limelight_license_option", true) == 1 ) ){
	// 	$audit = get_posts(["author" => $user->ID, "post_type" => "compliance_audit"])[0];
	// 	$new_scan_url = get_post_meta($audit->ID, "_ll_audit_new_scan_url", true);
	// 	$default_content = "<div class='default'>";
	// 	$default_content .= "<div class='default-message'>Click here to get started with a new scan</div>";
	// 	$default_content .= "<div class='default-button'><div class='vc_btn3-container ada-button dark vc_btn3-center large'>";
		
	// 	$default_content .= "<a style='display:inline-block;text-align:center;' class='vc_general vc_btn3 vc_btn3-size-lg vc_btn3-color-ada-button-dark' href='".$new_scan_url."new-scan' title='START NEW SCAN'>START A NEW SCAN</a>";
		
	// 	$default_content .= "</div>";
	// 	$default_content .= "</div>";
	// }
	
	get_header();
?>
<main id="main">
	<div class="outer-grid">
		<div class="grid">
			<?php
			while (have_posts()) : the_post();
			?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<div class="content-body">
							<?php the_content() ?>
						</div>
					</div>
				</article>
			<?php
			endwhile;
			?>
		</div>
	</div>
</main>
<?php
get_footer();