<?php
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}

$audits = get_posts(["author" => $user->ID, "post_type" => "compliance_audit", "numberposts" => -1]);
$domains = [];
$sitemaps = [];
foreach($audits AS $audit){
	$domain = get_post_meta($audit->ID, "_ll_audit_domain", true);
	$domain = Limelight_Child_Helpers::split_domain($domain);
	$domain['domain'] = untrailingslashit($domain['domain']);
	$rel_path = $rel_path = "/".$user->ID."/".$domain['domain']."/sitemap/";
	$path = $basedir.$rel_path.$filename;
	$has_sitemap = "true";
	if(!is_file($path)){
		$has_sitemap = "false";
	}
	if(is_file($path)){
		$sitemaps[] = $path;
	}
	$domains[$audit->ID] = $domain;
	$domains[$audit->ID]["has-sitemap"] = $has_sitemap;
}

get_header();

?>
    <main id="main" class="page-template-members">
        <div class="outer-grid">
            <div class="grid">
                <?php
                while (have_posts()) : the_post();
                ?>
                    <div class="content-wrapper dark">
                        <div class="content-header">
                            
                        </div>
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <div class="entry-content">
                                <div class="content-body">
									<header class="head-wrap">                                           
										<h2>Select a website to manage</h2>
									</header>
									<div class="body-wrap select-wrap">
										<select class="website-select ll-input" name="website-select">
											<?php foreach($domains AS $audit_id => $domain): ?>
												<option data-has-sitemap="<?php echo $domains[$audit_id]["has-sitemap"] ?>" value="<?php echo $audit_id ?>"><?php echo untrailingslashit($domain["protocol"]."://".$domain["domain"]); ?></option>
											<?php endforeach; ?>
										</select>
									</div>
									<header class="head-wrap">                                           
										<h2>Choose an option</h2>
									</header>
									<div class="body-wrap">
										<form id="sitemap" action="" method="POST" enctype="multipart/form-data">											
											<div class="input-wrap">
												<div>
													<label style="padding-right: 5px;" for="create">Create new</label>
													<input id="create" type="radio" name="_sitemap" value="create_new" checked />
												</div>
												<div>
													<label style="padding-right: 5px;" for="upload">Upload sitemap</label>
													<input id="upload" type="radio" name="_sitemap" value="upload" />
												</div>
												<div style="display:none;">
													<label style="padding-right: 5px;" for="existing">Review existing</label>
													<input id="existing" type="radio" name="_sitemap" value="review_existing" />
												</div>
											</div>

											<div style="display:none;">
												<p>Accepted file formats include .xml and .csv</p>
												<input aria-label="Upload File" type="file" name="fileUpload" id="fileUpload" accept=".xml,.csv" >
											</div>
											<button class="submit-button">SUBMIT</button>
											<div class="msg"></div>
											<?php if(!empty($sitemaps)): ?>
												<div>Since you have a sitemap registered, you can start a scan</div>
												<div class="vc_btn3-container ada-button dark large">
													<a class="vc_general vc_btn3 vc_btn3-size-lg vc_btn3-color-ada-button-dark" href="/new-scan">Start a new scan</a>
												</div>
											<?php endif; ?>
										</form>
									</div>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php
                endwhile;
                ?>
            </div>
        </div>
    </main>
<?php
get_footer();
