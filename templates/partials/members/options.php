<?php
use Limelight\Plugins\ADA_Webservice\API;
use Limelight_Child_Helpers as LCH;
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}

$audit_id = get_query_var("audit");
$domain = get_post_meta(get_post($audit_id)->ID, "_ll_audit_domain", true);

get_header();

$path = $basedir.get_post_meta($audit_id, "_sitemap_path", true);
$sitemap = LCH::get_sitemap_from_file($path, $domain);

unset($sitemap["domain"]);

wp_localize_script("options-scripts", "php_var", [
	"audit_id" => $audit_id,
	"sitemap" => $sitemap,
	"homepage" => $domain,
	"user_id" => get_current_user_id(),
	"api_nonce" => wp_create_nonce('wp_rest')
]);

?>
    <main id="main" class="page-template-members">
        <div class="outer-grid">
            <div class="grid">
                <?php
                while (have_posts()) : the_post();
                ?>
                    <div class="content-wrapper dark">
                        <div class="content-header">
							<header class="head-wrap remove">
								<h2>Select options and start the scan</h2>
							</header>
                        </div>
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <div class="entry-content">
                                <div class="body-wrap content-body">
									<div class="body-wrap">
										<div class="input-wrap remove" style="padding:5px;" >
											<div>Include pages with query strings?</div>
										</div>
										<div class="input-wrap remove" style="padding-top:5px;padding-bottom:20px;">
											<label style="padding-right:3px;" for="query-strings-yes">Yes</label>
											<input style="margin-right:50px;" id="query-strings-yes" type="radio" name="use_query_strings" value="1" checked />
											<label style="padding-right:3px;" for="query-strings-no">No</label>
											<input style="padding-right:3px;" id="query-strings-no" type="radio" name="use_query_strings" value="0" />
										</div>
										<div class="input-wrap remove" style="padding:5px;" >
											<div>Include color blind report? </div>
											<small>(This adds on average 3 minutes to the scan)</small>
										</div>
										<div class="input-wrap remove" style="padding-top:5px;padding-bottom:20px;">
											<label style="padding-right:3px;" for="color-blind-yes">Yes</label>
											<input style="margin-right:50px;" id="color-blind-yes" type="radio" name="use_color_blind" value="1" />
											<label style="padding-right:3px;" for="color-blind-no">No</label>
											<input style="padding-right:3px;" id="color-blind-no" type="radio" name="use_color_blind" value="0" checked />
										</div>

										<button class="start-scan remove">START SCAN</button>
										<div class="scan-message">
											<div class="message"></div>
											<div class="load"></div>
										</div>

										<a></a>
										<!--<a href='/wp-json/limelight/v1/test-pdf/' >PDF Test</a>-->
									</div>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php
                endwhile;
                ?>
            </div>
        </div>
    </main>
<?php
get_footer();
