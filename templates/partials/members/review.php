<?php
use Limelight\Plugins\ADA_Webservice\API;
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}

$audit = get_post(get_query_var("audit"));

if(isset($_POST["list"])){
	$revised = json_decode(stripslashes($_POST["list"]), true);
	$path = $basedir.get_post_meta($audit->ID, "_sitemap_path", true);
	//Rewrites the file with the revisions
	$rel_path = explode("sitemap.csv", get_post_meta($audit->ID, "_sitemap_path", true))[0];
	Limelight_Child_Helpers::generate_file($path, $revised, $basedir, $rel_path);
	wp_redirect( home_url("/sitemap") );
	exit;
}

global $rel_path;


$domain = get_post_meta($audit->ID, "_ll_audit_domain", true);
$domain = Limelight_Child_Helpers::split_domain($domain);
$rel_path = "/".$user->ID."/".$domain['domain']."/sitemap/";
$path = $basedir.$rel_path.$filename;

function convert_xml_to_csv( $xml, $f ){
    foreach ( $xml->children() as $item )
    {
        $hasChild = (count($item->children()) > 0)?true:false;

        if( !$hasChild ){
			if($item->getName() === "loc"){
				$put_arr = [ $item ];
	            fputcsv( $f, $put_arr ,',','"' );
			}
        }else{
        	convert_xml_to_csv( $item, $f );
        }
    }
}

function server_validate_upload($file){
	$is_csv = true;
	$pathinfo = pathinfo($file['name']);

	if( $pathinfo["extension"] !== "csv" ){ $is_csv = false; }

	if(getimagesize($file["tmp_name"])){
		return false;
	}

	$supportedFormats = ['csv','txt','xml', 'xls', 'xlsx'];

	if( in_array($pathinfo["extension"], $supportedFormats) ){
		//Store the uploaded file to the server
		global $basedir;
		global $rel_path;

		$filename = "sitemap.".$pathinfo["extension"];
		$path = $basedir.$rel_path.$filename;

		if(!file_exists($basedir.$rel_path)){
			mkdir( $basedir . $rel_path, 0777, true );
		}

		$success = move_uploaded_file( $file["tmp_name"], $path);

		if($success){
			$new_filename = "sitemap.csv";
			//Convert the file to CSV
			switch($pathinfo["extension"]){
				case "xml":
					if (file_exists($path)){
						$xml = simplexml_load_file($path);
						$f = fopen($basedir.$rel_path.$new_filename, 'w');
						convert_xml_to_csv($xml, $f);
						$stat = fstat($f);
						ftruncate($f, $stat['size']-1);
						fclose($f);
						unlink($path);
					}
					break;
			}
			return $new_filename;
		}
	}
	return false;
}

switch($_POST["_sitemap"]){
	case "create_new":
		API::create_sitemap($domain["domain"], true);
		$sitemap = API::get_sitemap();
		unset($sitemap["domain"]);
		global $basedir;
		global $rel_path;
		Limelight_Child_Helpers::generate_file( $path, $sitemap, $basedir, $rel_path );
		update_post_meta( $audit->ID, "_sitemap_path", $rel_path.$filename );
		break;
	case "upload":
		$filename = server_validate_upload( $_FILES["fileUpload"] );
		$path = $basedir.$rel_path.$filename;
		$sitemap = Limelight_Child_Helpers::get_sitemap_from_file( $path, $domain["domain"] );
		update_post_meta( $audit->ID, "_sitemap_path", $rel_path.$filename );
		break;
	case "review_existing":
		$sitemap = Limelight_Child_Helpers::get_sitemap_from_file( $path, $domain["domain"] );
		break;
}
//Don't need domain key on frontend, we have it in a variable $domain already
unset($sitemap["domain"]);

//Check for an empty sitemap
global $empty_sitemap;
$keys = array_keys($sitemap);
foreach($keys AS $key){
	if(!empty($sitemap[$key])){ $empty_sitemap = "false"; }
}
if($empty_sitemap === "true"){
	//Get errors
	global $errors;
	$errors = API::get_errors();
}

global $empty_sitemap;

get_header();

wp_localize_script("review-scripts", "php_var", [
	"sitemap" => $sitemap,
	"empty_sitemap" => $empty_sitemap,
	// "use_query_strings" => $use_query_strings,
	// "errors" => $errors
]);

?>

    <main id="main" class="page-template-members">
        <div class="outer-grid">
            <div class="grid">
                <?php
                while (have_posts()) : the_post();
                ?>
                    <div class="content-wrapper dark">
                        <div class="content-header">

                        </div>
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <div class="entry-content">
                                <div class="content-body">
									<header class="head-wrap">
                                            <h2>Review sitemap</h2>
                                        </header>
                                        <div class="body-wrap">
											<form id="sitemap-review" action="?" method="POST">
												<div class="input-wrap">
													<label for="add"><?php echo untrailingslashit($domain["domain"])."/"; ?></label>
													<input type="text" name="add" />
													<a class="sitemap-add" aria-label="Add to sitemap" href="#">ADD</a>
												</div>
												<div class="body-bottom">
		                                            <input type="hidden" name="list" value=""/>
		                                            <button>SAVE</button>
												</div>
												<div class="body-top">
													<ul class="sitemap">
														<!--populated by above script -->
			                                        </ul>
												</div>

											</form>
										</div>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php
                endwhile;
                ?>
            </div>
        </div>
    </main>
<?php

get_footer();
