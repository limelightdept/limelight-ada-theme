<?php
use Limelight\Plugins\ADA_Webservice\API;
use Limelight_Child_Helpers AS LCH;

if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}

$audits = get_posts(["author" => $user->ID, "post_type" => "compliance_audit", "numberposts" => -1]);
$domains = [];

foreach($audits AS $audit){
	$domain = get_post_meta($audit->ID, "_ll_audit_domain", true);
	$domain = LCH::split_domain($domain);
	$scan = LCH::get_scan_data($audit->ID);

	if(!empty($scan)){
		$domains[$audit->ID] = $domain;
	}
}

get_header();

wp_localize_script("previous-scan-scripts", "php_var", ["user_id" => $user->ID ]);

?>
    <main id="main" class="page-template-members">
        <div class="outer-grid">
            <div class="grid">
                <?php
                while (have_posts()) : the_post();
                ?>
                    <div class="content-wrapper dark">
                        <div class="content-header">

                        </div>
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <div class="entry-content">
                                <div class="content-body">
									<?php if(!empty($domains)): ?>
										<header class="head-wrap">
											<h2>Choose a website to view scan results for</h2>
										</header>
										<div class="body-wrap">
											<form id="sitemap" action="" method="POST" enctype="multipart/form-data">
												<div class="input-wrap">
													<div class="select-wrap">
														<select class="website-select ll-input" name="website-select">
															<?php foreach($domains AS $audit_id => $domain): ?>
																<option value="<?php echo $audit_id ?>"><?php echo untrailingslashit($domain["protocol"]."://".$domain["domain"]); ?></option>
															<?php endforeach; ?>
														</select>
													</div>
												</div>

												<button class="submit-button">View Scan Results</button>
											</form>
										</div>
									<?php else: ?>
										<header class="head-wrap">
											<h2>It looks like you haven't run any scans yet</h2>
										</header>
										<div class="body-wrap">
											Start a new scan by clicking <a href="/new-scan">here</a>
										</div>
									<?php endif; ?>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php
                endwhile;
                ?>
            </div>
        </div>
    </main>
<?php
get_footer();
