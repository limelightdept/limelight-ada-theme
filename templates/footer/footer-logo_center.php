<div class="footer-left limelight-navigation">
    <?php
    // Primary Menu
    if ( has_nav_menu( 'footer_primary' ) ) {
        $args = array(
            'theme_location' => 'footer_primary',
            'menu'           => 'Footer - Primary',
            'container'      => '',
            'container_id'   => '',
            'menu_class'     => '',
            'items_wrap'     => '<ul class="nav-list">%3$s</ul>',
        );

        echo '<nav class="nav-menu nav-footer nav-primary">';
        wp_nav_menu( $args );
        echo '</nav>';
    }

    if ( has_nav_menu( 'footer_secondary' ) ) {
        $args = array(
            'theme_location' => 'footer_secondary',
            'menu'           => 'Footer - Secondary',
            'container'      => '',
            'container_id'   => '',
            'menu_class'     => '',
            'items_wrap'     => '<ul class="nav-list">%3$s</ul>',
        );

        echo '<nav class="nav-menu nav-footer nav-secondary">';
        wp_nav_menu( $args );
        echo '</nav>';
    }

    
    $icons = Limelight\Theme\Icons::Instance();
    echo $icons->get_social_icons();
    ?>
</div>
<?php
    if($hide_navigation && $show_policies):
?>
<div class="show-policies footer-left limelight-navigation">
    <nav class=" nav-menu nav-footer nav-secondary">
        <ul class="nav-list">
            <?php
                foreach($policy_pages AS $policy_page_id => $policy_page):
            ?>
            <li id="menu-item-<?=$policy_page;?>" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-<?=$policy_page;?>">
                <a href="<?=get_permalink($policy_page);?>"><?=get_the_title($policy_page);?></a>
            </li>
            <?php 
                endforeach;
            ?>
        </ul>
    </nav>
</div>
<?php 
    endif;
?>
<div class="footer-center">
    <a <?php if(!$hide_navigation){ ?> href="/"<?php } ?> class="logo">
        <?php
        // display the image
        echo $logo;
        ?>
    </a>
</div>
<div class="footer-right"> 
    <?php if(!$hide_limelight_logo): ?>                             
        <a <?php if(!$hide_navigation){ ?> href="http://limelightdept.com/"<?php } ?> target="_blank" id="lm_logo">
            <?php
            echo Limelight\Theme\SVG::generate("Logo");
            ?>
        </a>
    <?php endif; ?>
    <div class="copyright">
        <?php echo do_shortcode(limelight_get_option("copyright")); ?>
    </div>
</div>