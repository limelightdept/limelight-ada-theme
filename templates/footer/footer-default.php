
<div class="footer-left">
    <div class="limelight-navigation">
        <div class="nav-wrapper">
            <?php
            // Primary Menu
            if ( has_nav_menu( 'footer_primary' ) ) {
                $args = array(
                    'theme_location' => 'footer_primary',
                    'menu'           => 'Footer - primary',
                    'container'      => '',
                    'container_id'   => '',
                    'menu_class'     => '',
                    'items_wrap'     => '<ul class="nav-list">%3$s</ul>',
                );

                echo '<nav class="nav-menu nav-footer nav-primary">';
                wp_nav_menu( $args );
                echo '</nav>';
            }

            // Secondary Menu
            if ( has_nav_menu( 'footer_secondary' ) ) {
                $args = array(
                    'theme_location' => 'footer_secondary',
                    'menu'           => 'Footer - Secondary',
                    'container'      => '',
                    'container_id'   => '',
                    'menu_class'     => '',
                    'items_wrap'     => '<ul class="nav-list">%3$s</ul>',
                );

                echo '<nav class="nav-menu nav-footer nav-secondary">';
                wp_nav_menu( $args );
                echo '</nav>';
            }


        echo "</div>";
        //$icons = new Limelight_Social_Icons();
        //echo $icons->get_social_icons();
        ?>
    </div>
</div>

<div class="footer-center">
    <a <?php if(!$hide_navigation){ ?> href="/"<?php } ?> >
        <?php echo "<img alt='Online ADA Logo' src='" . limelight_get_option("logo_footer") . "' class='footer-logo' />" ?>
    </a>
</div>
<div class="footer-right" >
    <div id="lm_logo">
        <?php
        //$svg = new Limelight_Svg();
        //echo $svg->get_svg("Limelight_logo");
        ?>
    </div>
    <div class="copyright">
        <?php echo do_shortcode(limelight_get_option("copyright")); ?>
    </div>
</div>
