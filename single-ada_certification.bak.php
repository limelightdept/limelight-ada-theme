<?php

if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
get_header();


global $wp_query;

$post = get_post($wp_query->query_vars["ada_certification"]);
$user = get_user_by("ID", $post->post_author);
$domain = get_post_meta($post->ID, "_ada_cert_domain", true);
$status = get_post_meta($post->ID, "_ada_cert_status", true);
$date_certified = get_post_meta($post->ID, "_ada_cert_date_certified", true);
$badge_link = get_post_meta($post->ID, "_ada_cert_badge", true);
switch($status){
    case "in_review":
        $status = "In Review";
        break;
    case "not_certified":
        $status = "Not Certified";
        break;
}
?>
<style>
    .badge-content{background-color:#eee;border:1px solid black;text-align:center;display:flex;height:auto;width:100%;justify-content: center;align-items:center;box-sizing: border-box;padding: 20px 5px;}
</style>
<div id="main">
    <div class="outer-grid">
        <div class="grid">
            <main>
                <article <?php post_class(); ?>>
                    <div class="entry-content">
                        <div class="content-wrapper" style="text-align:center;margin-top:150px;">
                            <h2>
                                <div>Hi <?php echo $user->display_name; ?></div>
                                <div>Your certification for <?php echo $domain; ?> was purchased on <?php echo get_post_meta($post->ID, "_ada_cert_purchase_date", true); ?>
                                    <?php if(!empty($date_certified) AND $status === "certified"): ?>
                                        and was certified on  <?php echo $date_certified; ?>
                                    <?php endif; ?>
                                </div>
                                <div style="margin-top:30px;">Your certification status is: <?php echo $status; ?></div>
                                <?php if($status === "certified"): ?>
                                    <div>This is what your public badge will look like:</div>
                                    <img style="width:200px;" src="<?php echo content_url(get_post_meta($post->ID, "_ada_cert_badge_upload_path", true)); ?>" />
                                    <div>Here is your Badge HTML:</div>
                                <?php endif ?>
                            </h2>
                                <?php if($status === "certified"): ?>
                                    <div class="badge-content" style="color:black;width:100%;">
                                        <?php
                                            echo trim(esc_html($badge_link));
                                        ?>
                                    </div>
                                <?php endif ?>
                            </h2>
                        </div>
                    </div>
                </article>
            </main>
        </div>
    </div>
</div>

<?php

get_footer();
