<?php
if (!defined('ABSPATH')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit;
}
get_header();

//If license option is 5, hide 10 form
switch(get_query_var("license_option")){
	case "5":
		$selector = "#wpcf7-f1032-p1042-o2";
		break;
	case "10":
		$selector = "#wpcf7-f1031-p1042-o1";
		break;
}
?>
<style>
	<?php echo $selector; ?>{display:none;}
</style>
    <main id="main">
        <div class="outer-grid">
			<div class="grid">
			<?php
			while (have_posts()) : the_post();
			?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
				</article>
			<?php
			endwhile;
			?>
			</div>
        </div>
    </main>
<?php
get_footer();
